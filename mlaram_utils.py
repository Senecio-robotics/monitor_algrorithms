import pickle

from logging_utils import LOG
from response_utils import MlException


class MlARAMService(object):
    def __init__(self, modelPath: str):
        try:
            self.model = pickle.load(open(modelPath, 'rb'))
            LOG.info("Multi-label ARAM loaded successfully:" + modelPath)
        except Exception as e:
            LOG.exception(e)
            raise MlException('Fail to load Multi-label ARAM file: [{}]'.format(modelPath))

    def predict(self, x_mat):
        try:
            predict_sparse_vec = self.model.predict(x_mat)
            return predict_sparse_vec
        except Exception as e:
            LOG.error(e)
            raise MlException('Fail Multi-label ARAM predict: [{}]'.format(str(e)))
