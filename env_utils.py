import os


def get_env_str(key: str, default: str):
    return os.environ.get(key) or default


def get_env_boolean(key: str, default: str):
    ret = get_env_str(key, default)
    return bool(ret)


def get_env_int(key: str, default: str):
    ret = get_env_str(key, default)
    return int(ret)


def get_env_float(key: str, default: str):
    ret = get_env_str(key, default)
    return float(ret)
