#import pandas as pd
#import cv2
# define a simple data batch
import numpy as np
#from os.path import exists #, isfile, join, dirname, basename, splitext
#from os import listdir, path, makedirs, scandir, remove, unlink, rmdir
np_epsf = np.finfo(float).eps
np_epsf32 = np.finfo(np.float32).eps

#from image_utils import read_image_file, get_COLOR_BGR2RGB
from misc_utils import update_feature_mat #get_image_blocks, get_bulk_swapped, update_mat_list,

#from signatureTableData import dict_table
from numpy_DataStaticClass  import numpy_DataStaticClass
TOP_N_CATEGORIES = numpy_DataStaticClass.TOP_N_CATEGORIES
RESNET_LABELS = numpy_DataStaticClass.RESNET_LABELS
EPS_SAF = numpy_DataStaticClass.EPS_SAF
KNN_SIZE = numpy_DataStaticClass.KNN_SIZE
NUM_IMGS_PER_BATCH = numpy_DataStaticClass.NUM_IMGS_PER_BATCH

def add_missing_cats(labels_topn, scores_topn, prob_vec, sorted_scores_args, show=False):
    set_labs = set(labels_topn)
    set_scores = set(scores_topn)
    if len(set_labs) < TOP_N_CATEGORIES:
        if show:
            print('0 add_missing_cats: 0deb len(set_labs), len(set_scores) ', len(set_labs), len(set_scores))
        try:
            tmp_labs = list()
            tmp_scores = list()
            for ii, cid in enumerate(labels_topn):
                #cid = labels_topn[ii]
                probability = scores_topn[ii]
                if cid not in tmp_labs:
                    tmp_labs += [cid]
                    tmp_scores += [probability]
                else:
                    jj = tmp_labs.index(cid)
                    if tmp_scores[jj] < probability:
                        tmp_scores[jj] = probability
            if len(tmp_labs) < TOP_N_CATEGORIES:
                if show:
                    print('1 add_missing_cats: end deb len(tmp_labs), len(tmp_scores) ', len(tmp_labs), len(tmp_scores))
                index_rest = sorted_scores_args[TOP_N_CATEGORIES:]
                for ii in index_rest:
                    resnet_lab = RESNET_LABELS[ii]
                    cid = resnet_lab[10:]
                    probability = prob_vec[ii]
                    if cid not in tmp_labs:
                        tmp_labs += [cid]
                        tmp_scores += [probability]
                        if len(tmp_labs) == TOP_N_CATEGORIES:
                            break
                if len(tmp_labs) < TOP_N_CATEGORIES:
                    print('2 add_missing_cats: end deb len(tmp_labs), len(tmp_scores) ', len(tmp_labs), len(tmp_scores))
                    cid ='unknown'
                    probability = 0.5*min(tmp_scores)
                    for kk_more in range(len(tmp_labs), TOP_N_CATEGORIES):
                        if cid not in tmp_labs or len(tmp_labs) < TOP_N_CATEGORIES:
                            tmp_labs += [cid]
                            tmp_scores += [probability]
            labels_topn = tuple(tmp_labs)
            scores_topn = tuple(tmp_scores)
        except Exception as inst:
            print("add_missing_cats had exception on response =  ", inst)
    return labels_topn, scores_topn


def numpy_add_missing_cats(labels_topn, scores_topn, prob_vec, sorted_scores_args, show=False):
    set_labs = set(labels_topn)
    set_scores = set(scores_topn)
    out_labels_topn = np.full(TOP_N_CATEGORIES, str(), dtype=np.ndarray)
    out_scores_topn = np.full(TOP_N_CATEGORIES, 0.0, dtype=np.ndarray)
    if len(set_labs) < TOP_N_CATEGORIES:
        if show:
            print('0 add_missing_cats: 0deb len(set_labs), len(set_scores) ', len(set_labs), len(set_scores))
        try:
            #tmp_labs = list()
            #tmp_scores = list()
            label_ind = 0
            for ii, cid in enumerate(labels_topn):
                #cid = labels_topn[ii]
                probability = scores_topn[ii]
                if cid not in out_labels_topn and label_ind < TOP_N_CATEGORIES:
                    out_labels_topn[label_ind] = cid
                    out_scores_topn[label_ind] = probability
                    label_ind += 1
                else:
                    jj = np.where(out_labels_topn == cid)
                    if jj > -1 and out_scores_topn[jj] < probability:
                        out_scores_topn[jj] = probability
            if label_ind < TOP_N_CATEGORIES:
                if show:
                    print('1 add_missing_cats: end deb out_ label_ind ', label_ind)
                index_rest = sorted_scores_args[TOP_N_CATEGORIES:]
                for ii in index_rest:
                    resnet_lab = RESNET_LABELS[ii]
                    cid = resnet_lab[10:]
                    probability = prob_vec[ii]
                    if cid not in out_labels_topn:
                        out_labels_topn[label_ind] = cid
                        out_scores_topn[label_ind] = probability
                        label_ind += 1
                        if label_ind == TOP_N_CATEGORIES:
                            break
                if label_ind < TOP_N_CATEGORIES:
                    print('2 add_missing_cats: end deb out_ label_ind ', label_ind)
                    cid ='unknown'
                    probability = float(0.5*np.min(out_scores_topn))
                    for kk_more in range(label_ind, TOP_N_CATEGORIES):
                        if cid not in out_labels_topn or label_ind < TOP_N_CATEGORIES:
                            out_labels_topn[label_ind] = cid
                            out_scores_topn[label_ind] = probability
                            label_ind += 1
        except Exception as inst:
            print("add_missing_cats had exception on response =  ", inst)
    else:
        for index in range(TOP_N_CATEGORIES):
            out_labels_topn[index] = labels_topn[index]
            out_scores_topn[index] = scores_topn[index]
    return out_labels_topn, out_scores_topn


def extract_all_topn_content(confidence_mat):
    # global mx, mod, Batch, RESNET_LABELS
    labels_topn_mat = []
    scores_topn_mat = []
    try:
        mat_dims = np.shape(confidence_mat)
        #print('debug extract_all_topn_content mat_dims = ', mat_dims)
        for index in range(mat_dims[0]):
            prob_vec = confidence_mat[index]
            prob_vec = np.squeeze(prob_vec)
            sorted_scores_args = np.argsort(prob_vec)[::-1]
            len_scores = len(sorted_scores_args)
            topn = TOP_N_CATEGORIES
            if TOP_N_CATEGORIES > len_scores:
                topn = len_scores

            index_topn = sorted_scores_args[0:topn]
            tmplst = [RESNET_LABELS[i] for i in index_topn]
            labels_topn = [clab_prefix[10:] for clab_prefix in tmplst]
            scores_topn = [prob_vec[i] for i in index_topn]
            if len(set(labels_topn)) < TOP_N_CATEGORIES:
                labels_topn, scores_topn = add_missing_cats(labels_topn, scores_topn, prob_vec, sorted_scores_args)
            labels_topn_mat = update_feature_mat(labels_topn, labels_topn_mat)
            scores_topn_mat = update_feature_mat(scores_topn, scores_topn_mat)
    except Exception as inst:
        print("extract_all_topn_content had exception on : ", inst)

    return labels_topn_mat, scores_topn_mat

def numpy_extract_all_topn_content(confidence_mat):
    # global mx, mod, Batch, RESNET_LABELS
    mat_dims = np.shape(confidence_mat)
    labels_topn_mat = np.full((mat_dims[0], TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    scores_topn_mat = np.full((mat_dims[0], TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    #print("deb labels_topn_mat ", np.shape(labels_topn_mat))
    #print("deb scores_topn_mat ", np.shape(scores_topn_mat))
    try:
        #print('debug extract_all_topn_content mat_dims = ', mat_dims)
        for index in range(mat_dims[0]):
            prob_vec = confidence_mat[index]
            prob_vec = np.squeeze(prob_vec)
            sorted_scores_args = np.argsort(prob_vec)[::-1]
            len_scores = len(sorted_scores_args)
            topn = TOP_N_CATEGORIES
            if TOP_N_CATEGORIES > len_scores:
                topn = len_scores
            #print("deb topn, prob_vec,sorted_scores_args ", topn, np.shape(prob_vec), np.shape(sorted_scores_args))
            index_topn = sorted_scores_args[0:topn]
            #print("0deb index_topn ", np.shape(index_topn))
            labels_topn = np.full(len(index_topn), str(), dtype=np.ndarray)
            scores_topn = np.full(len(index_topn), 0.0, dtype=np.ndarray)
            #print("00deb labels_topn ", np.shape(labels_topn))
            #print("00deb scores_topn ", np.shape(scores_topn))
            for jj, ii in enumerate(index_topn):
                clab_prefix = RESNET_LABELS[ii][10:]
                #print("0deb index,ii, clab_prefix ", index,ii, clab_prefix, type(clab_prefix))
                #print("0deb index,ii, prob_vec[ii] ", index,ii, prob_vec[ii])
                labels_topn[jj] = clab_prefix
                scores_topn[jj] = prob_vec[ii]
            #print("0deb labels_topn ", np.shape(labels_topn))
            #print("0deb scores_topn ", np.shape(scores_topn))

            if len(set(labels_topn)) < TOP_N_CATEGORIES:
                labels_topn, scores_topn = numpy_add_missing_cats(labels_topn, scores_topn, prob_vec,
                                                                  sorted_scores_args)
            #print("deb labels_topn ", np.shape(labels_topn))
            #print("deb scores_topn ", np.shape(scores_topn))
            labels_topn_mat[index] = labels_topn
            scores_topn_mat[index] = scores_topn
    except:
        print("failed numpy_extract_all_topn_content..  try extract_all_topn_content! ")
        labels_topn_mat, scores_topn_mat = extract_all_topn_content(confidence_mat)
        labels_topn_mat = np.array(labels_topn_mat)
        scores_topn_mat = np.array(scores_topn_mat)

    return labels_topn_mat, scores_topn_mat


def numpy_most_topn_from_all(labels_topn_all, scores_topn_all, max_cats=TOP_N_CATEGORIES):
    sorted_scores_args = np.argsort(scores_topn_all)[::-1]
    #print("numpy_most_topn_from_all deb input ", np.shape(sorted_scores_args),
    #      type(labels_topn_all), len(labels_topn_all))
    labels_topn_out = np.full(max_cats, str(), dtype=np.ndarray)
    scores_topn_out = np.full(max_cats, 0.0, dtype=np.ndarray)
    label_ind = 0
    for ii in sorted_scores_args:
        if label_ind == max_cats:
            break
        else:
            #print("numpy_most_topn_from_all deb ii ", ii, np.shape(sorted_scores_args),
            #      type(labels_topn_all), len(labels_topn_all), max_cats)

            cid = labels_topn_all[ii]
            probability = scores_topn_all[ii]
            #print("numpy_most_topn_from_all deb ii ", ii, cid, probability, type(cid), type(probability))
            if cid not in labels_topn_out and label_ind < max_cats:
                labels_topn_out[label_ind] = cid
                scores_topn_out[label_ind] = probability
                label_ind += 1
            else:
                jj = np.where(labels_topn_out == cid)
                jj = int(jj[0])
                #print("numpy_most_topn_from_all deb ii, jj ", ii, jj, type(jj), scores_topn_out[jj], type(scores_topn_out[jj]))
                if jj > -1 and np.float32(scores_topn_out[jj]) < probability:
                    scores_topn_out[jj] = probability

    if label_ind < max_cats:
        max_cats = min(len(labels_topn_all), max_cats)
        if max_cats > 0:
            inds = sorted_scores_args[:max_cats]
            for ii in inds:
                if label_ind == max_cats:
                    break
                else:
                    cid = labels_topn_all[ii]
                    probability = scores_topn_all[ii]
                    # if cid not in tmp_labs or cprob not in tmp_scores:
                    labels_topn_out[label_ind] = cid
                    scores_topn_out[label_ind] = probability
                    label_ind += 1

    return labels_topn_out, scores_topn_out


def numpy_knn_match_cost(np_vec, confidence):
    #dref = tuple([abs(x - confidence) for x in tpl_vec])
    dref = abs(np_vec-confidence)
    min_match = max(min(dref), EPS_SAF)
    knn = min(len(dref), KNN_SIZE)
    if knn > 1:
        saf_match = 10.0 * min_match
        accum_match = 0
        sorted_dref_args = np.argsort(dref)
        inds_dref = sorted_dref_args[:knn]
        den = 0
        for ii in inds_dref:
            cmnp = max(dref[ii], EPS_SAF)
            if cmnp < saf_match:
                accum_match += cmnp
                den += 1
            else:
                break
        min_match = accum_match / den
        cost = 1.0 - min_match
    else:
        cost = 1.0 - min_match
    return cost




def is_cost_valid(total_cost_vec, local_eps=2.22e-16):
    max_abs_val = max([abs(cval) for cval in total_cost_vec])
    if max_abs_val > (1.0 + local_eps):
        print('err: is_cost_valid not valid total_cost_vec = ', total_cost_vec)
        return False
    return True


def norm_cost(total_cost_vec_in):
    denom = max(TOP_N_CATEGORIES, max(total_cost_vec_in), 1.0)
    total_cost_vec = np.array([cval / denom for cval in total_cost_vec_in], dtype=np.float32)
    return total_cost_vec


def norm_freq_cost(total_cost_vec_in, local_eps=2.22e-16):
    max_abs_val = max([abs(x) for x in total_cost_vec_in])
    denom = max(max_abs_val, local_eps)
    total_cost_vec = np.array([cval / denom for cval in total_cost_vec_in], dtype=np.float32)
    return total_cost_vec


def norm_all_dist(dist_all_vec, local_eps=2.22e-16):
    max_abs_val = max([abs(x) for x in dist_all_vec])
    denom = max(max_abs_val, local_eps)
    out_dist_vec = np.array([cval / denom for cval in dist_all_vec], dtype=np.float32)
    return out_dist_vec

def get_merged_cost(norm_cost_vec, norm_freq_vec, alpha=0.25, len_tables_vec=18):
    norm_all_vec = np.array([], dtype=np.float32)
    #length = (len(norm_cost_vec) - 1) // 3 + 1
    beta = 1.0 - alpha
    for ii in range(0, len(norm_cost_vec), 3):
        bvec = norm_cost_vec[ii:ii + 3]
        avec = norm_freq_vec[ii:ii + 3]
        if len(norm_all_vec) == 0:
            norm_all_vec = np.concatenate((bvec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
        else:
            norm_all_vec = np.concatenate((norm_all_vec, bvec), axis=0)
            norm_all_vec = np.concatenate((norm_all_vec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
    # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
    # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    return norm_all_vec


def complete_merged_cost(norm_cost_vec, norm_freq_vec, alpha=0.25, len_tables_vec=18):
    norm_all_vec = np.array([], dtype=np.float32)
    beta = 1.0 - alpha
    for ii in range(0, len(norm_cost_vec), 3):
        bvec = norm_cost_vec[ii:ii + 3]
        avec = norm_freq_vec[ii:ii + 3]
        if len(norm_all_vec) == 0:
            norm_all_vec = np.concatenate((bvec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
        else:
            norm_all_vec = np.concatenate((norm_all_vec, bvec), axis=0)
            norm_all_vec = np.concatenate((norm_all_vec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
    # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
    # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    comp_norm_all_vec = np.concatenate((norm_all_vec, 1.0 - norm_all_vec), axis=None)
    return comp_norm_all_vec


def numpy_knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec):
    #global table_dct, mid_vec
    sz = 3 * len(table_dct)
    total_cost_vec = np.full(sz, 0.0)#, dtype=np.float32)
    if not table_dct or len(table_dct) == 0:
        print('knn_match_cost_counter table_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_dct.items():
        if not cpecies:
            print('knn_match_cost_counter index, cpecies is empty => run statistical algo = ', index)
        else:
            tindex = 3 * index
            for labelIndex in range(len(labels_topn)):
                clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    val_vec = cpecies[clab]
                    if len(val_vec) > 0:
                        cost = numpy_knn_match_cost(np.asarray(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cost
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    total_cost_vec[tindex + 2] += cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        index += 1
    return total_cost_vec


def numpy_freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec, mid_vec):
    #global table_dct, table_freq_dct
    #global freq_mid_vec, mid_vec
    sz = 3 * len(table_freq_dct)
    total_cost_vec = np.full(sz, 0.0)#, dtype=np.float32)
    if not table_freq_dct or len(table_freq_dct) == 0:
        print('freq_weight_counter table_freq_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_freq_dct.items():
        if not cpecies:
            print('freq_weight_counter index, cpecies is empty => run statistical algo = ', index)
        else:
            ref_tbl = table_dct[ckey]
            tindex = 3 * index
            for labelIndex in range(len(labels_topn)):
                clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    cfreq = cpecies[clab]
                    val_vec = ref_tbl[clab]
                    if len(val_vec) > 0:
                        cost = numpy_knn_match_cost(np.asarray(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cfreq * cost
                    # total_cost_vec[tindex+1] += cfreq
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    creq = freq_mid_vec[index]
                    total_cost_vec[tindex + 2] += creq * cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        index += 1

    return total_cost_vec

def numpy_knn_match_cost_dist(np_vec, confidence):
    #dref = tuple([abs(x - confidence) for x in tpl_vec])
    dref = abs(np_vec-confidence)
    min_match = max(min(dref), EPS_SAF)
    knn = min(len(dref), KNN_SIZE)
    if knn > 1:
        saf_match = 10.0 * min_match
        accum_match = 0
        sorted_dref_args = np.argsort(dref)
        inds_dref = sorted_dref_args[:knn]
        den = 0
        for ii in inds_dref:
            cmnp = max(dref[ii], EPS_SAF)
            if cmnp < saf_match:
                accum_match += cmnp
                den += 1
            else:
                break
        min_match = accum_match/den
        cost = 1.0 - min_match
    else:
        cost = 1.0 - min_match
    return cost, min_match

def numpy_knn_match_cost_counter_dist(labels_topn, scores_topn, table_dct, mid_vec):
    # global table_dct, mid_vec
    sz = 3 * len(table_dct)
    total_cost_vec = np.full(sz, 0.0)#, dtype=np.float32)
    dist_all_vec = []  # [0]*len(labels_topn)*len(table_dct) # 20*6
    if not table_dct or len(table_dct) == 0:
        print('table_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_dct.items():
        if not cpecies:
            cur_dist_vec = list()
            print('index, cpecies is empty => run statistical algo = ', index)
        else:
            cur_dist_vec = [0.0] * len(labels_topn)
            tindex = 3 * index
            for labelIndex, clabel in enumerate(labels_topn):
                #clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    val_vec = cpecies[clab]
                    if len(val_vec) > 0:
                        cost, cdist = numpy_knn_match_cost_dist(np.asarray(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cost
                        cur_dist_vec[labelIndex] = cost
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    total_cost_vec[tindex + 2] += cost
                    cur_dist_vec[labelIndex] = -cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        if len(cur_dist_vec) > 0:
            dist_all_vec.extend(cur_dist_vec)
        index += 1
    return total_cost_vec, dist_all_vec


def numpy_freq_weight_counter_dist(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec, mid_vec):
    # global table_dct, table_freq_dct
    # global freq_mid_vec, mid_vec
    sz = 3 * len(table_freq_dct)
    total_cost_vec = np.full(sz, 0.0)#, dtype=np.float32)
    dist_all_vec = []  # [0]*len(labels_topn)*len(table_dct) # 20*6
    if not table_freq_dct or len(table_freq_dct) == 0:
        print('table_freq_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_freq_dct.items():
        if not cpecies:
            cur_dist_vec = list()
            print('index, cpecies is empty => run statistical algo = ', index)
        else:
            ref_tbl = table_dct[ckey]
            tindex = 3 * index
            cur_dist_vec = [0] * len(labels_topn)
            for labelIndex, clabel in enumerate(labels_topn):
                #clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    cfreq = cpecies[clab]
                    val_vec = ref_tbl[clab]
                    if len(val_vec) > 0:
                        cost, cdist = numpy_knn_match_cost_dist(np.asarray(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cfreq * cost
                        cur_dist_vec[labelIndex] = cfreq * cost
                    # total_cost_vec[tindex+1] += cfreq
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    creq = freq_mid_vec[index]
                    total_cost_vec[tindex + 2] += creq * cost
                    cur_dist_vec[labelIndex] = -creq * cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        if len(cur_dist_vec) > 0:
            dist_all_vec.extend(cur_dist_vec)
        index += 1
    return total_cost_vec, dist_all_vec


#functions not used


def calc_cost_hist_probabilty(label_vec, conf_vec):
    out_hist_probabilty = dict()
    for index, clab in enumerate(label_vec):
        score = conf_vec[index]
        out_hist_probabilty[clab] = score/len(label_vec)

    return out_hist_probabilty

def calc_hist_probabilty(hash_dict, normhashFreq, local_eps=np.finfo(float).eps):
    tmp_hist_probabilty = dict()
    out_hist_probabilty = dict()
    tot_weight = 0.0
    for clab, confidence_vec in hash_dict.items():
        conf_weight = len(confidence_vec)
        weigth_freq = local_eps
        if clab in normhashFreq:
            weigth_freq = normhashFreq[clab]
        cur_weight = weigth_freq * conf_weight
        tmp_hist_probabilty[clab] = cur_weight*np.mean(confidence_vec)
        tot_weight += cur_weight

    tot_weight = max(tot_weight, local_eps)
    for clab in tmp_hist_probabilty:
        out_hist_probabilty[clab] = tmp_hist_probabilty[clab]/tot_weight
    return out_hist_probabilty

def tbl_hist_probabilty(table_dct, table_freq_dct):
    tbl_prob = dict()
    for ctag in table_dct:
        ctag_tbl_dict = table_dct[ctag]
        if ctag in table_freq_dct:
            ctag_tbl_Freq = table_freq_dct[ctag]
            chist_prob_dct = calc_hist_probabilty(ctag_tbl_dict, ctag_tbl_Freq)
            tbl_prob[ctag] = chist_prob_dct
    return tbl_prob


def calc_match_hist(tbl_hist_prob, hist_prob):
    out_prob = 0.0
    for clab, score in hist_prob.items():
        if clab in tbl_hist_prob:
            tbl_score = tbl_hist_prob[clab]
            cprob = score*tbl_score
            out_prob += cprob
    return out_prob

def match_tbl_hist(tbl_hist_prob, hist_probabilty):
    match_prob_vec = list()
    for ctag in tbl_hist_prob:
        ctbl_hist_probs = tbl_hist_prob[ctag]
        cprob = calc_match_hist(ctbl_hist_probs, hist_probabilty)
        match_prob_vec += [cprob]
    return match_prob_vec

def get_top_n_cats():
    return TOP_N_CATEGORIES

def calc_tot_values(tmp_cdct):
    ctot_ref = 0
    for ccat, cvalues_vec in tmp_cdct.items():
        if isinstance(cvalues_vec, list):
            clen = len(np.unique(cvalues_vec))
            ctot_ref += clen
            #if clen > 1:
            #    print("list", ccat, clen, ctot_ref)
        elif isinstance(cvalues_vec, float):
            clen = len(np.unique([cvalues_vec]))
            ctot_ref += clen
            if clen > 1:
                print("float", ccat, clen, ctot_ref)
    return ctot_ref


def complete_merged_dist(dist_all_vec, freq_dist_all_vec, add_complete=False, step=20):
    out_dist_vec = np.array([], dtype=np.float32)
    for ii in range(0, len(dist_all_vec), step):
        bvec = dist_all_vec[ii:ii+step]
        avec = freq_dist_all_vec[ii:ii+step]
        if len(out_dist_vec) == 0:
            out_dist_vec = np.concatenate((bvec, avec), axis=0)
            #print('ii, bvec, avec ', ii, bvec, avec, out_dist_vec)
        else:
            out_dist_vec = np.concatenate((out_dist_vec, bvec), axis=0)
            out_dist_vec = np.concatenate((out_dist_vec, avec), axis=0)
            #print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
       # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
       # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    #if do_norm:
    #    out_dist_vec = norm_all_dist(out_dist_vec)
    if add_complete:
        out_dist_vec = np.concatenate((out_dist_vec, 1.0-out_dist_vec), axis=None)
    return out_dist_vec

def update_dict(dct, ccat, values):
    values = list(np.unique(values))
    if len(values) > 0:
        if ccat not in dct:
            dct[ccat] = values
        else:
            cur_vals = list(np.unique(dct[ccat]))
            for cval in values:
                if cval not in cur_vals:
                    cur_vals += [cval]
            dct[ccat] = cur_vals
    return dct
