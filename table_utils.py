#import pandas as pd
import cv2
# define a simple data batch
import numpy as np
from os.path import exists #, isfile, join, dirname, basename, splitext
#from os import listdir, path, makedirs, scandir, remove, unlink, rmdir
np_epsf = np.finfo(float).eps
np_epsf32 = np.finfo(np.float32).eps

from image_utils import read_image_file, get_COLOR_BGR2RGB
from misc_utils import get_image_blocks, get_bulk_swapped, update_mat_list, update_feature_mat

#from signatureTableData import dict_table
from DataStaticClass import DataStaticClass
TOP_N_CATEGORIES = DataStaticClass.TOP_N_CATEGORIES
RESNET_LABELS = DataStaticClass.RESNET_LABELS
EPS_SAF = DataStaticClass.EPS_SAF
KNN_SIZE = DataStaticClass.KNN_SIZE
NUM_IMGS_PER_BATCH = DataStaticClass.NUM_IMGS_PER_BATCH

def calc_cost_hist_probabilty(label_vec, conf_vec):
    out_hist_probabilty = dict()
    for index, clab in enumerate(label_vec):
        score = conf_vec[index]
        out_hist_probabilty[clab] = score/len(label_vec)

    return out_hist_probabilty

def calc_hist_probabilty(hash_dict, normhashFreq, local_eps=np.finfo(float).eps):
    tmp_hist_probabilty = dict()
    out_hist_probabilty = dict()
    tot_weight = 0.0
    for clab, confidence_vec in hash_dict.items():
        conf_weight = len(confidence_vec)
        weigth_freq = local_eps
        if clab in normhashFreq:
            weigth_freq = normhashFreq[clab]
        cur_weight = weigth_freq * conf_weight
        tmp_hist_probabilty[clab] = cur_weight*np.mean(confidence_vec)
        tot_weight += cur_weight

    tot_weight = max(tot_weight, local_eps)
    for clab in tmp_hist_probabilty:
        out_hist_probabilty[clab] = tmp_hist_probabilty[clab]/tot_weight
    return out_hist_probabilty

def tbl_hist_probabilty(table_dct, table_freq_dct):
    tbl_prob = dict()
    for ctag in table_dct:
        ctag_tbl_dict = table_dct[ctag]
        if ctag in table_freq_dct:
            ctag_tbl_Freq = table_freq_dct[ctag]
            chist_prob_dct = calc_hist_probabilty(ctag_tbl_dict, ctag_tbl_Freq)
            tbl_prob[ctag] = chist_prob_dct
    return tbl_prob


def calc_match_hist(tbl_hist_prob, hist_prob):
    out_prob = 0.0
    for clab, score in hist_prob.items():
        if clab in tbl_hist_prob:
            tbl_score = tbl_hist_prob[clab]
            cprob = score*tbl_score
            out_prob += cprob
    return out_prob

def match_tbl_hist(tbl_hist_prob, hist_probabilty):
    match_prob_vec = list()
    for ctag in tbl_hist_prob:
        ctbl_hist_probs = tbl_hist_prob[ctag]
        cprob = calc_match_hist(ctbl_hist_probs, hist_probabilty)
        match_prob_vec += [cprob]
    return match_prob_vec

def get_top_n_cats():
    return TOP_N_CATEGORIES

def calc_tot_values(tmp_cdct):
    ctot_ref = 0
    for ccat, cvalues_vec in tmp_cdct.items():
        if isinstance(cvalues_vec, list):
            clen = len(np.unique(cvalues_vec))
            ctot_ref += clen
            #if clen > 1:
            #    print("list", ccat, clen, ctot_ref)
        elif isinstance(cvalues_vec, float):
            clen = len(np.unique([cvalues_vec]))
            ctot_ref += clen
            if clen > 1:
                print("float", ccat, clen, ctot_ref)
    return ctot_ref



def batch_tags_scores_crops(cur_val_crop_vec, Dictionary=DataStaticClass):
    labels_topn_mat = []
    scores_topn_mat = []
    image_vec = []
    for index, cval in enumerate(cur_val_crop_vec):
        src_imgfilename = cval[0]
        src_isexist = exists(src_imgfilename)
        if not src_isexist:
            print('skip no src_imgfilename not found! index, src_imgfilename = ', index, src_imgfilename)
            continue
        try:
            img = cv2.imread(src_imgfilename)
            if img is None:
                print('skip img is None index, src_imgfilename ', index, src_imgfilename)
                continue
            bbx = cval[1]
            cmin, rmin, cmax, rmax = bbx
            crop_img = img[rmin:rmax, cmin:cmax].copy()
            dims = crop_img.shape
            if crop_img is None or dims[0] < 1 or dims[1] < 1:
                cvals = (src_imgfilename, bbx, dims)
                print("except crop : skip index, cvals = ", index, index, cvals)
                continue
        except:
            print("except: skip index, src_imgfilename ", index, src_imgfilename)
            continue
        image_vec += [get_COLOR_BGR2RGB(crop_img)]


    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec)

    if len(image_vec) > 0:
        del image_vec
    # print('len batch_image_blocks = ', len(batch_image_blocks))
    # print('len img_pieces_vec = ', len(img_pieces_vec))
    # print('img_pieces_vec = ', img_pieces_vec)
    BATCH_SIZE = Dictionary.BATCH_SIZE
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('max_valid_img, max_valid_blk = ', max_valid_img, max_valid_blk)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('1batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
    else:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('2batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
            max_valid_blk = to_blk

        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        swapped_image_blocks = get_bulk_swapped(batch_Images)
        #if len(batch_Images) > 0:
        #    del batch_Images
        confidence_mat = []
        SHAPE_ONE = Dictionary.SHAPE_ONE
        for swapped_image_blk in swapped_image_blocks:
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            #prob_vec = get_mxnet_prediction(in_blk)
            prob_vec = Dictionary.predict_one_data(in_blk)
            confidence_mat = update_feature_mat(prob_vec, confidence_mat)
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks

        # print('3batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
        cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
        del confidence_mat
        labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
        scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)

    return labels_topn_mat, scores_topn_mat, img_pieces_vec

def batch_tags_scores(imgfilename_vec, Dictionary=DataStaticClass, use_cv=True):
    labels_topn_mat = []
    scores_topn_mat = []
    image_vec = []
    for imgfilename in imgfilename_vec:
        try:
            cimg = read_image_file(imgfilename, use_cv)
            if cimg is None:
                print('cimg is None skip get_feature_tags_scores_batch cimg is None ', imgfilename)
                continue
        except:
            print('except: skip get_feature_tags_scores_batch ', imgfilename)
            continue
        image_vec += [get_COLOR_BGR2RGB(cimg)]

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec)

    if len(image_vec) > 0:
        del image_vec
    # print('len batch_image_blocks = ', len(batch_image_blocks))
    # print('len img_pieces_vec = ', len(img_pieces_vec))
    # print('img_pieces_vec = ', img_pieces_vec)
    BATCH_SIZE = Dictionary.BATCH_SIZE
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('max_valid_img, max_valid_blk = ', max_valid_img, max_valid_blk)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('1batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
    else:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('2batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
            max_valid_blk = to_blk

        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        swapped_image_blocks = get_bulk_swapped(batch_Images)
        #if len(batch_Images) > 0:
        #    del batch_Images
        confidence_mat = []
        SHAPE_ONE = Dictionary.SHAPE_ONE
        for swapped_image_blk in swapped_image_blocks:
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            #prob_vec = get_mxnet_prediction(in_blk)
            prob_vec = Dictionary.predict_one_data(in_blk)
            confidence_mat = update_feature_mat(prob_vec, confidence_mat)
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks

        # print('3batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
        cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
        del confidence_mat
        labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
        scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)

    return labels_topn_mat, scores_topn_mat, img_pieces_vec

def image_batch_tags_scores(img_vec, Dictionary=DataStaticClass):
    labels_topn_mat = []
    scores_topn_mat = []
    image_vec = []
    for index, cimg in enumerate(img_vec):
        if cimg is None:
            print('cimg is None skip batch_tags_scores cimg is None index = ', index)
            continue
        image_vec += [get_COLOR_BGR2RGB(cimg)]

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec)

    if len(img_vec) > 0:
        del img_vec
    if len(image_vec) > 0:
        del image_vec
    # print('len batch_image_blocks = ', len(batch_image_blocks))
    # print('len img_pieces_vec = ', len(img_pieces_vec))
    # print('img_pieces_vec = ', img_pieces_vec)
    BATCH_SIZE = Dictionary.BATCH_SIZE
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('max_valid_img, max_valid_blk = ', max_valid_img, max_valid_blk)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('1batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
    else:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_Images) > 0:
            #    del batch_Images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            #get_batch_mxnet_prediction
            confidence_mat = Dictionary.predict_batch_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            # print('2batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
            cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
            labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)
            del confidence_mat
            max_valid_blk = to_blk

        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        swapped_image_blocks = get_bulk_swapped(batch_Images)
        #if len(batch_Images) > 0:
        #    del batch_Images
        confidence_mat = []
        SHAPE_ONE = Dictionary.SHAPE_ONE
        for swapped_image_blk in swapped_image_blocks:
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            #prob_vec = get_mxnet_prediction(in_blk)
            prob_vec = Dictionary.predict_one_data(in_blk)
            confidence_mat = update_feature_mat(prob_vec, confidence_mat)
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks

        # print('3batch_tags_scores len: confidence_mat = ', np.shape(confidence_mat)) #11221
        cur_labels_topn_mat, cur_scores_topn_mat = extract_all_topn_content(confidence_mat)
        del confidence_mat
        labels_topn_mat = update_mat_list(cur_labels_topn_mat, labels_topn_mat)
        scores_topn_mat = update_mat_list(cur_scores_topn_mat, scores_topn_mat)

    return labels_topn_mat, scores_topn_mat, img_pieces_vec



def add_missing_cats(labels_topn, scores_topn, prob_vec, sorted_scores_args, show=False):
    set_labs = set(labels_topn)
    set_scores = set(scores_topn)
    if len(set_labs) < TOP_N_CATEGORIES:
        if show:
            print('0 add_missing_cats: 0deb len(set_labs), len(set_scores) ', len(set_labs), len(set_scores))
        try:
            tmp_labs = list()
            tmp_scores = list()
            for ii, cid in enumerate(labels_topn):
                #cid = labels_topn[ii]
                probability = scores_topn[ii]
                if cid not in tmp_labs:
                    tmp_labs += [cid]
                    tmp_scores += [probability]
                else:
                    jj = tmp_labs.index(cid)
                    if tmp_scores[jj] < probability:
                        tmp_scores[jj] = probability
            if len(tmp_labs) < TOP_N_CATEGORIES:
                if show:
                    print('1 add_missing_cats: end deb len(tmp_labs), len(tmp_scores) ', len(tmp_labs), len(tmp_scores))
                index_rest = sorted_scores_args[TOP_N_CATEGORIES:]
                for ii in index_rest:
                    resnet_lab = RESNET_LABELS[ii]
                    cid = resnet_lab[10:]
                    probability = prob_vec[ii]
                    if cid not in tmp_labs:
                        tmp_labs += [cid]
                        tmp_scores += [probability]
                        if len(tmp_labs) == TOP_N_CATEGORIES:
                            break
                if len(tmp_labs) < TOP_N_CATEGORIES:
                    print('2 add_missing_cats: end deb len(tmp_labs), len(tmp_scores) ', len(tmp_labs), len(tmp_scores))
                    cid ='unknown'
                    probability = 0.5*min(tmp_scores)
                    if cid not in tmp_labs:
                        tmp_labs += [cid]
                        tmp_scores += [probability]
            labels_topn = tuple(tmp_labs)
            scores_topn = tuple(tmp_scores)
        except Exception as inst:
            print("add_missing_cats had exception on response =  ", inst)
    return labels_topn, scores_topn


def extract_all_topn_content(confidence_mat):
    # global mx, mod, Batch, RESNET_LABELS
    labels_topn_mat = []
    scores_topn_mat = []
    try:
        mat_dims = np.shape(confidence_mat)
        #print('debug extract_all_topn_content mat_dims = ', mat_dims)
        for index in range(mat_dims[0]):
            prob_vec = confidence_mat[index]
            prob_vec = np.squeeze(prob_vec)
            sorted_scores_args = np.argsort(prob_vec)[::-1]
            len_scores = len(sorted_scores_args)
            topn = TOP_N_CATEGORIES
            if TOP_N_CATEGORIES > len_scores:
                topn = len_scores

            index_topn = sorted_scores_args[0:topn]
            tmplst = [RESNET_LABELS[i] for i in index_topn]
            labels_topn = [clab_prefix[10:] for clab_prefix in tmplst]
            scores_topn = [prob_vec[i] for i in index_topn]
            if len(set(labels_topn)) < TOP_N_CATEGORIES:
                labels_topn, scores_topn = add_missing_cats(labels_topn, scores_topn, prob_vec, sorted_scores_args)
            labels_topn_mat = update_feature_mat(labels_topn, labels_topn_mat)
            scores_topn_mat = update_feature_mat(scores_topn, scores_topn_mat)
    except Exception as inst:
        print("extract_all_topn_content had exception on : ", inst)

    return labels_topn_mat, scores_topn_mat


def get_topn_from_all(labels_topn_all, scores_topn_all, max_cats=TOP_N_CATEGORIES):
    labels_topn_out = tuple()
    scores_topn_out = tuple()
    max_cats = min(len(labels_topn_all), max_cats)
    if max_cats > 0:
        sorted_scores_args = np.argsort(scores_topn_all)[::-1]
        inds = sorted_scores_args[:max_cats]
        tmp_labs = []
        tmp_scores = []
        for ii in inds:
            cid = labels_topn_all[ii]
            probability = scores_topn_all[ii]
            tmp_labs += [cid]
            tmp_scores += [probability]
        labels_topn_out = tuple(tmp_labs)
        scores_topn_out = tuple(tmp_scores)

    return labels_topn_out, scores_topn_out

def most_topn_from_all(labels_topn_all, scores_topn_all, max_cats=TOP_N_CATEGORIES):
    sorted_scores_args = np.argsort(scores_topn_all)[::-1]
    tmp_labs = []
    tmp_scores = []
    for ii in sorted_scores_args:
        if len(tmp_labs) == max_cats:
            break
        else:
            #print("most_topn_from_all deb ii ", ii, np.shape(sorted_scores_args),
            #      type(labels_topn_all), len(labels_topn_all), max_cats)
            cid = labels_topn_all[ii]
            probability = scores_topn_all[ii]
            if cid not in tmp_labs:
                tmp_labs += [cid]
                tmp_scores += [probability]
            else:
                jj = tmp_labs.index(cid)
                if tmp_scores[jj] < probability:
                    tmp_scores[jj] = probability

    if len(tmp_labs) < max_cats:
        max_cats = min(len(labels_topn_all), max_cats)
        if max_cats > 0:
            inds = sorted_scores_args[:max_cats]
            for ii in inds:
                if len(tmp_labs) == max_cats:
                    break
                else:
                    cid = labels_topn_all[ii]
                    cprob = scores_topn_all[ii]
                    # if cid not in tmp_labs or cprob not in tmp_scores:
                    tmp_labs += [cid]
                    tmp_scores += [cprob]

    labels_topn_out = tuple(tmp_labs)
    scores_topn_out = tuple(tmp_scores)
    return labels_topn_out, scores_topn_out

def get_topn_tags_scores_mat(labels_topn_mat_all, scores_topn_mat_all, img_pieces_vec, max_cats=TOP_N_CATEGORIES):
    labels_topn_mat = []
    scores_topn_mat = []
    startat = 0
    for index in range(len(img_pieces_vec)):
        pieces = img_pieces_vec[index]
        endat = startat + pieces
        labels_topns = labels_topn_mat_all[startat]
        scores_topns = scores_topn_mat_all[startat]
        for kk in range(startat + 1, endat):
            labels_topns += labels_topn_mat_all[kk]
            scores_topns += scores_topn_mat_all[kk]
        labels_topn, scores_topn = most_topn_from_all(labels_topns, scores_topns, max_cats)
        labels_topn_mat = update_mat_list(labels_topn, labels_topn_mat)
        scores_topn_mat = update_mat_list(scores_topn, scores_topn_mat)
        startat = endat
    return labels_topn_mat, scores_topn_mat


def knn_match_cost(tpl_vec, confidence):
    dref = tuple([abs(x - confidence) for x in tpl_vec])
    min_match = max(min(dref), EPS_SAF)
    knn = min(len(dref), KNN_SIZE)
    if knn > 1:
        saf_match = 10.0 * min_match
        accum_match = 0
        sorted_dref_args = np.argsort(dref)
        inds_dref = sorted_dref_args[:knn]
        den = 0
        for ii in inds_dref:
            cmnp = max(dref[ii], EPS_SAF)
            if cmnp < saf_match:
                accum_match += cmnp
                den += 1
            else:
                break
        min_match = accum_match / den
        cost = 1.0 - min_match
    else:
        cost = 1.0 - min_match
    return cost




def is_cost_valid(total_cost_vec, local_eps=2.22e-16):
    max_abs_val = max([abs(cval) for cval in total_cost_vec])
    if max_abs_val > (1.0 + local_eps):
        print('err: is_cost_valid not valid total_cost_vec = ', total_cost_vec)
        return False
    return True


def norm_cost(total_cost_vec_in):
    denom = max(TOP_N_CATEGORIES, max(total_cost_vec_in), 1.0)
    total_cost_vec = np.array([cval / denom for cval in total_cost_vec_in], dtype=np.float32)
    return total_cost_vec


def norm_freq_cost(total_cost_vec_in, local_eps=2.22e-16):
    max_abs_val = max([abs(x) for x in total_cost_vec_in])
    denom = max(max_abs_val, local_eps)
    total_cost_vec = np.array([cval / denom for cval in total_cost_vec_in], dtype=np.float32)
    return total_cost_vec


def norm_all_dist(dist_all_vec, local_eps=2.22e-16):
    max_abs_val = max([abs(x) for x in dist_all_vec])
    denom = max(max_abs_val, local_eps)
    out_dist_vec = np.array([cval / denom for cval in dist_all_vec], dtype=np.float32)
    return out_dist_vec

def get_merged_cost(norm_cost_vec, norm_freq_vec, alpha=0.25, len_tables_vec=18):
    norm_all_vec = np.array([], dtype=np.float32)
    #length = (len(norm_cost_vec) - 1) // 3 + 1
    beta = 1.0 - alpha
    for ii in range(0, len(norm_cost_vec), 3):
        bvec = norm_cost_vec[ii:ii + 3]
        avec = norm_freq_vec[ii:ii + 3]
        if len(norm_all_vec) == 0:
            norm_all_vec = np.concatenate((bvec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
        else:
            norm_all_vec = np.concatenate((norm_all_vec, bvec), axis=0)
            norm_all_vec = np.concatenate((norm_all_vec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
    # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
    # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    return norm_all_vec


def complete_merged_cost(norm_cost_vec, norm_freq_vec, alpha=0.25, len_tables_vec=18):
    norm_all_vec = np.array([], dtype=np.float32)
    beta = 1.0 - alpha
    for ii in range(0, len(norm_cost_vec), 3):
        bvec = norm_cost_vec[ii:ii + 3]
        avec = norm_freq_vec[ii:ii + 3]
        if len(norm_all_vec) == 0:
            norm_all_vec = np.concatenate((bvec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
        else:
            norm_all_vec = np.concatenate((norm_all_vec, bvec), axis=0)
            norm_all_vec = np.concatenate((norm_all_vec, avec), axis=0)
            # print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
    # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
    # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    comp_norm_all_vec = np.concatenate((norm_all_vec, 1.0 - norm_all_vec), axis=None)
    return comp_norm_all_vec


def knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec):
    #global table_dct, mid_vec
    sz = 3 * len(table_dct)
    total_cost_vec = sz * [0]
    if not table_dct or len(table_dct) == 0:
        print('knn_match_cost_counter table_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_dct.items():
        if not cpecies:
            print('knn_match_cost_counter index, cpecies is empty => run statistical algo = ', index)
        else:
            tindex = 3 * index
            for labelIndex in range(len(labels_topn)):
                clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    val_vec = cpecies[clab]
                    if len(val_vec) > 0:
                        cost = knn_match_cost(tuple(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cost
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    total_cost_vec[tindex + 2] += cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        index += 1
    return total_cost_vec


def freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec, mid_vec):
    #global table_dct, table_freq_dct
    #global freq_mid_vec, mid_vec
    sz = 3 * len(table_freq_dct)
    total_cost_vec = sz * [0]
    if not table_freq_dct or len(table_freq_dct) == 0:
        print('freq_weight_counter table_freq_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_freq_dct.items():
        if not cpecies:
            print('freq_weight_counter index, cpecies is empty => run statistical algo = ', index)
        else:
            ref_tbl = table_dct[ckey]
            tindex = 3 * index
            for labelIndex in range(len(labels_topn)):
                clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    cfreq = cpecies[clab]
                    val_vec = ref_tbl[clab]
                    if len(val_vec) > 0:
                        cost = knn_match_cost(tuple(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cfreq * cost
                    # total_cost_vec[tindex+1] += cfreq
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    creq = freq_mid_vec[index]
                    total_cost_vec[tindex + 2] += creq * cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        index += 1

    return total_cost_vec

def knn_match_cost_dist(tpl_vec, confidence):
    dref = tuple([abs(x - confidence) for x in tpl_vec])
    min_match = max(min(dref), EPS_SAF)
    knn = min(len(dref), KNN_SIZE)
    if knn > 1:
        saf_match = 10.0 * min_match
        accum_match = 0
        sorted_dref_args = np.argsort(dref)
        inds_dref = sorted_dref_args[:knn]
        den = 0
        for ii in inds_dref:
            cmnp = max(dref[ii], EPS_SAF)
            if cmnp < saf_match:
                accum_match += cmnp
                den += 1
            else:
                break
        min_match = accum_match/den
        cost = 1.0 - min_match
    else:
        cost = 1.0 - min_match
    return cost, min_match

def knn_match_cost_counter_dist(labels_topn, scores_topn, table_dct, mid_vec):
    # global table_dct, mid_vec
    sz = 3 * len(table_dct)
    total_cost_vec = sz * [0]
    dist_all_vec = []  # [0]*len(labels_topn)*len(table_dct) # 20*6
    if not table_dct or len(table_dct) == 0:
        print('table_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_dct.items():
        if not cpecies:
            cur_dist_vec = list()
            print('index, cpecies is empty => run statistical algo = ', index)
        else:
            cur_dist_vec = [0.0] * len(labels_topn)
            tindex = 3 * index
            for labelIndex, clabel in enumerate(labels_topn):
                #clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    val_vec = cpecies[clab]
                    if len(val_vec) > 0:
                        cost, cdist = knn_match_cost_dist(tuple(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cost
                        cur_dist_vec[labelIndex] = cost
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    total_cost_vec[tindex + 2] += cost
                    cur_dist_vec[labelIndex] = -cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        if len(cur_dist_vec) > 0:
            dist_all_vec.extend(cur_dist_vec)
        index += 1
    return total_cost_vec, dist_all_vec


def freq_weight_counter_dist(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec, mid_vec):
    # global table_dct, table_freq_dct
    # global freq_mid_vec, mid_vec
    sz = 3 * len(table_freq_dct)
    total_cost_vec = sz * [0]
    dist_all_vec = []  # [0]*len(labels_topn)*len(table_dct) # 20*6
    if not table_freq_dct or len(table_freq_dct) == 0:
        print('table_freq_dct is empty => run statistical algo')
        return total_cost_vec
    index = 0
    for ckey, cpecies in table_freq_dct.items():
        if not cpecies:
            cur_dist_vec = list()
            print('index, cpecies is empty => run statistical algo = ', index)
        else:
            ref_tbl = table_dct[ckey]
            tindex = 3 * index
            cur_dist_vec = [0.0] * len(labels_topn)
            for labelIndex, clabel in enumerate(labels_topn):
                #clabel = labels_topn[labelIndex]
                confidence = scores_topn[labelIndex]
                clab = clabel.lower()
                if clab in cpecies:
                    cfreq = cpecies[clab]
                    val_vec = ref_tbl[clab]
                    if len(val_vec) > 0:
                        cost, cdist = knn_match_cost_dist(tuple(val_vec), confidence)
                        total_cost_vec[tindex + 1] += cfreq * cost
                        cur_dist_vec[labelIndex] = cfreq * cost
                    # total_cost_vec[tindex+1] += cfreq
                else:
                    dif_score = abs(mid_vec[index] - confidence)
                    cost = min(max(1.0 - dif_score, 0.0), 1.0)
                    creq = freq_mid_vec[index]
                    total_cost_vec[tindex + 2] += creq * cost
                    cur_dist_vec[labelIndex] = -creq * cost

            total_cost_vec[tindex] = total_cost_vec[tindex + 1] - total_cost_vec[tindex + 2]
        if len(cur_dist_vec) > 0:
            dist_all_vec.extend(cur_dist_vec)
        index += 1
    return total_cost_vec, dist_all_vec

def complete_merged_dist(dist_all_vec, freq_dist_all_vec, add_complete=False, step=20):
    out_dist_vec = np.array([], dtype=np.float32)
    for ii in range(0, len(dist_all_vec), step):
        bvec = dist_all_vec[ii:ii+step]
        avec = freq_dist_all_vec[ii:ii+step]
        if len(out_dist_vec) == 0:
            out_dist_vec = np.concatenate((bvec, avec), axis=0)
            #print('ii, bvec, avec ', ii, bvec, avec, out_dist_vec)
        else:
            out_dist_vec = np.concatenate((out_dist_vec, bvec), axis=0)
            out_dist_vec = np.concatenate((out_dist_vec, avec), axis=0)
            #print('ii, bvec, avec ', ii, bvec, avec, norm_all_vec)
       # norm_all_vec += beta*norm_cost_vec[ii:ii+3]
       # norm_all_vec += alpha*norm_freq_vec[ii:ii+3]
    #if do_norm:
    #    out_dist_vec = norm_all_dist(out_dist_vec)
    if add_complete:
        out_dist_vec = np.concatenate((out_dist_vec, 1.0-out_dist_vec), axis=None)
    return out_dist_vec

def update_dict(dct, ccat, values):
    values = list(np.unique(values))
    if len(values) > 0:
        if ccat not in dct:
            dct[ccat] = values
        else:
            cur_vals = list(np.unique(dct[ccat]))
            for cval in values:
                if cval not in cur_vals:
                    cur_vals  += [cval]
            dct[ccat] = cur_vals
    return dct
