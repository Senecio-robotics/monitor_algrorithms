import lightgbm
import numpy
import pandas

from logging_utils import LOG
from response_utils import MlException

def load_lgbm_model(modelPath):
    try:
        print("lgbm.load started: " + modelPath)
        LGBM_Model = lightgbm.Booster(model_file=modelPath)
        LOG.info("LGBM loaded successfully:" + modelPath)
        print("lgbm.load completed: " + modelPath)
    except Exception as e:
        LOG.exception(e)
        raise MlException('Fail to load LGBM file: [{}]'.format(modelPath))
        #print('failed: LGBM_Model not loaded ', fullfile_lgbm)



def vector2DataFrame(dimensions: numpy.ndarray) -> pandas.DataFrame:
    dimensionsDataFrame = numpy.zeros((1, dimensions.size))
    for k in range(0, dimensions.size):
        dimensionsDataFrame[0, k] = dimensions[k]
    return pandas.DataFrame(data=dimensionsDataFrame)


def LGBM_predict(LGBM_MODEL, dataFrame0) -> float:
    try:
        prediction = LGBM_MODEL.predict(dataFrame0)
        #probability = prediction[0]
        return prediction
    except Exception as e:
        LOG.error(e)
        raise MlException('Fail LGBM predict: [{}]'.format(str(e)))


class LGBMService(object):
    def __init__(self, modelPath: str):
        try:
            print("lgbm.load started: " + modelPath)
            self.model = lightgbm.Booster(model_file=modelPath)
            LOG.info("LGBM loaded successfully:" + modelPath)
            print("lgbm.load completed: " + modelPath)
        except Exception as e:
            LOG.exception(e)
            raise MlException('Fail to load LGBM file: [{}]'.format(modelPath))

    def dataframe_predict(self, dataFrame0) -> float:
        try:
            prediction = self.model.predict(dataFrame0)
            #probability = prediction[0]
            return prediction
        except Exception as e:
            LOG.error(e)
            raise MlException('Fail LGBM predict: [{}]'.format(str(e)))

    def predict(self, dimensions: numpy.ndarray) -> float:
        try:
            prediction = self.predictions(dimensions)
            #probability = prediction[0]
            return prediction
        except Exception as e:
            LOG.error(e)
            raise MlException('Fail LGBM predict: [{}]'.format(str(e)))

    def predictions(self, dimensions: numpy.ndarray) -> numpy.ndarray:
        try:
            dataFrame = LGBMService.vectorToDataFrame(dimensions)
            prediction = self.model.predict(dataFrame)
            return prediction
        except Exception as e:
            LOG.error(e)
            raise MlException('Fail LGBM predict: [{}]'.format(str(e)))

    @staticmethod
    def vectorToDataFrame(dimensions: numpy.ndarray) -> pandas.DataFrame:
        dimensionsDataFrame = numpy.zeros((1, dimensions.size))
        for k in range(0, dimensions.size):
            dimensionsDataFrame[0, k] = dimensions[k]
        return pandas.DataFrame(data=dimensionsDataFrame)
