# define a simple data batch
import numpy as np
#import cv2
#from os.path import exists
from numpy_table_utils import numpy_extract_all_topn_content
from image_utils import read_image_file, get_COLOR_BGR2RGB
from numpy_misc_utils import get_image_blocks_numpy, parallel_image_blocks_numpy, get_bulk_swapped_numpy#, update_feature_mat , update_mat_list

#from embedFeatureData import dict_embed
from numpy_DataStaticClass import numpy_DataStaticClass
TOP_N_CATEGORIES = numpy_DataStaticClass.TOP_N_CATEGORIES
RESNET_LABELS = numpy_DataStaticClass.RESNET_LABELS
EPS_SAF = numpy_DataStaticClass.EPS_SAF
KNN_SIZE = numpy_DataStaticClass.KNN_SIZE

############## Parallel multiprocessing #######


def base_batch_features(index, batch_image_blocks):
    BATCH_SIZE = numpy_DataStaticClass.BATCH_SIZE
    from_blk = index * BATCH_SIZE
    to_blk = (index + 1) * BATCH_SIZE
    batch_Images = batch_image_blocks[from_blk:to_blk]
    swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
    cur_feature_mat = numpy_DataStaticClass.predict_features_batch(swapped_image_blocks)
    return cur_feature_mat

def build_batch_features(feature_mat, batch_image_blocks, batches_num, feature_ind,
                         mproc=None, n_proc=4, batchParallel=False):

    if batchParallel:

        print("Run build_batch_features mproc.Pool parallel ...")
        if mproc is None or n_proc < 1:
            print("Exit build_batch_features error mproc is None or n_proc < 1")
            return feature_mat, feature_ind
        with mproc.Pool(processes=n_proc) as pool:
            out_mat = pool.map(base_batch_features, range(0, batches_num), batch_image_blocks)
            for index in range(0, batches_num):
                cur_feature_mat = out_mat[index]
                if cur_feature_mat is None:
                    print('1 numpy_batch_img_features  skip: cur_feature_mat is None ! ')
                    continue
                for cfeature_vec in cur_feature_mat:
                    feature_mat[feature_ind] = cfeature_vec
                    feature_ind += 1

    else:

        for index in range(0, batches_num):  #todo parallel
            cur_feature_mat = base_batch_features(index, batch_image_blocks)
            if cur_feature_mat is None:
                print('1 numpy_batch_img_features  skip: cur_feature_mat is None !')
                continue
            # feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1

    return feature_mat, feature_ind


def parallel_batch_img_features(img_vec, mproc=None, n_proc=4, batchParallel=False, sliceParallel=False, disp=False):
    np_image_vec = np.full(len(img_vec), np.ndarray([]), dtype=np.ndarray)
    for index, cimg in enumerate(img_vec):  #todo parallel
        if cimg is None or len(cimg) == 0:
            print('skip numpy_batch_img_features: cimg is None or len(cimg) index,cimg = ', index, cimg)
            continue
        cimg_BGR2RGB = get_COLOR_BGR2RGB(cimg)
        if cimg_BGR2RGB is None or len(cimg_BGR2RGB) == 0:
            print('skip numpy_batch_img_features: cimg_BGR2RGB is None or len(cimg_BGR2RGB) index, cimg_BGR2RGB = ',
                  index, cimg_BGR2RGB)
            continue
        np_image_vec[index] = cimg_BGR2RGB.copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = parallel_image_blocks_numpy(np_image_vec,
                                                                                   sliceParallel=sliceParallel)

    #if len(np_image_vec) > 0:
    #    del np_image_vec
    if disp:
        print(' numpy_batch_img_features img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))
        print(' numpy_batch_img_features img_pieces_vec min, max, mean = ', min(img_pieces_vec), max(img_pieces_vec),
              sum(img_pieces_vec)/len(img_pieces_vec))

    BATCH_SIZE = numpy_DataStaticClass.BATCH_SIZE
    if disp:
        print("numpy_batch_img_features: BATCH_SIZE = ", BATCH_SIZE)

    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    feature_ind = 0
    if residual == 0:

        feature_mat, feature_ind = build_batch_features(feature_mat, batch_image_blocks, batches_num,
                             feature_ind, mproc=mproc, n_proc=n_proc, batchParallel=batchParallel)

    else:  # if residual > 0:
        max_valid_blk = batches_num * BATCH_SIZE
        feature_mat, feature_ind = build_batch_features(feature_mat, batch_image_blocks, batches_num,
                             feature_ind, mproc=mproc, n_proc=n_proc, batchParallel=batchParallel)
        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
        #if len(batch_images) > 0:
        #    del batch_images
        #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
        cur_feature_mat = numpy_DataStaticClass.predict_features_batch(swapped_image_blocks)
        # print('residual: dims: cur_feature_mat = ', np.shape(cur_feature_mat))
        if cur_feature_mat is None:
            print('3 numpy_batch_img_features  skip: cur_feature_mat is None !  ')
        else:
            # feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1

    return feature_mat, img_pieces_vec, img_dims_vec

def base_combined_tags_features(index, batch_image_blocks):

    BATCH_SIZE = numpy_DataStaticClass.BATCH_SIZE
    from_blk = index * BATCH_SIZE
    to_blk = (index + 1) * BATCH_SIZE
    batch_Images = batch_image_blocks[from_blk:to_blk]
    swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
    combined_mat = numpy_DataStaticClass.predict_combined_data(swapped_image_blocks)
    return combined_mat

def build_combined_tags_features(feature_mat, labels_topn_mat, scores_topn_mat, batch_image_blocks, batches_num,
                         mproc=None, n_proc=4, batchParallel=False):

    MAX_EMBED = numpy_DataStaticClass.MAX_EMBED
    BATCH_SIZE = numpy_DataStaticClass.BATCH_SIZE
    feature_ind = 0
    label_ind = 0
    score_ind = 0
    if batchParallel:
        print("Run build_batch_features mproc.Pool parallel ...")
        if mproc is None or n_proc < 1:
            print("Exit build_combined_tags_features error mproc is None or n_proc < 1")
            return feature_mat, labels_topn_mat, scores_topn_mat, feature_ind, label_ind, score_ind
        with mproc.Pool(processes=n_proc) as pool:
            out_mat = pool.map(base_combined_tags_features, range(0, batches_num), batch_image_blocks)
            for index in range(0, batches_num):
                combined_mat = out_mat[index]
                if combined_mat is None:
                    print('parallel build_combined_tags_features  skip: combined_mat is None ! ')
                    continue
                confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
                add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
                add_feature_mat = combined_mat[:, :MAX_EMBED]
                #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
                #del confidence_mat, combined_mat
                # max_valid_blk = to_blk
                #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
                #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
                #feature_mat = update_mat_list(add_feature_mat, feature_mat)
                for cfeature_vec in add_feature_mat:
                    #print("deb cfeature_vec ", np.shape(cfeature_vec))
                    feature_mat[feature_ind] = cfeature_vec
                    feature_ind += 1
                for clab_vec in add_labels_topn_mat:
                    #print("deb clab_vec ", np.shape(clab_vec))
                    labels_topn_mat[label_ind] = clab_vec
                    label_ind += 1
                for cscore_vec in add_scores_topn_mat:
                    #print("deb cscore_vec ", np.shape(cscore_vec))
                    scores_topn_mat[score_ind] = cscore_vec
                    score_ind += 1

    else:
        for index in range(0, batches_num): #todo parallel
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = numpy_DataStaticClass.predict_combined_data(swapped_image_blocks)
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            for cfeature_vec in add_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            for clab_vec in add_labels_topn_mat:
                labels_topn_mat[label_ind] = clab_vec
                label_ind += 1
            for cscore_vec in add_scores_topn_mat:
                scores_topn_mat[score_ind] = cscore_vec
                score_ind += 1
    return feature_mat, labels_topn_mat, scores_topn_mat, feature_ind, label_ind, score_ind

def parallel_batch_img_feature_tags_scores(img_vec, mproc=None, n_proc=4, batchParallel=False,
                                           sliceParallel=False, disp=False):
    np_image_vec = np.full(len(img_vec), np.ndarray([]), dtype=np.ndarray)
    for index, cimg in enumerate(img_vec): #todo parallel
        if cimg is None:
            print('cimg is None skip numpy_batch_img_feature_tags_scores cimg is None index = ', index)
            continue
        np_image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

 #   batch_image_blocks, img_pieces_vec, img_dims_vec = numpy_get_image_blocks_numpy(image_vec)
    batch_image_blocks, img_pieces_vec, img_dims_vec = parallel_image_blocks_numpy(np_image_vec,
                                                                                   sliceParallel=sliceParallel)

    #if len(np_image_vec) > 0:
    #    del np_image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))

    BATCH_SIZE = numpy_DataStaticClass.BATCH_SIZE
    if disp:
        print("numpy_batch_img_feature_tags_scores: BATCH_SIZE = ", BATCH_SIZE)
    MAX_EMBED = numpy_DataStaticClass.MAX_EMBED
    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    labels_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    scores_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:

        feature_mat, labels_topn_mat, scores_topn_mat, feature_ind, label_ind, score_ind = \
            build_combined_tags_features(feature_mat, labels_topn_mat, scores_topn_mat, batch_image_blocks, batches_num,
                                     mproc=mproc, n_proc=n_proc, batchParallel=batchParallel)

    else:  # if residual > 0:
        max_valid_blk = batches_num * BATCH_SIZE
        feature_mat, labels_topn_mat, scores_topn_mat, feature_ind, label_ind, score_ind = \
            build_combined_tags_features(feature_mat, labels_topn_mat, scores_topn_mat, batch_image_blocks, batches_num,
                                     mproc=mproc, n_proc=n_proc, batchParallel=batchParallel)
        # add one workers (chunk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        # LOG.debug('00size  batch_image_blocks[from_blk:to_blk] ', to_blk-from_blk, np.shape(batch_Images))
        # if len(batch_Images) < int(0.25*BATCH_SIZE):
        swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
        if len(batch_images) > 0:
            del batch_images
        add_feature_mat = numpy_DataStaticClass.predict_features_batch(swapped_image_blocks)
        #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
        SHAPE_ONE = numpy_DataStaticClass.SHAPE_ONE
        confidence_mat = np.full(len(swapped_image_blocks), np.ndarray([]), dtype=np.ndarray)
        for kk_sblk, swapped_image_blk in enumerate(swapped_image_blocks):
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            prob_vec = numpy_DataStaticClass.predict_one_data(in_blk)
            #confidence_mat = update_feature_mat(prob_vec, confidence_mat)
            confidence_mat[kk_sblk] = prob_vec
        # LOG.debug('33size confidence_mat, add_feature_mat ', np.shape(confidence_mat), np.shape(add_feature_mat))
        add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
        #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
        #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
        #feature_mat = update_mat_list(add_feature_mat, feature_mat)
        for cfeature_vec in add_feature_mat:
            feature_mat[feature_ind] = cfeature_vec
            feature_ind += 1
        for clab_vec in add_labels_topn_mat:
            labels_topn_mat[label_ind] = clab_vec
            label_ind += 1
        for cscore_vec in add_scores_topn_mat:
            scores_topn_mat[score_ind] = cscore_vec
            score_ind += 1
        # LOG.debug('3size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
        # feature_mat = np.concatenate((feature_mat, add_feature_mat), axis=0)
        # labels_topn_mat = np.concatenate((labels_topn_mat, add_labels_topn_mat), axis=0)
        # scores_topn_mat = np.concatenate((scores_topn_mat, add_scores_topn_mat), axis=0)
    return feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec

############## End Parallel multiprocessing #######

def numpy_batch_img_features(img_vec, Dictionary=numpy_DataStaticClass, do_rescale=True, use_slices=True, disp=False):
    np_image_vec = np.full(len(img_vec), np.ndarray([]), dtype=np.ndarray)
    for index, cimg in enumerate(img_vec):  #todo parallel
        if cimg is None or len(cimg) == 0:
            print('skip numpy_batch_img_features: cimg is None or len(cimg) index,cimg = ', index, cimg)
            continue
        cimg_BGR2RGB = get_COLOR_BGR2RGB(cimg)
        if cimg_BGR2RGB is None or len(cimg_BGR2RGB) == 0:
            print('skip numpy_batch_img_features: cimg_BGR2RGB is None or len(cimg_BGR2RGB) index, cimg_BGR2RGB = ',
                  index, cimg_BGR2RGB)
            continue
        np_image_vec[index] = cimg_BGR2RGB.copy()


    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks_numpy(np_image_vec, do_rescale=do_rescale,
                                                                              use_slices=use_slices)

    if len(np_image_vec) > 0:
        del np_image_vec
    if disp:
        print(' numpy_batch_img_features img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))
        print(' numpy_batch_img_features img_pieces_vec min, max, mean = ', min(img_pieces_vec), max(img_pieces_vec),
              sum(img_pieces_vec)/len(img_pieces_vec))
    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("numpy_batch_img_features: BATCH_SIZE = ", BATCH_SIZE)
    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    feature_ind = 0
    empty_feature_ind = 0
    if residual == 0:
        for index in range(0, batches_num):#todo parallel
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            #try:
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
            #except:
           #     print("exception: from_blk:to_blk ", from_blk, to_blk)
           #     print("exception: batch_Images ", np.shape(batch_Images))
           #     for debimg in batch_Images:
           #         print("exception: debimg ", debimg, np.shape(debimg), type(debimg))
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                empty_feature_ind += 1
                print('1 numpy_batch_img_features  skip: cur_feature_mat is None !empty_feature_ind = ',
                      empty_feature_ind)
                continue
            #feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):#todo parallel
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                empty_feature_ind += 1
                print('2 numpy_batch_img_features  skip: cur_feature_mat is None ! empty_feature_ind = ',
                      empty_feature_ind)
                continue
            #feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            max_valid_blk = to_blk
        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
        #if len(batch_images) > 0:
        #    del batch_images
        #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
        cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        # print('residual: dims: cur_feature_mat = ', np.shape(cur_feature_mat))
        if cur_feature_mat is None:
            empty_feature_ind += 1
            print('3 numpy_batch_img_features  skip: cur_feature_mat is None ! empty_feature_ind = ',
                  empty_feature_ind)
        else:
            # feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1

    if empty_feature_ind > 0:
        st0 = len(feature_mat)-empty_feature_ind
        mask = np.ones(len(feature_mat), dtype=bool)
        false_vec = [ii_rm for ii_rm in range(st0, len(feature_mat))]
        print("numpy_batch_img_features: delete empty lines false_vec .. ", empty_feature_ind, false_vec)
        mask[false_vec] = False
        feature_mat = feature_mat[mask]
    return feature_mat, img_pieces_vec, img_dims_vec

def numpy_batch_imgfilename_features(imgfilename_vec, Dictionary=numpy_DataStaticClass, use_cv=True, do_rescale=True,
                                     use_slices=True, disp=False):
    #feature_mat = []
    np_image_vec = np.full(len(imgfilename_vec), np.ndarray([]), dtype=np.ndarray)
    for index, imgfilename in enumerate(imgfilename_vec):
        try:
            cimg = read_image_file(imgfilename, use_cv)
            if cimg is None:
                print('cimg is None skip numpy_batch_imgfilename_features cimg is None ', imgfilename)
                continue
        except:
            print('except: skip numpy_batch_imgfilename_features ', imgfilename)
            continue
        np_image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks_numpy(np_image_vec, do_rescale=do_rescale, use_slices=use_slices)

    if len(np_image_vec) > 0:
        del np_image_vec
    if disp:
        print(' numpy_batch_imgfilename_features img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))
        print(' numpy_batch_imgfilename_features img_pieces_vec min, max, mean = ', min(img_pieces_vec),
              max(img_pieces_vec), sum(img_pieces_vec)/len(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("numpy_batch_imgfilename_features: BATCH_SIZE = ", BATCH_SIZE)
    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    feature_ind = 0
    empty_feature_ind = 0
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                empty_feature_ind += 1
                print('1 numpy_batch_imgfilename_features  skip: cur_feature_mat is None !empty_feature_ind = ',
                      empty_feature_ind)
                continue
            #feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                empty_feature_ind += 1
                print('2 numpy_batch_imgfilename_features  skip: cur_feature_mat is None ! empty_feature_ind = ',
                      empty_feature_ind)
                continue
            #feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            max_valid_blk = to_blk
        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        swapped_image_blocks = get_bulk_swapped_numpy(batch_Images)
        #if len(batch_images) > 0:
        #    del batch_images
        #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
        cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        # print('residual: dims: cur_feature_mat = ', np.shape(cur_feature_mat))
        if cur_feature_mat is None:
            empty_feature_ind += 1
            print('3 numpy_batch_imgfilename_features  skip: cur_feature_mat is None ! empty_feature_ind = ',
                  empty_feature_ind)
        else:
            #feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            for cfeature_vec in cur_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1

    if empty_feature_ind > 0:
        st0 = len(feature_mat)-empty_feature_ind
        mask = np.ones(len(feature_mat), dtype=bool)
        false_vec = [ii_rm for ii_rm in range(st0, len(feature_mat))]
        print("numpy_batch_imgfilename_features: delete empty lines false_vec .. ", empty_feature_ind, false_vec)
        mask[false_vec] = False
        feature_mat = feature_mat[mask]
    return feature_mat

def numpy_batch_img_feature_tags_scores(img_vec, Dictionary=numpy_DataStaticClass, disp=False):
    np_image_vec = np.full(len(img_vec), np.ndarray([]), dtype=np.ndarray)
    for index, cimg in enumerate(img_vec): #todo parallel
        if cimg is None:
            print('cimg is None skip numpy_batch_img_feature_tags_scores cimg is None index = ', index)
            continue
        np_image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

 #   batch_image_blocks, img_pieces_vec, img_dims_vec = numpy_get_image_blocks_numpy(image_vec)
    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks_numpy(np_image_vec)

    if len(np_image_vec) > 0:
        del np_image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("numpy_batch_img_feature_tags_scores: BATCH_SIZE = ", BATCH_SIZE)
    MAX_EMBED = Dictionary.MAX_EMBED
    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    labels_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    scores_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    feature_ind = 0
    label_ind = 0
    score_ind = 0
    if residual == 0:
        for index in range(0, batches_num): #todo parallel
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            # max_valid_blk = to_blk
            #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            #feature_mat = update_mat_list(add_feature_mat, feature_mat)
            for cfeature_vec in add_feature_mat:
                #print("deb cfeature_vec ", np.shape(cfeature_vec))
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            for clab_vec in add_labels_topn_mat:
                #print("deb clab_vec ", np.shape(clab_vec))
                labels_topn_mat[label_ind] = clab_vec
                label_ind += 1
            for cscore_vec in add_scores_topn_mat:
                #print("deb cscore_vec ", np.shape(cscore_vec))
                scores_topn_mat[score_ind] = cscore_vec
                score_ind += 1

    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num): #todo parallel
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            #feature_mat = update_mat_list(add_feature_mat, feature_mat)
            for cfeature_vec in add_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            for clab_vec in add_labels_topn_mat:
                labels_topn_mat[label_ind] = clab_vec
                label_ind += 1
            for cscore_vec in add_scores_topn_mat:
                scores_topn_mat[score_ind] = cscore_vec
                score_ind += 1
            # LOG.debug('2size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
            max_valid_blk = to_blk
        # add one workers (chunk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        # LOG.debug('00size  batch_image_blocks[from_blk:to_blk] ', to_blk-from_blk, np.shape(batch_Images))
        # if len(batch_Images) < int(0.25*BATCH_SIZE):
        swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
        if len(batch_images) > 0:
            del batch_images
        add_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
        SHAPE_ONE = Dictionary.SHAPE_ONE
        confidence_mat = np.full(len(swapped_image_blocks), np.ndarray([]), dtype=np.ndarray)
        for kk_sblk, swapped_image_blk in enumerate(swapped_image_blocks):
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            prob_vec = Dictionary.predict_one_data(in_blk)
            #confidence_mat = update_feature_mat(prob_vec, confidence_mat)
            confidence_mat[kk_sblk] = prob_vec
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks
        # LOG.debug('33size confidence_mat, add_feature_mat ', np.shape(confidence_mat), np.shape(add_feature_mat))
        add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
        del confidence_mat
        #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
        #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
        #feature_mat = update_mat_list(add_feature_mat, feature_mat)
        for cfeature_vec in add_feature_mat:
            feature_mat[feature_ind] = cfeature_vec
            feature_ind += 1
        for clab_vec in add_labels_topn_mat:
            labels_topn_mat[label_ind] = clab_vec
            label_ind += 1
        for cscore_vec in add_scores_topn_mat:
            scores_topn_mat[score_ind] = cscore_vec
            score_ind += 1
        # LOG.debug('3size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
        # feature_mat = np.concatenate((feature_mat, add_feature_mat), axis=0)
        # labels_topn_mat = np.concatenate((labels_topn_mat, add_labels_topn_mat), axis=0)
        # scores_topn_mat = np.concatenate((scores_topn_mat, add_scores_topn_mat), axis=0)
    return feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec

def numpy_batch_imgfilename_feature_tags_scores(imgfilename_vec, Dictionary=numpy_DataStaticClass, use_cv=True, disp=False):

    np_image_vec = np.full(len(imgfilename_vec), np.ndarray([]), dtype=np.ndarray)
    for index, imgfilename in enumerate(imgfilename_vec):
        try:
            cimg = read_image_file(imgfilename, use_cv)
            if cimg is None:
                print('cimg is None skip batch_imgfilename_feature_tags_scores cimg is None ', imgfilename)
                continue
        except:
            print('except: skip batch_imgfilename_feature_tags_scores ', imgfilename)
            continue
        np_image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks_numpy(np_image_vec)
    if len(np_image_vec) > 0:
        del np_image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("batch_imgfilename_feature_tags_scores: BATCH_SIZE = ", BATCH_SIZE)
    MAX_EMBED = Dictionary.MAX_EMBED
    feature_mat = np.full((len(batch_image_blocks), 2048), np.ndarray([]), dtype=np.ndarray)
    labels_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    scores_topn_mat = np.full((len(batch_image_blocks), TOP_N_CATEGORIES), np.ndarray([]), dtype=np.ndarray)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    feature_ind = 0
    label_ind = 0
    score_ind = 0
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = numpy_extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            # max_valid_blk = to_blk
            #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            #feature_mat = update_mat_list(add_feature_mat, feature_mat)
            for cfeature_vec in add_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            for clab_vec in add_labels_topn_mat:
                labels_topn_mat[label_ind] = clab_vec
                label_ind += 1
            for cscore_vec in add_scores_topn_mat:
                scores_topn_mat[score_ind] = cscore_vec
                score_ind += 1

    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat =  numpy_extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            #feature_mat = update_mat_list(add_feature_mat, feature_mat)
            # LOG.debug('2size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
            for cfeature_vec in add_feature_mat:
                feature_mat[feature_ind] = cfeature_vec
                feature_ind += 1
            for clab_vec in add_labels_topn_mat:
                labels_topn_mat[label_ind] = clab_vec
                label_ind += 1
            for cscore_vec in add_scores_topn_mat:
                scores_topn_mat[score_ind] = cscore_vec
                score_ind += 1
            max_valid_blk = to_blk
        # add one workers (chunk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        # LOG.debug('00size  batch_image_blocks[from_blk:to_blk] ', to_blk-from_blk, np.shape(batch_Images))
        # if len(batch_Images) < int(0.25*BATCH_SIZE):
        swapped_image_blocks = get_bulk_swapped_numpy(batch_images)
        if len(batch_images) > 0:
            del batch_images
        add_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        #add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
        SHAPE_ONE = Dictionary.SHAPE_ONE
        confidence_mat = np.full(len(swapped_image_blocks), np.ndarray([]), dtype=np.ndarray)
        for kk_sblk, swapped_image_blk in enumerate(swapped_image_blocks):
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            prob_vec = Dictionary.predict_one_data(in_blk)
            #confidence_mat = update_feature_mat(prob_vec, confidence_mat)
            confidence_mat[kk_sblk] = prob_vec
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks
        # LOG.debug('33size confidence_mat, add_feature_mat ', np.shape(confidence_mat), np.shape(add_feature_mat))
        add_labels_topn_mat, add_scores_topn_mat =  numpy_extract_all_topn_content(confidence_mat)
        del confidence_mat
        #labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
        #scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
        #feature_mat = update_mat_list(add_feature_mat, feature_mat)
        for cfeature_vec in add_feature_mat:
            feature_mat[feature_ind] = cfeature_vec
            feature_ind += 1
        for clab_vec in add_labels_topn_mat:
            labels_topn_mat[label_ind] = clab_vec
            label_ind += 1
        for cscore_vec in add_scores_topn_mat:
            scores_topn_mat[score_ind] = cscore_vec
            score_ind += 1
        # LOG.debug('3size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
        # feature_mat = np.concatenate((feature_mat, add_feature_mat), axis=0)
        # labels_topn_mat = np.concatenate((labels_topn_mat, add_labels_topn_mat), axis=0)
        # scores_topn_mat = np.concatenate((scores_topn_mat, add_scores_topn_mat), axis=0)
    return feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec


