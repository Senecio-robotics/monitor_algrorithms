import cv2
import numpy as np
import requests
from requests.adapters import HTTPAdapter

from logging_utils import LOG
from response_utils import MlException
from PIL import Image
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

DEFAULT_IMAGE_SIZE = 224
DOWNLOAD_IMAGE_TIMEOUT = 10
REQUESTS_MAX_RETRIES = 2
REQUESTS_POOL_MAXSIZE = 100


def init_requests_session():
    session = requests.Session()
    http_adapter = requests.adapters.HTTPAdapter(pool_maxsize=REQUESTS_POOL_MAXSIZE, max_retries=REQUESTS_MAX_RETRIES)
    session.mount('http://', http_adapter)
    session.mount('https://', http_adapter)
    return session


REQUESTS_SESSION = init_requests_session()


# 1.1: Load an image file
def download_image(url, decode=True, convert_color=True):
    try:
        img = download(url)
        img = np.asarray(bytearray(img))
        if decode:
            img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        if convert_color:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return img
    except Exception as e:
        raise MlException('Fail to read image ({}): [{}]'.format(url, str(e)))


def download(url):
    try:
        img = REQUESTS_SESSION.get(url, timeout=DOWNLOAD_IMAGE_TIMEOUT)
        return img.content
    except Exception as e:
        LOG.error('Fail download file url=[{}] : [{}]'.format(url, str(e)))
        raise MlException('Fail to download file [{}]'.format(url))


# Used for MXNET: FIREARMS & WAR_&_TERROR (Alex + Olga)
def numpy_nd_array(img: np.ndarray):
    img = resize_min(img, DEFAULT_IMAGE_SIZE)
    img = np.swapaxes(img, 0, 2)
    img = np.swapaxes(img, 1, 2)
    img = img[np.newaxis, :]
    return img



# Used for MxNet: FIREARMS & WAR_&_TERROR (Alex + Olga) + ALCOHOL (Gabi)



def get_resize_img_dims(img, target_size=DEFAULT_IMAGE_SIZE):

    try:
        dims = np.shape(img)[0:2]
        if dims[0] != target_size or dims[1] != target_size:
            scaledImg = resize_min(img)
            return scaledImg, dims
        else:
            return img, dims
    except Exception as e:
        raise MlException('Fail to get_resize_img_dims  file : [{}]'.format(str(e)))

def pil_resize_image(image, size=None):
    """Resize an image to the given size."""
    if size is None:
        size = [DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE]
    return image.resize(size, Image.ANTIALIAS)


def resize_image_224(img, out_size=(DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE), use_scale=False):
    dims = np.shape(img)[0:2]
    if use_scale:
        scx = out_size[1] / dims[1]  # width ratio
        scy = out_size[0] / dims[0]  # height ratio
        img_out = cv2.resize(img, (0, 0), fx=scx, fy=scy)
    else:
        out_size = (out_size[1], out_size[0])  # (width, height)
        img_out = cv2.resize(img, out_size, interpolation=cv2.INTER_AREA)
    return img_out


def resize_min(img, size0=DEFAULT_IMAGE_SIZE, disp=False):
    dims = np.shape(img)[0:2]
    sc_max = size0 / max(dims)
    if sc_max >= 1:
        sc_min = size0 / min(dims)
        if sc_min > 1:  # or sc_max > 1:
            if disp:
                print('resize_min warning: sc >= 1 _max >= 1 dims, sc_max, sc_min = ', dims, sc_max, sc_min)
            out_size = (size0, size0)  # (width, height)
            img = cv2.resize(img, out_size, interpolation=cv2.INTER_AREA)
    else:
        sc_min = size0 / min(dims)
        img = cv2.resize(img, (0, 0), fx=sc_min, fy=sc_min)
    return img


def rescale_img(img, out_size_in=(DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE), use_min=True, use_scale_in=False):
    if use_min:
        return resize_min(img, size0=out_size_in[1])
    else:
        return resize_image_224(img, out_size=out_size_in, use_scale=use_scale_in)






def resize_min_pil(img, size0=DEFAULT_IMAGE_SIZE):
    """Resize an image to the given size. w, h = img.size"""
    w0, h0 = img.size
    dims = [w0, h0]
    sc_max = size0/max(dims)
    if sc_max >= 1:
        sc_min = size0/min(dims)
        if sc_min > 1:# or sc_max > 1:
            out_size = [DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE]  # (width, height)
            img = img.resize(out_size, Image.ANTIALIAS)
            if img.mode != 'RGB':
                img = img.convert("RGB")
    else:
        sc_min = size0/min(dims)
        width = int(float(w0)*float(sc_min)+0.5)
        height = int(float(h0)*float(sc_min)+0.5)
        out_size = [width, height]
        img = img.resize(out_size, Image.ANTIALIAS)
        if img.mode != 'RGB':
            img = img.convert("RGB")
    return np.array(img) #np.asarray(img) #cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)


def get_COLOR_BGR2RGB(img):
    try:
        img_out = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except Exception as inst:
        LOG.warn("cv2.cvtColor(img0, cv2.COLOR_BGR2RGB) had exception on get_COLOR_BGR2RGB: %s" % str(inst))
        img_out = None
        pass
    return img_out

def read_image_file(filename, use_cv=True):
    try:
        if use_cv:
            img_out = cv2.imread(filename)
        else:
            from PIL import Image
            img_out = Image.open(filename)
            if img_out is not None:
                if img_out.mode != 'RGB':
                    img_out = img_out.convert("RGB")
            img_out = np.array(img_out)  # PIL size width, height  => numpy size height, width

        if img_out is None:
            print("cv2.imread is None  try plt.imread on get_readimg: ")
            # example 8 bit: filename =
            import matplotlib.pyplot as plt
            img_out = plt.imread(filename)
            # plt.imshow(img_out)
            if img_out is None:
                print("plt.imread is None  try imageio.imread on get_readimg: ")
                import imageio
                img_out = imageio.imread(filename)

    except Exception as inst:
        print("cv2.imread had exception on get_imread: ", inst)
        img_out = None
        pass

    return img_out
