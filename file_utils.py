import csv
import json
import ntpath
import os
#import re
import pickle
import feather
#from shutil import copyfile, move
from os import listdir, path, makedirs #, scandir, remove, unlink, rmdir
from os.path import isfile, join, basename, splitext   #exists, dirname,

#from logging_utils import LOG
#from response_utils import MlException
import numpy as np

######  check/ protection utils
def read_csv_all_array(array_file, delimiter=',', skip=True):
    mapping = []
    try:
        print('csv.load [' + array_file + '] started')
        with open(array_file) as f:
            reader = csv.reader(f, delimiter=delimiter)
            if skip:
                for index, row in enumerate(reader):
                    if index < 1:
                        continue
                    mapping.append(row)
            else:
                for index, row in enumerate(reader):
                    mapping.append(row)
        print('csv.load [' + array_file + '] completed')
    except Exception as e:
        print("Cant process file: {}".format(str(array_file)), e)

    return mapping


def read_csv_array(array_file, delimiter=','):
    mapping = []
    try:
        print('csv.load [' + array_file + '] started')
        with open(array_file) as f:
            reader = csv.reader(f, delimiter=delimiter)
            for row in reader:
                mapping.append(row[0])
        print('csv.load [' + array_file + '] completed')
    except Exception as e:
        print("Cant process file: {}".format(str(array_file)), e)

    return mapping


def read_csv_mapping(mapping_file, delimiter=';'):
    mapping = {}
    try:
        print('csv.load [' + mapping_file + '] started')
        with open(mapping_file) as f:
            reader = csv.reader(f, delimiter)
            for row in reader:
                mapping[row[0]] = row[1]
        print('csv.load [' + mapping_file + '] completed')
    except Exception as e:
        print("Cant process file: {}".format(str(mapping_file)), e)
    return mapping


def read_rstrip_csv_array(file, delimiter=';'):
    mapping = read_csv_array(array_file=file, delimiter=delimiter)
    retval = []
    for row in mapping:
        retval.append(row.rstrip())
    print('read_rstrip_csv_array out len(retval) = ', len(retval))
    return retval


def read_json(json_file):
    try:
        print('Json.load [' + json_file + '] started')
        with open(json_file) as f:
            content = json.load(f)
        print('Json.load [' + json_file + '] completed')
        return content
    except Exception as e:
        print("Cant process file: {}".format(str(json_file)), e)
        print('Fail to load json file: [{}]'.format(json_file))


def check_directory(directory: str):
    if not os.path.exists(directory):
        os.makedirs(directory)


def load_pickle_file(file_name: str):
    try:
        print("Pickle.load started: " + file_name)
        with open(file_name, 'rb') as handle:
            dictionary = pickle.load(handle)
        print("Pickle.load completed: " + file_name)
        return dictionary
    except Exception as e:
        print("Cant process file: {}".format(str(file_name)), e)
        print('Fail to load pickle file: [{}]'.format(file_name))


def external_path(directory, file_name):
    return directory + ntpath.basename(file_name)

def GetTupleFeatherData(featurefile):
    try:
        # load back to dict
        print("feather feture_mat :load started: " + featurefile)
        val_data = feather.read_dataframe(featurefile)
        #vals_keys = val_data.values
        #dims = vals_keys.shape
        vals_keys = val_data.values
        dims = vals_keys.shape
        print("GetTupleFeatherData dims = ", dims)
        blk_size = int(dims[0])
        iblk = 0
        irow0 = iblk * blk_size
        irowe = (iblk + 1) * blk_size
        # ckey = vals_keys[irow0][0]
        cval_mat = vals_keys[irow0:irowe]
        cval_mat_lst = cval_mat.tolist()
        out_data = tuple([cv[1:] for cv in cval_mat_lst])
        # print(np.shape(out_data))
        print("feather feture_mat: load completed: " + featurefile)
        return out_data
    except Exception as e:
        print("Cant process file: {}".format(str(featurefile)), e)
        print('Fail to load feather file: [{}]'.format(featurefile))


def list_feature_pickle_files(file_dir, ext=None):
    try:
        print("file_dir-collect started: " + file_dir)
        if ext is None:
            ext = ['.pickle', '.feather']
        file_list = listdir(file_dir)
        files_vec = []
        for file in file_list:
            fullfilename = join(file_dir, file)
            if file not in files_vec:
                if isfile(fullfilename) and file[file.rfind('.'):].lower() in ext:
                    files_vec += [file]
        print("file_dir-collect completed: len(files_vec) = " + str(len(files_vec)))
        return files_vec
    except Exception as e:
        print("Cant collect process file_dir: {}".format(str(file_dir)), e)
        print('Fail to collect file_dir: [{}]'.format(file_dir))


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

        # return json.dumps(data, cls=MyEncoder)
    # print(json.dumps(json_1, cls=MyEncoder))


def SaveListData2File(listitem, filename, prefix='txt'):
    try:
        print("SaveListData2File started: " + filename)
        # define empty list
        if prefix == 'txt' or prefix == '.txt':
            # open file and read the content in a list
            # with open(filename, 'w') as filehandle:
            #    for listitem in places:
            #        filehandle.write('%s\n' % listitem)
            with open(filename, 'w') as filehandle:
                filehandle.writelines("%s\n" % place for place in listitem)

        elif prefix == 'json' or prefix == '.json':
            import json
            with open(filename, 'w') as filehandle:
                json.dump(listitem, fp=filehandle, cls=MyEncoder)

        elif prefix == 'data' or prefix == 'pickle' or prefix == '.pickle':  # pickle  *.data file

            # load additional module
            import pickle
            with open(filename, 'wb') as filehandle:
                # read the data as binary data stream
                pickle.dump(listitem, filehandle)
        print("SaveListData2File completed: filename = " + filename)
    except Exception as e:
        print("Cant save  Data to filename: {}".format(str(filename)), e)
        print('Fail to save data to filename: [{}]'.format(filename))


def pref_list_dir_img_files(img_dir):
    ext_vec = ['.jpg', '.png', '.jpeg', '.gif', '.jpg!s', '.bmp', '.tif', '.tiff']
    file_list = listdir(img_dir)
    # files_vec = [img_dir+file for file in file_list if isfile(join(img_dir, file))
    #         and file[file.rfind('.'):].lower() in ext_vec]
    files_vec = []
    for file in file_list:
        fullfilename = join(img_dir, file)
        filename = basename(fullfilename)
        img_name = splitext(filename)[0]
        if img_name not in files_vec:
            if isfile(fullfilename) and file[file.rfind('.'):].lower() in ext_vec:
                files_vec += [img_name]

    return files_vec




def list_dir_img_files(img_dir):
    ext_vec = ['.jpg', '.png', '.jpeg', '.gif', '.jpg!s', '.bmp', '.tif', '.tiff']
    file_list = listdir(img_dir)
    # files_vec = [img_dir+file for file in file_list if isfile(join(img_dir, file))
    #         and file[file.rfind('.'):].lower() in ext_vec]
    files_vec = []
    for file in file_list:
        fullfilename = join(img_dir, file)
        if fullfilename not in files_vec:
            if isfile(fullfilename) and file[file.rfind('.'):].lower() in ext_vec:
                files_vec += [fullfilename]

    return files_vec



def createFolder(dir):
    try:
        isexist = path.exists(dir)
        if not isexist:
            makedirs(dir)
            # print('debug: createFolder:directory makedirs = ', dir)
        # else:
        #    print('createFolder:directory already exists = ', dir)
    except Exception as inst:
        print("createFolder had exception on : ", inst)

