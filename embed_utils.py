# define a simple data batch
import numpy as np
#import cv2
#from os.path import exists
from table_utils import extract_all_topn_content
from image_utils import read_image_file, get_COLOR_BGR2RGB
from misc_utils import get_image_blocks, get_bulk_swapped, update_mat_list, update_feature_mat

#from embedFeatureData import dict_embed
from DataStaticClass import DataStaticClass
TOP_N_CATEGORIES = DataStaticClass.TOP_N_CATEGORIES
RESNET_LABELS = DataStaticClass.RESNET_LABELS
EPS_SAF = DataStaticClass.EPS_SAF
KNN_SIZE = DataStaticClass.KNN_SIZE



def batch_img_features(img_vec, Dictionary=DataStaticClass, do_rescale=True, use_slices=True, disp=False):
    #feature_mat = []
    image_vec = [[]]*len(img_vec)
    for index, cimg in enumerate(img_vec):
        if cimg is None:
            print('cimg is None skip batch_img_features: cimg is None index = ', index)
            continue
        image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec, do_rescale=do_rescale, use_slices=use_slices)

    if len(image_vec) > 0:
        del image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))
        print(' img_pieces_vec min, max, mean = ', min(img_pieces_vec), max(img_pieces_vec),
              sum(img_pieces_vec)/len(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("batch_img_features: BATCH_SIZE = ", BATCH_SIZE)
    feature_mat = np.array([], dtype=np.float32)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                print('1 batch_img_features  skip: cur_feature_mat is None !')
                continue
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                print('2 batch_img_features  skip: cur_feature_mat is None !')
                continue
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            max_valid_blk = to_blk
        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        swapped_image_blocks = get_bulk_swapped(batch_Images)
        #if len(batch_images) > 0:
        #    del batch_images
        #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
        cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        # print('residual: dims: cur_feature_mat = ', np.shape(cur_feature_mat))
        if cur_feature_mat is None:
            print('3 batch_img_features  skip: cur_feature_mat is None !')
        else:
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
    return feature_mat, img_pieces_vec, img_dims_vec

def batch_imgfilename_features(imgfilename_vec, Dictionary=DataStaticClass, use_cv=True, do_rescale=True, use_slices=True, disp=False):
    #feature_mat = []
    image_vec = [[]]*len(imgfilename_vec)
    for index, imgfilename in enumerate(imgfilename_vec):
        try:
            cimg = read_image_file(imgfilename, use_cv)
            if cimg is None:
                print('cimg is None skip batch_imgfilename_features: cimg is None ', imgfilename)
                continue
        except:
            print('except: skip batch_imgfilename_features: ', imgfilename)
            continue
        image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec, do_rescale=do_rescale, use_slices=use_slices)

    if len(image_vec) > 0:
        del image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))
        print(' img_pieces_vec min, max, mean = ', min(img_pieces_vec), max(img_pieces_vec),
              sum(img_pieces_vec)/len(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("batch_imgfilename_features: BATCH_SIZE = ", BATCH_SIZE)
    feature_mat = np.array([], dtype=np.float32)
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                print('1 batch_img_features  skip: cur_feature_mat is None !')
                continue
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_Images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_Images)
            #if len(batch_images) > 0:
            #    del batch_images
            #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
            cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
            # print('index, dims: cur_feature_mat = ', index, np.shape(cur_feature_mat))
            if cur_feature_mat is None:
                print('2 batch_img_features  skip: cur_feature_mat is None !')
                continue
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
            max_valid_blk = to_blk
        # add residual
        # print('len(batch_image_blocks) , batches_num, all_batches = ', len(batch_image_blocks), batches_num, all_batches)
        # print('residual, from_img, max_valid_blk = ', residual, from_img, max_valid_blk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_Images = batch_image_blocks[from_blk:to_blk]
        swapped_image_blocks = get_bulk_swapped(batch_Images)
        #if len(batch_images) > 0:
        #    del batch_images
        #cur_feature_mat = get_mxnet_features_batch(swapped_image_blocks)
        cur_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        # print('residual: dims: cur_feature_mat = ', np.shape(cur_feature_mat))
        if cur_feature_mat is None:
            print('3 batch_img_features  skip: cur_feature_mat is None !')
        else:
            feature_mat = update_mat_list(cur_feature_mat, feature_mat)
    return feature_mat


def batch_img_feature_tags_scores(img_vec, Dictionary=DataStaticClass, disp=False):
    image_vec = [[]]*len(img_vec)
    #print("deb 0 batch_img_feature_tags_scores ")
    for index, cimg in enumerate(img_vec):
        if cimg is None:
            print('cimg is None skip batch_img_feature_tags_scores: cimg is None index = ', index)
            continue
        image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec)
    if len(image_vec) > 0:
        del image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("batch_img_feature_tags_scores: BATCH_SIZE = ", BATCH_SIZE)
    MAX_EMBED = Dictionary.MAX_EMBED
    feature_mat = np.array([], dtype=np.float32)
    labels_topn_mat = []
    scores_topn_mat = []
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            # max_valid_blk = to_blk
            labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            feature_mat = update_mat_list(add_feature_mat, feature_mat)

    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            feature_mat = update_mat_list(add_feature_mat, feature_mat)
            # LOG.debug('2size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
            max_valid_blk = to_blk
        # add one workers (chunk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        # LOG.debug('00size  batch_image_blocks[from_blk:to_blk] ', to_blk-from_blk, np.shape(batch_Images))
        # if len(batch_Images) < int(0.25*BATCH_SIZE):
        swapped_image_blocks = get_bulk_swapped(batch_images)
        if len(batch_images) > 0:
            del batch_images
        add_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
        SHAPE_ONE = Dictionary.SHAPE_ONE
        confidence_mat = []
        for swapped_image_blk in swapped_image_blocks:
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            prob_vec = Dictionary.predict_one_data(in_blk)
            confidence_mat = update_feature_mat(prob_vec, confidence_mat)
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks
        # LOG.debug('33size confidence_mat, add_feature_mat ', np.shape(confidence_mat), np.shape(add_feature_mat))
        add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
        del confidence_mat
        labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
        scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
        feature_mat = update_mat_list(add_feature_mat, feature_mat)
        # LOG.debug('3size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
        # feature_mat = np.concatenate((feature_mat, add_feature_mat), axis=0)
        # labels_topn_mat = np.concatenate((labels_topn_mat, add_labels_topn_mat), axis=0)
        # scores_topn_mat = np.concatenate((scores_topn_mat, add_scores_topn_mat), axis=0)
    return feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec


def batch_imgfilename_feature_tags_scores(imgfilename_vec, Dictionary=DataStaticClass, use_cv=True, disp=False):

    image_vec = [[]]*len(imgfilename_vec)
    for index, imgfilename in enumerate(imgfilename_vec):
        try:
            cimg = read_image_file(imgfilename, use_cv)
            if cimg is None:
                print('cimg is None skip batch_imgfilename_feature_tags_scores cimg is None ', imgfilename)
                continue
        except:
            print('except: skip batch_imgfilename_feature_tags_scores ', imgfilename)
            continue
        image_vec[index] = get_COLOR_BGR2RGB(cimg).copy()

    batch_image_blocks, img_pieces_vec, img_dims_vec = get_image_blocks(image_vec)
    if len(image_vec) > 0:
        del image_vec
    if disp:
        print(' img_pieces_vec len, sum = ', len(img_pieces_vec), sum(img_pieces_vec))

    BATCH_SIZE = Dictionary.BATCH_SIZE
    if disp:
        print("batch_imgfilename_feature_tags_scores: BATCH_SIZE = ", BATCH_SIZE)
    MAX_EMBED = Dictionary.MAX_EMBED
    feature_mat = np.array([], dtype=np.float32)
    labels_topn_mat = []
    scores_topn_mat = []
    ratio_batch = len(batch_image_blocks) / BATCH_SIZE
    batches_num = int(ratio_batch)
    all_batches = batches_num * BATCH_SIZE
    # print('all_batches, batches_num = ', all_batches, batches_num)
    residual = len(batch_image_blocks) - all_batches  ##len(image_vec) % BATCH_SIZE
    if residual == 0:
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            # max_valid_blk = to_blk
            labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            feature_mat = update_mat_list(add_feature_mat, feature_mat)

    else:  # if residual > 0:
        max_valid_blk = 0
        for index in range(0, batches_num):
            from_blk = index * BATCH_SIZE
            to_blk = (index + 1) * BATCH_SIZE
            batch_images = batch_image_blocks[from_blk:to_blk]
            swapped_image_blocks = get_bulk_swapped(batch_images)
            if len(batch_images) > 0:
                del batch_images
            # (len(combined_mat column), 13269) 13269 = 2048 + 11221
            combined_mat = Dictionary.predict_combined_data(swapped_image_blocks)
            if len(swapped_image_blocks) > 0:
                del swapped_image_blocks
            confidence_mat = combined_mat[:, MAX_EMBED:]  # columns length = 11221
            add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
            add_feature_mat = combined_mat[:, :MAX_EMBED]
            add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
            del confidence_mat, combined_mat
            labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
            scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
            feature_mat = update_mat_list(add_feature_mat, feature_mat)
            # LOG.debug('2size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
            max_valid_blk = to_blk
        # add one workers (chunk)
        from_blk = max_valid_blk
        to_blk = len(batch_image_blocks)
        batch_images = batch_image_blocks[from_blk:to_blk]
        # len_res = to_blk-from_blk
        # LOG.debug('00size  batch_image_blocks[from_blk:to_blk] ', to_blk-from_blk, np.shape(batch_Images))
        # if len(batch_Images) < int(0.25*BATCH_SIZE):
        swapped_image_blocks = get_bulk_swapped(batch_images)
        if len(batch_images) > 0:
            del batch_images
        add_feature_mat = Dictionary.predict_features_batch(swapped_image_blocks)
        add_feature_mat = np.array(add_feature_mat, dtype=np.float32)
        SHAPE_ONE = Dictionary.SHAPE_ONE
        confidence_mat = []
        for swapped_image_blk in swapped_image_blocks:
            in_blk = np.reshape(swapped_image_blk, SHAPE_ONE)
            prob_vec = Dictionary.predict_one_data(in_blk)
            confidence_mat = update_feature_mat(prob_vec, confidence_mat)
        if len(swapped_image_blocks) > 0:
            del swapped_image_blocks
        # LOG.debug('33size confidence_mat, add_feature_mat ', np.shape(confidence_mat), np.shape(add_feature_mat))
        add_labels_topn_mat, add_scores_topn_mat = extract_all_topn_content(confidence_mat)
        del confidence_mat
        labels_topn_mat = update_mat_list(add_labels_topn_mat, labels_topn_mat)
        scores_topn_mat = update_mat_list(add_scores_topn_mat, scores_topn_mat)
        feature_mat = update_mat_list(add_feature_mat, feature_mat)
        # LOG.debug('3size add_feature_mat, feature_mat ', np.shape(add_feature_mat), np.shape(feature_mat))
        # feature_mat = np.concatenate((feature_mat, add_feature_mat), axis=0)
        # labels_topn_mat = np.concatenate((labels_topn_mat, add_labels_topn_mat), axis=0)
        # scores_topn_mat = np.concatenate((scores_topn_mat, add_scores_topn_mat), axis=0)
    return feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec


