# noiseFilterEmbedData.py
# from lgbm_utils import LGBMService
# from mlaram_utils import MlARAMService
# from logging_utils import LOG
from env_configuration import MLAwsModes, NOISE_FILTER_Files  # , MLConfiguration
from file_utils import read_rstrip_csv_array, external_path  # , read_json, load_pickle_file
from image_utils import DEFAULT_IMAGE_SIZE
from mxnet_utils import get_mxnet_features_batch0, MXNetBatchService, load_resnet152_model
# from lgbm_utils import LGBM_predict
import numpy


def calc_mid_dict_numpy(dict_ref, use_weights=False):
    mid_vec = []
    if use_weights:
        for freq_vals in dict_ref.values():
            vals = freq_vals[0]
            if isinstance(vals, list):
                mid_vec.extend(vals)
            else:
                mid_vec.extend([vals])
    else:
        for vals in dict_ref.values():
            if isinstance(vals, list):
                mid_vec.extend(vals)
            else:
                mid_vec.extend([vals])
    mid = sum(mid_vec) / max(len(mid_vec), 1)
    mid = min(max(1.0 - mid, 0.0), 1.0)
    return mid



def get_all_mid_dicts_numpy(tables_dict):
    mid_vec = [0] * len(tables_dict)
    index = 0
    for dict_ref in tables_dict.values():
        mid_vec[index] = calc_mid_dict_numpy(dict_ref)
        index += 1
    return mid_vec




def calc_freq_mid_dict_numpy(dict_ref):
    freq_vec = numpy.full(len(dict_ref.values()), 0.0, dtype=numpy.float32)
    for index, cf in enumerate(dict_ref.values()):
        freq_vec[index] = cf
    # sorted_scores_args = numpy.argsort(freq_vec)
    # inds_vec = sorted_scores_args[:max_cats]
    # for ii in inds_vec:
    #    sum_mid += freq_vec[ii]
    mid = 0.5 * (numpy.median(freq_vec) + numpy.mean(freq_vec))
    return mid


def get_all_freq_mid_dicts_numpy(freq_tables_dict):
    freq_mid_vec = numpy.full(len(freq_tables_dict.values()), 0.0, dtype=numpy.float32)
    for index, dict_ref in enumerate(freq_tables_dict.values()):
        freq_mid_vec[index] = calc_freq_mid_dict_numpy(dict_ref)
    return freq_mid_vec


class numpy_DataStaticClass:
    # ------------------------------------------------------------------------------------------------------------------
    # Load  trained model
    # print('deb0 NOISE_FILTER_Files = ', NOISE_FILTER_Files)
    # NMM_LGBM_MODEL = LGBMService(external_path(NOISE_FILTER_Files.ENV_DIRECTORY, NOISE_FILTER_Files.NMM_MODEL)).model
    # SIM_MLARAM_MODEL = MlARAMService(external_path(NOISE_FILTER_Files.ENV_DIRECTORY, NOISE_FILTER_Files.SIM_MLARAM_MODEL)).model
    # NMM_TBL_MODEL = LGBMService(external_path(NOISE_FILTER_Files.ENV_DIRECTORY, NOISE_FILTER_Files.NMM_TBL_MODEL)).model
    # NMM_FREQ_TBL = read_json(external_path(NOISE_FILTER_Files.ENV_DIRECTORY, NOISE_FILTER_Files.NMM_FREQ_TBL))
    # cfile = 'empty_min_embed_mosq_2020-12-04.pickle'
    # NMM_TBL = load_pickle_file(external_path(NOISE_FILTER_Files.ENV_DIRECTORY, NOISE_FILTER_Files.NMM_TBL))
    # print('NMM_TBL: len(NMM_TBL) = ', len(NMM_TBL))
    # print('NMM_FREQ_TBL: len(NMM_FREQ_TBL) = ', len(NMM_FREQ_TBL))
    # for ckey, cval in NMM_TBL.items():
    #     print('table: ckey, len(cval) = ', ckey, len(cval))
    # for ckey, cval in NMM_FREQ_TBL.items():
    #     print('freq_table: ckey, len(cval) = ', ckey, len(cval))

    # Load Global variables only once
    RESNET_LABELS = read_rstrip_csv_array(external_path(NOISE_FILTER_Files.MXNET_DIRECTORY,
                                                        NOISE_FILTER_Files.NMM_LABELS))

    # For batch : NUM_IMGS_PER_BATCH = number of images per batch
    # BLOCKS_PER_IMG = number of blocks per image
    SHAPE_ONE = (1, 3, DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE)
    NUM_IMGS_PER_BATCH = 8  # MLConfiguration.ML_SPECIES_IMAGE_BATCH_SIZE  # limited up to 1000 not working above it
    BLOCKS_PER_IMG = 2
    # NUM_IMGS_PER_BATCH = 1
    # BLOCKS_PER_IMG = 3

    BATCH_SIZE = min(int(NUM_IMGS_PER_BATCH * BLOCKS_PER_IMG + 0.5), NUM_IMGS_PER_BATCH * 3)
    SHAPE_BATCH = (BATCH_SIZE, 3, DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE)
    print('DataStaticClass: NUM_IMGS_PER_BATCH, BLOCKS_PER_IMG, BATCH_SIZE ', NUM_IMGS_PER_BATCH, BLOCKS_PER_IMG, BATCH_SIZE)

    # SIM_DEFAULT_PROBABILITY = 0.8
    #devices = {'cpu': mxnet.cpu(), 'gpu': mxnet.gpu(0), 'gpucpu': [mxnet.gpu(0), mxnet.cpu()]}
    hw_ctx = "cpu" #"cpu"  # "gpu"
    print("device = ", hw_ctx)
    modelPrefix = MLAwsModes.MXNET_DIRECTORY + MLAwsModes.MXNET_PREFIX
    modelEpoch = 0

    print('\n DataStaticClass: load single lables/scores only model SHAPE_ONE = ', SHAPE_ONE)
    lab_score_only = 1  # single lables/scores only
    # MXNET_NMM_MODEL = load_resnet152_model(prefix=modelPrefix,
    #                                      checkpoint_epoch=modelEpoch,
    #                                      feature_mod=lab_score_only,
    #                                      shape=SHAPE_ONE, device=hw_ctx)

    try:
        MXNET_NMM_MODEL = MXNetBatchService(modelPrefix, modelEpoch, SHAPE_ONE, hw_ctx, lab_score_only).model

    except:
        try:
            print(" \n numpy lab_score_only Numpy failed MXNetBatchService try...cur_MXNetBatchService")
            print("modelPrefix, modelEpoch, SHAPE_ONE, hw_ctx, lab_score_only ",
                  modelPrefix, modelEpoch, SHAPE_ONE, hw_ctx, lab_score_only)
            MXNET_NMM_MODEL = load_resnet152_model(prefix=modelPrefix,
                                                         checkpoint_epoch=modelEpoch,
                                                         feature_mod=lab_score_only,
                                                         shape=SHAPE_ONE, device=hw_ctx)
        except Exception as inst:
            print("lab_score_only MXNET_NMM_MODEL = MXNetBatchService had exception : ", inst)

    # print("MXNetBatchService MXNET_NMM_MODEL0 == MXNET_NMM_MODEL ", MXNET_NMM_MODEL0, MXNET_NMM_MODEL)
    # print('\n load batch lables/scores only model...')
    lab_score_only = 1  # batch lables/scores only
    #BATCH_MXNET_NMM_MODEL = load_resnet152_model(prefix=modelPrefix,
    #                                       checkpoint_epoch=modelEpoch,
    #                                       feature_mod=lab_score_only,
    #                                       shape=SHAPE_BATCH, device=hw_ctx)
    try:
        BATCH_MXNET_NMM_MODEL = MXNetBatchService(modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, lab_score_only).model
    except:
        try:
            print(" \n numpy lab_score_only Numpy failed MXNetBatchService try...cur_MXNetBatchService")
            print("modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, lab_score_only ",
                  modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, lab_score_only)
            BATCH_MXNET_NMM_MODEL = load_resnet152_model(prefix=modelPrefix,
                                                         checkpoint_epoch=modelEpoch,
                                                         feature_mod=lab_score_only,
                                                         shape=SHAPE_BATCH, device=hw_ctx)
        except Exception as inst:
            print("lab_score_only BATCH_MXNET_NMM_MODEL = MXNetBatchService had exception : ", inst)


    # Init mxnet prediction model algo
    # SIM_SINGLE_MXNET_MODEL = mxnet_utils.load_prediction_model(
    #    prefix=MLAwsModes.MXNET_DIRECTORY + MLAwsModes.MXNET_PREFIX,
    #    checkpoint_epoch=0,
    #    shape=SHAPE_ONE,
    #    device="cpu")

    print('\n DataStaticClass: load batch features only model SHAPE_BATCH = ', SHAPE_BATCH)
    # BATCH: Init mxnet feature model algo
    features_only = 2  # get features only
    # NMM_FEATURE_MXNET_MODEL = load_resnet152_model(prefix=modelPrefix,
    #                                      checkpoint_epoch=modelEpoch,
    #                                      feature_mod=features_only,
    #                                      shape=SHAPE_BATCH, device=hw_ctx)
    try:
        NMM_FEATURE_MXNET_MODEL = MXNetBatchService(modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, features_only).model
    except:
        try:
            print(" \n numpy features_only failed MXNetBatchService try...cur_MXNetBatchService")
            print("modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, features_only ",
                  modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, features_only)
            NMM_FEATURE_MXNET_MODEL = load_resnet152_model(prefix=modelPrefix,
                                                           checkpoint_epoch=modelEpoch,
                                                           feature_mod=features_only,
                                                           shape=SHAPE_BATCH, device=hw_ctx)
        except Exception as inst:
            print("NMM_FEATURE_MXNET_MODEL = MXNetBatchService had exception : ", inst)

    print('\n DataStaticClass: load batch Combined features-lables-scores model...')
    # BATCH: Init mxnet combined( feature+prediction ) model algo
    combs = 0  # Combined features-lables-scores model
    # NMM_COMBINED_MODEL = load_resnet152_model(prefix=modelPrefix,
    #                                       checkpoint_epoch=modelEpoch,
    #                                       feature_mod=combs,
    #                                       shape=SHAPE_BATCH, device=hw_ctx)
    try:
        NMM_COMBINED_MODEL = MXNetBatchService(modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, combs).model
    except:
        try:
            print(" \n numpy combs Numpy failed MXNetBatchService try...cur_MXNetBatchService")
            print("modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, combs ",
                  modelPrefix, modelEpoch, SHAPE_BATCH, hw_ctx, combs)

            NMM_COMBINED_MODEL = load_resnet152_model(prefix=modelPrefix,
                                                      checkpoint_epoch=modelEpoch,
                                                      feature_mod=combs,
                                                      shape=SHAPE_BATCH, device=hw_ctx)
        except Exception as inst:
            print("comb NMM_COMBINED_MODEL = MXNetBatchService had exception : ", inst)

    TOP_N_CATEGORIES = 20
    EPS_SAF = numpy.finfo(float).eps  # 2.22e-16
    KNN_SIZE = 3
    MAX_EMBED = 2048

    #
    # mid_vec = get_all_mid_dicts(NMM_TBL)
    # print('\nmid_vec = ', len(mid_vec), mid_vec)
    # freq_mid_vec = get_all_freq_mid_dicts(NMM_FREQ_TBL)
    # print('\nfreq_mid_vec = ', len(freq_mid_vec), freq_mid_vec)
    #    return species_vec, species_lab_cat, species_cat_lab, ids_cats_vec, mid_vec, freq_mid_vec

    # ----------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def predict_features_batch(img_batch: numpy.ndarray) -> numpy.ndarray:
        return get_mxnet_features_batch0(numpy_DataStaticClass.NMM_FEATURE_MXNET_MODEL, img_batch)

    @staticmethod
    def predict_combined_data(img_batch: numpy.ndarray) -> numpy.ndarray:
        return get_mxnet_features_batch0(numpy_DataStaticClass.NMM_COMBINED_MODEL, img_batch)

    @staticmethod
    def predict_one_data(img_batch: numpy.ndarray) -> numpy.ndarray:
        return get_mxnet_features_batch0(numpy_DataStaticClass.MXNET_NMM_MODEL, img_batch)

    @staticmethod
    def predict_batch_data(img_batch: numpy.ndarray) -> numpy.ndarray:
        return get_mxnet_features_batch0(numpy_DataStaticClass.BATCH_MXNET_NMM_MODEL, img_batch)


#   @staticmethod
#    def predict_batch_data(img_batch: numpy.ndarray) -> numpy.ndarray:
#        return get_mxnet_features_batch0(DataStaticClass.BATCH_MXNET_NMM_MODEL, img_batch)

#   @staticmethod
#    def predict_lgbm(dataFrame0):
#        return LGBM_predict(DataStaticClass.NMM_LGBM_MODEL, dataFrame0)



