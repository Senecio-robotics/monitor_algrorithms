from configuration_models import Configuration, AwsS3ModelKeys, NOISE_FILTER_Files # AwsS3Keys,
from env_utils import get_env_str, get_env_int #, get_env_float

MLConfiguration = Configuration(
    get_env_str("ML_MODE", "SERO_ML"),
    # Logging configuration:
    get_env_str("LOGENTRIES_TOKEN", str(None)),
    get_env_str("ML_LOG_MODE", "INFO"),
    # Use by default batch_size=6 - as it is in sero tagging configuration
    get_env_int("ML_IMAGE_BATCH_SIZE", "6"),
    get_env_int("ML_SPECIES_IMAGE_BATCH_SIZE", "3"),
    get_env_str("ML_S3_MODELS_BUCKET", "senecio-models")
)
# print(str(MLConfiguration))




NOISE_FILTER_Files = NOISE_FILTER_Files(
    get_env_str("NMM_MODEL", "monitor_noise_filter_embed_2multiclass_94730x2048_2_283s0.00540515_lgbm_95_2021-03-27.txt"),
    #get_env_str("NMM_MODEL", "noise_filter_embed_2multiclass_89522x2048_2_288_s0.00540336_lgbm_95_2021-03-22.txt"),
    get_env_str("NMM_TBL_MODEL", "monitor_noise_filter_2TBL_multiclass_46108x92_2_176s1.09607e-08_lgbm_95_2021-03-27.txt"),
    get_env_str("NMM_TBL", "table_spec_monitor_2_2887_2021-03-27_top_20.pickle"),
    get_env_str("NMM_FREQ_TBL", "table_freq_spec_monitor_2_2887_2021-03-27_top_20.json"),
    #get_env_str("ML_SIM_MLARAM_MODEL", "ml/models/sim/sim4_embed2048_MLARAM_pred_1.0_384x2048_2020-12-05.sav"),
    get_env_str("NMM_LABELS", "synset.txt")
)

# ML: image recognition models
MLAwsModes = AwsS3ModelKeys(
    # MXNet:
    get_env_str("ML_S3_MODELS_MXNET_PREFIX", "resnet-152"),
    get_env_str("ML_S3_MODELS_MXNET_PARAMS", "resnet-152-0000.params"),
    get_env_str("ML_S3_MODELS_MXNET_JSON", "resnet-152-symbol.json"),
    # LGBM:
)
