# classification of 3 categories ['unknown', 'Culex pipiens', 'Culex tarsalis']
# 3 binary model are used
# code implmented mainly with numpy package to fcilated time performance
# Written by Gabriel Weizman

# Step 1: load global variables: todo put it in a class
from file_utils import load_pickle_file
from DataStaticClass import get_all_mid_dicts, get_all_freq_mid_dicts
from table_utils import is_cost_valid, norm_cost, norm_freq_cost, get_merged_cost
from table_utils import complete_merged_cost, knn_match_cost_counter, freq_weight_counter
from table_utils import most_topn_from_all
from misc_utils import flatten
from embed_utils import batch_img_feature_tags_scores
from embed_utils import batch_img_features
import json
import numpy as np
import pandas as pd

#### main static class ##############################
# class detect_UN_CP_CT:
#use_generic_models = False
src_dir = 'D:/senecio/data/resources/external/UN_CP_CT/'
models_dir = src_dir + 'models/'
tables_dir = src_dir + "tables/"
tbl_name = 'table_spec_califIsr3CP_CT_UN_3x4253_2021-08-18_top_20.pickle'
tbl_freq_name = 'table_freq_spec_califIsr3CP_CT_UN_3x4253_2021-08-18_top_20.json'
lgbm_model_name1 = 'califIsr2cats_CP_UN_embed_multiclass_166096x2048_2_30000_s9.87656e-05_lgbm_95_2021-08-22.txt'
lgbm_model_name2 = 'califIsr2cats_CT_UN_embed_multiclass_166096x2048_30000_s9.80191e-05_lgbm_95_2021-08-22.txt'
lgbm_model_name3 = 'califIsr2cats_CP_CT_embed_multiclass_73715x2048_2_30000_s1.79523e-08_lgbm_95_2021-08-18.txt'

valid_tags = ['unknown', 'Culex pipiens', 'Culex tarsalis']
nclass = len(valid_tags)-1
print("detect_UN_CP_CT_embed support valid_tags, nclass = ", valid_tags, nclass)
ids_cats_vec = [ii for ii in range(len(valid_tags))]
print('ids_cats_vec = ', len(ids_cats_vec), ids_cats_vec)

monitor_lab_cat = dict()
monitor_cat_lab = dict()
for ii, ctag in enumerate(valid_tags):
    # print(st)
    monitor_lab_cat[ii] = ctag
    monitor_cat_lab[ctag] = ii

print('monitor_lab_cat: ', monitor_lab_cat)
print('monitor_cat_lab: ', monitor_cat_lab)

embed_only = False
print("global embed_only = ", embed_only)


table_dct = []
table_freq_dct = []
table_dct_file = tables_dir + tbl_name
try:
    print('\n  Load : table_dct_file dict tables ')
    table_dct = load_pickle_file(table_dct_file)
    print('fullimg table_dct_file = ', table_dct_file, 'len(table_dct) = ', len(table_dct))
except:
    print('failed: table_dct not loaded ', table_dct_file)

if len(table_dct) != len(valid_tags):
    print("error len(table_dct) != len(valid_tags) ", len(table_dct), len(valid_tags))

print("debug: detect_UN_CP_CT_embed support valid_tags = ", valid_tags, len(table_dct))
tbl_tags_vec = [ckey for ckey in table_dct if ckey in valid_tags]
print("debug: detect_UN_CP_CT_embed support tbl_tags_vec = ", tbl_tags_vec)
if valid_tags != tbl_tags_vec:
    reorder_tbl_dct = dict()
    for ctag in valid_tags:
        if ctag in table_dct:
            cval_vec = table_dct[ctag]
            reorder_tbl_dct[ctag] = cval_vec
        else:
            print("error ctag not in table_dct ", ctag)
    table_dct = reorder_tbl_dct.copy()
    tbl_tags_vec = [ckey for ckey in table_dct if ckey in valid_tags]
    print("table warn: reorder, tbl_tags_vec = ", tbl_tags_vec)
else:
    print("table final: tbl_tags_vec = ", tbl_tags_vec)

for ckey, cval in table_dct.items():
    print('table: ckey, len(cval) = ', ckey, len(cval))

# if table_freq_dct:

table_freq_dct_file = tables_dir + tbl_freq_name
try:
    print('\n  Load : table_freq_dct_file ')
    with open(table_freq_dct_file, 'r') as filehandle:
        table_freq_dct = json.load(filehandle)
    print('fullimg table_freq_dct_file = ', table_freq_dct_file, 'len(table_freq_dct) = ', len(table_freq_dct))
except:
    print('failed: table_freq_dct_file not loaded ', table_freq_dct_file)

if len(table_freq_dct) != len(valid_tags):
    print("error len(table_freq_dct) != len(valid_tags) ", len(table_freq_dct), len(valid_tags))

tbl_freq_tags_vec = [ckey for ckey in table_freq_dct if ckey in valid_tags]
if valid_tags != tbl_freq_tags_vec:
    reorder_freq_tbl_dct = dict()
    for ctag in valid_tags:
        if ctag in table_freq_dct:
            cval_vec = table_freq_dct[ctag]
            reorder_freq_tbl_dct[ctag] = cval_vec
        else:
            print("error ctag not in table_freq_dct ", ctag)
    table_freq_dct = reorder_freq_tbl_dct.copy()
    tbl_freq_tags_vec = [ckey for ckey in table_freq_dct if ckey in valid_tags]
    print("freq table warn: reorder, tbl_freq_tags_vec = ", tbl_freq_tags_vec)
else:
    print("freq table final: tbl_tags_vec = ", tbl_freq_tags_vec)

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name1
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model1 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model1 not loaded ', lgbm_model_name1)
    LGBM_Model1 = None

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name2
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model2 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model2 not loaded ', lgbm_model_name2)
    LGBM_Model2 = None

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name3
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model3 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model3 not loaded ', lgbm_model_name3)
    LGBM_Model3 = None

mid_vec = get_all_mid_dicts(table_dct)
print('\nmid_vec = ', len(mid_vec), mid_vec)
freq_mid_vec = get_all_freq_mid_dicts(table_freq_dct)
print('\nfreq_mid_vec = ', len(freq_mid_vec), freq_mid_vec)


# Step 2: load all functions
#np_epsf = np.finfo(float).eps
#np_epsf32 = np.finfo(np.float32).eps


def get_unique_vals(cats_vec, prob_vec, scal_vec):
    if len(np.unique(cats_vec)) == len(cats_vec):
        return cats_vec, prob_vec, scal_vec
    else:
        print("warn get_unique_vals: len(np.unique(cats_vec)) != len(cats_vec) ", cats_vec)
        final_cats_vec = []
        final_prob_vec = []
        final_sc_vec = []
        #len(scal_vec) = 2 binary,  len(cats_vec) = len(prob_vec) = 1
        for index, clab in enumerate(cats_vec):
            if clab not in final_cats_vec:
                final_cats_vec += [clab]
                final_prob_vec += [prob_vec[index]]
                final_sc_vec += [scal_vec[index]]
        return final_cats_vec, final_prob_vec, final_sc_vec

def predict_multiclass_lgbm_all(LGBM_Model, feature_mat, img_pieces_vec, nclass, ignore_warn=True):
    #score_mat = []
    #predictions_mat = []
    cvec = [0.0]*nclass
    sc_mat = [cvec]*len(img_pieces_vec)
    cvec = (0)*nclass
    feature_pieces = [cvec]*len(img_pieces_vec)
    prob_vec = [0.0]*len(img_pieces_vec)
    category_vec = [0]*len(img_pieces_vec)
    try:
        x_mat = np.array(feature_mat)
        x_test = pd.DataFrame(x_mat)
        y_predicted_mat = LGBM_Model.predict(x_test)
        startat = 0
        for jj_piece, pieces in enumerate(img_pieces_vec):
            category = [0]
            probabilitiy = [0.0]
            #sc_out_vec = [0] * nclass
            endat = startat + pieces
            cinds = (startat, endat)
            feature_pieces[jj_piece] = cinds
            cur_x_mat = x_mat[startat:endat]
            dims = np.shape(cur_x_mat)
            csaf = max(int(0.5 * dims[0]), 1)  # +0.5
            y_predicted = y_predicted_mat[startat:endat]
            #pred_lab_vec, prob_embed_vec, sc_vec = detect_UN_CP_CT.three_models_features_detection(image_feature_mat)
            scores_vec = y_predicted[0]
            sc_out_vec = [0] * len(scores_vec)
            if nclass != len(scores_vec) and not ignore_warn:
                if len(scores_vec) > nclass:
                    print('Warning: predict_multiclass_lgbm_all: use transform!', jj_piece, nclass, len(scores_vec))

            sc_sz = len(y_predicted)
            count_above = [0] * len(sc_out_vec)
            for score_vec in y_predicted:
                for index, cval in enumerate(score_vec):
                    sc_out_vec[index] += cval
                max_val = max(score_vec)
                for index in range(len(score_vec)):
                    if score_vec[index] == max_val:
                        count_above[index] += 1
            sc_out_vec = [cval / sc_sz for cval in sc_out_vec]
            max_count = max(count_above)
            # max_index = count_above.index(max_count) if count_above >= csaf:
            tot_count = 0
            for ii in range(len(count_above)):
                if count_above[ii] == max_count:
                    if count_above[ii] >= csaf:
                        if ii not in category:
                            tot_count += 1
                            if tot_count < 2:
                                category = [ii]
                                probabilitiy = [sc_out_vec[ii]]
                            else:
                                print("warn predict_multiclass_lgbm_all: tot_count > 1 pick first! ", tot_count,
                                      category, ii, probabilitiy, sc_out_vec[ii])
                        else:
                            if probabilitiy[ii] < sc_out_vec[ii]:
                                probabilitiy[ii] = sc_out_vec[ii]

            if len(category) == 0:
                max_prob = max(sc_out_vec)
                category = [sc_out_vec.index(max_prob)]
                probabilitiy = [max_prob]

            prob_vec[jj_piece] = probabilitiy[0]
            category_vec[jj_piece] = category[0]
            sc_mat[jj_piece] = sc_out_vec
            startat = endat
    except Exception as inst:
        print("Fail predict_multiclass_lgbm_all: had exception on : ", inst)
        pass
    return category_vec, prob_vec, sc_mat, feature_pieces


def predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass, ignore_warn=True):
    sc_out_vec = [0] * nclass
    category = [0]
    probabilitiy = [0.0]
    x_mat = np.array(feature_mat)
    dims = np.shape(x_mat)
    #print("deb feature_mat ", np.shape(feature_mat), type(feature_mat) )
    #print("deb x_mat ", np.shape(x_mat), type(x_mat) )
    csaf = max(int(0.5 * dims[0]), 1)  # +0.5
    try:
        #x_test = pd.DataFrame(x_mat)
        #y_predicted = LGBM_Model.predict(x_test)
        x_test = pd.DataFrame(x_mat)
        y_predicted = LGBM_Model.predict(x_test)
        scores_vec = y_predicted[0]
        sc_out_vec = [0] * len(scores_vec)
        if nclass != len(scores_vec) and not ignore_warn:
            if len(scores_vec) > nclass:
                print('Warning: predict_nudity_multiclass3_lgbm: use transform!', nclass, len(scores_vec))

        sc_sz = len(y_predicted)
        count_above = [0] * len(sc_out_vec)
        for score_vec in y_predicted:
            for index, cval in enumerate(score_vec):
                sc_out_vec[index] += cval
            max_val = max(score_vec)
            for index in range(len(score_vec)):
                if score_vec[index] == max_val:
                    count_above[index] += 1
        sc_out_vec = [cval / sc_sz for cval in sc_out_vec]
        max_count = max(count_above)
        # max_index = count_above.index(max_count) if count_above >= csaf:
        tot_count = 0
        for ii in range(len(count_above)):
            if count_above[ii] == max_count:
                if count_above[ii] >= csaf:
                    if ii not in category:
                        tot_count += 1
                        if tot_count < 2:
                            category = [ii]
                            probabilitiy = [sc_out_vec[ii]]
                        else:
                            print("warn predict_multiclass_lgbm: tot_count > 1 pick first! ", tot_count, category, ii,
                                  probabilitiy, sc_out_vec[ii])
                    else:
                        if probabilitiy[ii] < sc_out_vec[ii]:
                            probabilitiy[ii] = sc_out_vec[ii]

        if len(category) == 0:
            max_prob = max(sc_out_vec)
            category = [sc_out_vec.index(max_prob)]
            probabilitiy = [max_prob]

    except Exception as inst:
        print("Fail predict_multiclass_lgbm: had exception on : ", inst)
        pass
    return category, probabilitiy, sc_out_vec


def valid3to2cats_find_by_features(LGBM_Model, feature_mat, nclass, eps_saf=1.0e-08):
    cats_out_vec = [0]
    probs_out_vec = [0.0]
    category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    # category_vec = adjust_tbl_embed(category_vec)
    max_prob = max(prob_vec)
    for index, cprob in enumerate(prob_vec):
        cat_val = category_vec[index]
        if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
            cats_out_vec = [cat_val]
            probs_out_vec = [cprob]
    return cats_out_vec, probs_out_vec, sc_vec


def valid3to2cats_find_tags_by_features(LGBM_Model, feature_mat, total_cost_vec, ids_cats_vec, nclass, embed_only=False,
                                        use_all=False,  detect_CP=True, detect_CT=False, detect_UN=True, eps_saf=1.0e-08):
    cats_out_vec = [0]
    probs_out_vec = [0.0]
    n_cost = nclass * 3
    category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    # category_vec = adjust_tbl_embed(category_vec)

    if embed_only or len(total_cost_vec) == 0:
        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec = [cat_val]
                probs_out_vec = [cprob]
    else:
        #tot_vec = list()
        #inds_vec = list()
        do_return = False
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN: # case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT: # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT: # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(ctot - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN: # case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT: # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1: #CT is Un
                                findex = 0
                    elif detect_CT: # case UN vs CT
                        if index == 1: #CP is Un
                            findex = 0
                #if findex < len(ids_cats_vec) and findex > -1:
                cat_val = ids_cats_vec[findex]
                #else:
                #    print("error findex >= len(ids_cats_vec) ", ii, index, findex, len(ids_cats_vec))
                #    print("error detect_UN, detect_CP, detect_CT ", detect_UN, detect_CP, detect_CT)
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        #pos_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN: # case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT: # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT: # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cpos - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:# case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:# case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:# case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        #neg_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii]
                #neg_vec.append(cneg)
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN: # case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT: # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT: # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec[:len_max]), 6):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cneg - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN: # case: CP vs. CT
                        if index > 0:
                            findex = index-1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT: # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else: # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT: # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob

    return cats_out_vec, probs_out_vec, sc_vec

def all_find_tags_by_features(category_vec, prob_vec, total_cost_vec, ids_cats_vec, nclass,
                              embed_only=False, use_all=False, detect_CP=True, detect_CT=False, detect_UN=True,
                              eps_saf=1.0e-08):
    cats_out_vec = [0]
    probs_out_vec = [0.0]
    n_cost = nclass * 3
    #category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    # category_vec = adjust_tbl_embed(category_vec)

    if embed_only or len(total_cost_vec) == 0:
        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob
    else:
        #tot_vec = list()
        #inds_vec = list()
        do_return = False
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(ctot - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        #pos_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cpos - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        #neg_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii]
                #neg_vec.append(cneg)
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = [0] * length
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec[:len_max]), 6):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cneg - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob

    return cats_out_vec, probs_out_vec

def estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1, cats_out_vec2, prob_out_vec2, sc_vec2):

    def_cat = 0
    cats_out_vec = [def_cat]*len(cats_out_vec1)
    prob_out_vec = [0.0]*len(cats_out_vec1)
    sc_vec = [0.0]*len(cats_out_vec1)
    is_succeed = True
    for index, cur_cat in enumerate(cats_out_vec1):
        cur_prob = prob_out_vec1[index]
        cur_sc = sc_vec1[index]
        cur_cat2 = cats_out_vec2[index]
        cur_sc2 = sc_vec2[cur_cat2]
        ave_sc = 0.5 * (cur_sc + cur_sc2)
        sc_vec[index] = ave_sc
        if cur_cat == 1: #C.P.
            if cur_cat in cats_out_vec2: #C.T.
                is_succeed = False
            elif def_cat in cats_out_vec2:
                if cur_cat not in cats_out_vec:
                    cats_out_vec[index] = cur_cat
                    prob_out_vec[index] = cur_prob
                else:
                    ii_cur_cat = cats_out_vec.index(cur_cat)
                    if prob_out_vec[ii_cur_cat] < cur_prob:
                        prob_out_vec[ii_cur_cat] = cur_prob
        else:# cur_cat == 0
            cur_cat2 = cats_out_vec2[index]
            cur_prob2 = prob_out_vec2[index]
            if cur_cat2 == 1:#C.T.
                cur_cat3 = cur_cat2+1
                if cur_cat3 not in cats_out_vec:
                    cats_out_vec[index] = cur_cat3
                    prob_out_vec[index] = cur_prob2
                else:
                    ii_cur_cat = cats_out_vec.index(cur_cat3)
                    if prob_out_vec[ii_cur_cat] < cur_prob2:
                        prob_out_vec[ii_cur_cat] = cur_prob2
            else: #UN
                ave_prob = 0.5*(cur_prob + cur_prob2)
                if cur_cat2 not in cats_out_vec:
                    cats_out_vec[index] = cur_cat2
                    prob_out_vec[index] = ave_prob
                else:
                    ii_cur_cat = cats_out_vec.index(cur_cat2)
                    if prob_out_vec[ii_cur_cat] < ave_prob:
                        prob_out_vec[ii_cur_cat] = ave_prob
    if not is_succeed:
        cats_out_vec = cats_out_vec1 + cats_out_vec2
        prob_out_vec = prob_out_vec1 + prob_out_vec2
    return is_succeed, cats_out_vec, prob_out_vec, sc_vec


#@staticmethod
def three_models_features_detection(feature_mat):
    global nclass #= detect_UN_CP_CT.nclass
    global LGBM_Model1 #= detect_UN_CP_CT.LGBM_Model1
    global LGBM_Model2 #= detect_UN_CP_CT.LGBM_Model2
    global nclass# = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,

    cats_out_vec1, prob_out_vec1, sc_vec1 = valid3to2cats_find_by_features(LGBM_Model1, feature_mat, nclass)

    cats_out_vec1, prob_out_vec1, sc_vec1 = get_unique_vals(cats_out_vec1, prob_out_vec1, sc_vec1)

    cats_out_vec2, prob_out_vec2, sc_vec2 = valid3to2cats_find_by_features(LGBM_Model2, feature_mat, nclass)

    cats_out_vec2, prob_out_vec2, sc_vec2 = get_unique_vals(cats_out_vec2, prob_out_vec2, sc_vec2)

    # table truth
    is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1,
                                                                          cats_out_vec2, prob_out_vec2, sc_vec2)
    if not is_succeed:
        cats_out_vec12 = cats_out_vec.copy()
        prob_out_vec12 = prob_out_vec.copy()
        sc_vec12 = sc_vec.copy()
        global LGBM_Model3 #= detect_UN_CP_CT.LGBM_Model3
        cats_out_vec3, prob_out_vec3, sc_vec3 = valid3to2cats_find_by_features(LGBM_Model3, feature_mat, nclass)

        print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
              sc_vec12)
        print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
              sc_vec3)

        cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)

    return cats_out_vec, prob_out_vec, sc_vec


#@staticmethod
def predict_LGBM_by_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, img_pieces_vec, img_dims_vec = batch_img_features(img_vec, disp=disp)
    prob_vec = [0.0]*len(img_pieces_vec)
    category_vec = [0]*len(img_pieces_vec)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        image_feature_mat = feature_mat[startat:endat]
        pred_lab_vec, prob_embed_vec, _ = three_models_features_detection(image_feature_mat)
        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]
        startat = endat
    return category_vec, prob_vec


#@staticmethod
def three_models_features_tags_detection(labels_topn_all, scores_topn_all, feature_mat):
    global embed_only #= detect_UN_CP_CT.embed_only
    global ids_cats_vec #= detect_UN_CP_CT.ids_cats_vec
    global LGBM_Model1 #= detect_UN_CP_CT.LGBM_Model1
    global LGBM_Model2 #= detect_UN_CP_CT.LGBM_Model2
    global table_dct #= detect_UN_CP_CT.table_dct
    global table_freq_dct #= detect_UN_CP_CT.table_freq_dct
    global freq_mid_vec #= detect_UN_CP_CT.freq_mid_vec
    global  mid_vec #= detect_UN_CP_CT.mid_vec
    use_complete = True
    cats_out_vec = [0]
    prob_out_vec = [0.0]
    nclass = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    sc_vec = [0] * nclass
    #print("deb three_models_features_tags_detection:  ", np.shape(labels_topn_all), labels_topn_all[0], \
    #      np.shape(scores_topn_all), scores_topn_all[0])
    labels_topn, scores_topn = most_topn_from_all(labels_topn_all, scores_topn_all)

    if len(labels_topn) == 0:
        print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
        return cats_out_vec, prob_out_vec, sc_vec

    # print('deb000: len(labels_topn) ', len(labels_topn))
    total_cost_vec = knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
    # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
    norm_cost_vec = norm_cost(total_cost_vec)
    # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
    freq_weight_vec = freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                          mid_vec)
    # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
    norm_freq_vec = norm_freq_cost(freq_weight_vec)
    ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
    # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
    if ok_2D:
        # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
        if use_complete:
            norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
        else:
            norm_all_vec = get_merged_cost(norm_cost_vec, norm_freq_vec)
    else:
        print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
        norm_all_vec = []
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,

    cats_out_vec1, prob_out_vec1, sc_vec1 = valid3to2cats_find_tags_by_features(LGBM_Model1, feature_mat,
                                                                                norm_all_vec,
                                                                                ids_cats_vec, nclass,
                                                                                embed_only=embed_only,
                                                                                use_all=False, detect_CP=True,
                                                                                detect_CT=False, detect_UN=True)

    cats_out_vec1, prob_out_vec1, sc_vec1 = get_unique_vals(cats_out_vec1, prob_out_vec1, sc_vec1)

    cats_out_vec2, prob_out_vec2, sc_vec2 = valid3to2cats_find_tags_by_features(LGBM_Model2, feature_mat,
                                                                                norm_all_vec,
                                                                                ids_cats_vec, nclass,
                                                                                embed_only=embed_only,
                                                                                use_all=False, detect_CP=False,
                                                                                detect_CT=True, detect_UN=True)

    cats_out_vec2, prob_out_vec2, sc_vec2 = get_unique_vals(cats_out_vec2, prob_out_vec2, sc_vec2)

    # table truth
    is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1,
                                                                          cats_out_vec2, prob_out_vec2, sc_vec2)
    if not is_succeed:
        cats_out_vec12 = cats_out_vec.copy()
        prob_out_vec12 = prob_out_vec.copy()
        sc_vec12 = sc_vec.copy()
        global LGBM_Model3 #= detect_UN_CP_CT.LGBM_Model3
        cats_out_vec3, prob_out_vec3, sc_vec3 = valid3to2cats_find_tags_by_features(LGBM_Model3, feature_mat,
                                                                                    norm_all_vec, ids_cats_vec, nclass,
                                                                                    embed_only=embed_only,
                                                                                    use_all=False, detect_CP=True, detect_CT=True, detect_UN=False)

        print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
              sc_vec12)
        print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
              sc_vec3)

        cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)

    return cats_out_vec, prob_out_vec, sc_vec


#@staticmethod
def predict_LGBM_by_tags_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        batch_img_feature_tags_scores(img_vec, disp=disp)
    prob_vec = [0.0]*len(img_pieces_vec)
    category_vec = [0]*len(img_pieces_vec)

    #feature_mat = batch_img_features(img_vec)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = flatten(labels_topns)
        scores_topn_all = flatten(scores_topns)
        pred_lab_vec, prob_embed_vec, _ = three_models_features_tags_detection(labels_topn_all,
                                                                               scores_topn_all, image_feature_mat)

        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]
        startat = endat
    return category_vec, prob_vec



#@staticmethod
def predict_LGBM_from_fullimg(img, bbx_vec, disp=False):
    global embed_only #= detect_UN_CP_CT.embed_only
    img_vec = [[]]*len(bbx_vec)
    for index, bbx in enumerate(bbx_vec):
        cmin, rmin, cmax, rmax = bbx
        crop_img = img[rmin:rmax, cmin:cmax].copy()
        img_vec[index] = crop_img
    if embed_only:
        predictions_mat, score_mat = predict_LGBM_by_features(img_vec, disp=disp)
    else:
        predictions_mat, score_mat = predict_LGBM_by_tags_features(img_vec, disp=disp)
    return predictions_mat, score_mat



#all parallel
def all_three_models_features_detection_orig(feature_mat, img_pieces_vec):
    global LGBM_Model1
    global LGBM_Model2
    global LGBM_Model3
    global table_dct
    global table_freq_dct
    global freq_mid_vec
    global mid_vec
    global nclass #= min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,


    cats_out_mat1, prob_out_mat1, sc_mat1, feature_pieces1 = predict_multiclass_lgbm_all(LGBM_Model1,
                                                                                         feature_mat, img_pieces_vec, nclass)

    for index, cats_out_vec1 in enumerate(cats_out_mat1):
        prob_out_vec1 = prob_out_mat1[index]
        sc_vec1 = sc_mat1[index]
        cats_out_vec1, prob_out_vec1, sc_vec1 = get_unique_vals([cats_out_vec1], [prob_out_vec1], sc_vec1)
        cats_out_mat1[index] = cats_out_vec1.copy()
        prob_out_mat1[index] = prob_out_vec1.copy()
        sc_mat1[index] = sc_vec1.copy()

    cats_out_mat2, prob_out_mat2, sc_mat2, feature_pieces2 = predict_multiclass_lgbm_all(LGBM_Model2,
                                                                                         feature_mat, img_pieces_vec, nclass)

    for index, cats_out_vec2 in enumerate(cats_out_mat2):
        prob_out_vec2 = prob_out_mat2[index]
        sc_vec2 = sc_mat2[index]
        cats_out_vec2, prob_out_vec2, sc_vec2 = get_unique_vals([cats_out_vec2], [prob_out_vec2], sc_vec2)
        cats_out_mat2[index] = cats_out_vec2.copy()
        prob_out_mat2[index] = prob_out_vec2.copy()
        sc_mat2[index] = sc_vec2.copy()

    # table truth
    cats_out_mat = []
    prob_out_mat = []
    sc_mat = []
    for index, cats_out_vec2 in enumerate(cats_out_mat2):
        prob_out_vec2 = prob_out_mat2[index]
        sc_vec2 = sc_mat2[index]
        cats_out_vec1 = prob_out_mat1[index]
        prob_out_vec1 = prob_out_mat1[index]
        sc_vec1 = sc_mat1[index]
        try:
            is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table([cats_out_vec1], [prob_out_vec1], sc_vec1,
                                                                                  [cats_out_vec2], [prob_out_vec2], sc_vec2)
        except:
            print("exception estimate_truth_table [cats_out_vec1], [prob_out_vec1]")
            is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table(cats_out_vec1, prob_out_vec1,
                                                                                  sc_vec1,
                                                                                  cats_out_vec2, prob_out_vec2,
                                                                                  sc_vec2)
        if not is_succeed:
            cats_out_vec12 = cats_out_vec.copy()
            prob_out_vec12 = prob_out_vec.copy()
            sc_vec12 = sc_vec.copy()
            startat, endat = feature_pieces1[index]
            image_feature_mat = feature_mat[startat:endat]
            cats_out_vec3, prob_out_vec3, sc_vec3 = predict_multiclass_lgbm(LGBM_Model3, image_feature_mat, nclass)

            print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
                  sc_vec12)
            print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
                  sc_vec3)

            cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
            cats_out_mat += [cats_out_vec]
            prob_out_mat += [prob_out_vec]
            sc_mat += [sc_vec]
        else:
            cats_out_mat += [cats_out_vec]
            prob_out_mat += [prob_out_vec]
            sc_mat += [sc_vec]

    return cats_out_mat, prob_out_mat, sc_mat


#@staticmethod
def all_three_models_features_detection(feature_mat, img_pieces_vec):
    global LGBM_Model1 #= detect_UN_CP_CT.LGBM_Model1
    global LGBM_Model2 #= detect_UN_CP_CT.LGBM_Model2
    global table_dct #= detect_UN_CP_CT.table_dct
    global table_freq_dct #= detect_UN_CP_CT.table_freq_dct
    global freq_mid_vec #= detect_UN_CP_CT.freq_mid_vec
    global mid_vec #= detect_UN_CP_CT.mid_vec
    global nclass #= min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,


    cats_out_mat1, prob_out_mat1, sc_mat1, feature_pieces1 = predict_multiclass_lgbm_all(LGBM_Model1, feature_mat,
                                                                                         img_pieces_vec, nclass)

    cats_out_mat2, prob_out_mat2, sc_mat2, feature_pieces2 = predict_multiclass_lgbm_all(LGBM_Model2, feature_mat,
                                                                                         img_pieces_vec, nclass)

    #print("0deb sc_mat1 ", np.shape(sc_mat1), sc_mat1)
    #print("0deb sc_mat2 ", np.shape(sc_mat2), sc_mat2)
    for index, ccat1 in enumerate(cats_out_mat1):
        sc_vec1 = sc_mat1[index]
        cats_out_vec1, prob_out_vec1, sc_vec1 = get_unique_vals([ccat1], [prob_out_mat1[index]], sc_vec1)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()


    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        cats_out_vec2, prob_out_vec2, sc_vec2 = get_unique_vals([ccat2], [prob_out_mat2[index]], sc_vec2)
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    # print("1deb sc_mat1 ", np.shape(sc_mat1), sc_mat1)
    #print("1deb sc_mat2 ", np.shape(sc_mat2), sc_mat2)
    # table truth
    cats_out_mat = [0]*len(cats_out_mat2)
    prob_out_mat = [0.0]*len(cats_out_mat2)
    sc_mat = [[]]*len(cats_out_mat2)
    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        sc_vec1 = sc_mat1[index]
        #print("deb index, sc_vec1 ", index, sc_vec1)
        # print("deb index, sc_vec2 ", index, sc_vec2)
        is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table([cats_out_mat1[index]],
                                                                              [prob_out_mat1[index]], sc_vec1,
                                                                              [ccat2], [prob_out_mat2[index]], sc_vec2)
        if not is_succeed:
            cats_out_vec12 = cats_out_vec.copy()
            prob_out_vec12 = prob_out_vec.copy()
            sc_vec12 = sc_vec.copy()
            global LGBM_Model3 #= detect_UN_CP_CT.LGBM_Model3
            startat, endat = feature_pieces1[index]
            image_feature_mat = feature_mat[startat:endat]
            cats_out_vec3, prob_out_vec3, sc_vec3 = predict_multiclass_lgbm(LGBM_Model3, image_feature_mat, nclass)

            print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
                  sc_vec12)
            print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
                  sc_vec3)

            cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = sc_vec
        else:
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = sc_vec

    return cats_out_mat, prob_out_mat, sc_mat


#@staticmethod
def all_three_models_tags_features_detection(feature_mat, labels_topn_mat, scores_topn_mat,
                                             img_pieces_vec, embed_only=False, disp=False):
    global LGBM_Model1 #= detect_UN_CP_CT.LGBM_Model1
    global LGBM_Model2 #= detect_UN_CP_CT.LGBM_Model2
    global table_dct #= detect_UN_CP_CT.table_dct
    global table_freq_dct #= detect_UN_CP_CT.table_freq_dct
    global freq_mid_vec #= detect_UN_CP_CT.freq_mid_vec
    global mid_vec #= detect_UN_CP_CT.mid_vec
    global ids_cats_vec #= detect_UN_CP_CT.ids_cats_vec
    global nclass #= min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,


    cats_out_mat1, prob_out_mat1, sc_mat1, feature_pieces1 = predict_multiclass_lgbm_all(LGBM_Model1, feature_mat,
                                                                                         img_pieces_vec, nclass)

    cats_out_mat2, prob_out_mat2, sc_mat2, feature_pieces2 = predict_multiclass_lgbm_all(LGBM_Model2, feature_mat,
                                                                                         img_pieces_vec, nclass)

    for index, pieces in enumerate(img_pieces_vec):
        startat, endat = feature_pieces1[index]
        cats_out_vec1 = [cats_out_mat1[index]]
        prob_out_vec1 = [prob_out_mat1[index]]
        sc_vec1 = sc_mat1[index]
        cats_out_vec2 = [cats_out_mat2[index]]
        prob_out_vec2 = [prob_out_mat2[index]]
        sc_vec2 = sc_mat2[index]
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        #image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = flatten(labels_topns)
        scores_topn_all = flatten(scores_topns)
        labels_topn, scores_topn = most_topn_from_all(labels_topn_all, scores_topn_all)

        if len(labels_topn) == 0:
            print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
            cats_out_mat1[index] = cats_out_vec1[0]
            prob_out_mat1[index] = prob_out_vec1[0]
            sc_mat1[index] = sc_vec1.copy()
            cats_out_mat2[index] = cats_out_vec2[0]
            prob_out_mat2[index] = prob_out_vec2[0]
            sc_mat2[index] = sc_vec2.copy()
            continue

        # print('deb000: len(labels_topn) ', len(labels_topn))
        total_cost_vec = knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
        # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
        norm_cost_vec = norm_cost(total_cost_vec)
        # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
        freq_weight_vec = freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                              mid_vec)
        # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
        norm_freq_vec = norm_freq_cost(freq_weight_vec)
        ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
        # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
        if ok_2D:
            # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
            norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
        else:
            print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
            #norm_all_vec = []
            cats_out_mat1[index] = cats_out_vec1[0]
            prob_out_mat1[index] = prob_out_vec1[0]
            sc_mat1[index] = sc_vec1.copy()
            cats_out_mat2[index] = cats_out_vec2[0]
            prob_out_mat2[index] = prob_out_vec2[0]
            sc_mat2[index] = sc_vec2.copy()
            continue
        cats_out_vec1, prob_out_vec1 = all_find_tags_by_features(cats_out_vec1, prob_out_vec1, norm_all_vec,
                                                                 ids_cats_vec, nclass,
                                                                 embed_only=embed_only,
                                                                 use_all=False, detect_CP=True,
                                                                 detect_CT=False, detect_UN=True)

        cats_out_vec2, prob_out_vec2 = all_find_tags_by_features(cats_out_vec2, prob_out_vec2, norm_all_vec,
                                                                 ids_cats_vec, nclass,
                                                                 embed_only=embed_only,
                                                                 use_all=False, detect_CP=False,
                                                                 detect_CT=True, detect_UN=True)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    for index, ccat1 in enumerate(cats_out_mat1):
        sc_vec1 = sc_mat1[index]
        cats_out_vec1, prob_out_vec1, sc_vec1 = get_unique_vals([ccat1], [prob_out_mat1[index]], sc_vec1)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()


    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        cats_out_vec2, prob_out_vec2, sc_vec2 = get_unique_vals([ccat2], [prob_out_mat2[index]], sc_vec2)
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    # table truth
    cats_out_mat = [0]*len(cats_out_mat2)
    prob_out_mat = [0.0]*len(cats_out_mat2)
    sc_mat = [[]]*len(cats_out_mat2)
    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        sc_vec1 = sc_mat1[index]
        is_succeed, cats_out_vec, prob_out_vec, sc_vec = estimate_truth_table([cats_out_mat1[index]],
                                                                              [prob_out_mat1[index]], sc_vec1,
                                                                              [ccat2], [prob_out_mat2[index]], sc_vec2)
        if not is_succeed:
            global LGBM_Model3 #= detect_UN_CP_CT.LGBM_Model3
            startat, endat = feature_pieces1[index]
            image_feature_mat = feature_mat[startat:endat]
            cats_out_vec3, prob_out_vec3, sc_vec3 = predict_multiclass_lgbm(LGBM_Model3, image_feature_mat, nclass)
            labels_topns = labels_topn_mat[startat:endat]
            scores_topns = scores_topn_mat[startat:endat]
            # image_feature_mat = feature_mat[startat:endat]
            labels_topn_all = flatten(labels_topns)
            scores_topn_all = flatten(scores_topns)
            labels_topn, scores_topn = most_topn_from_all(labels_topn_all, scores_topn_all)
            if len(labels_topn) == 0:
                print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
                cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
                cats_out_mat[index] = cats_out_vec[0]
                prob_out_mat[index] = prob_out_vec[0]
                sc_mat[index] = sc_vec.copy()
                continue

            # print('deb000: len(labels_topn) ', len(labels_topn))
            total_cost_vec = knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
            # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
            norm_cost_vec = norm_cost(total_cost_vec)
            # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
            freq_weight_vec = freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                                  mid_vec)
            # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
            norm_freq_vec = norm_freq_cost(freq_weight_vec)
            ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
            # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
            if ok_2D:
                # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
                norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
            else:
                print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
                # norm_all_vec = []
                cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
                cats_out_mat[index] = cats_out_vec[0]
                prob_out_mat[index] = prob_out_vec[0]
                sc_mat[index] = sc_vec.copy()
                continue

            cats_out_vec3, prob_out_vec3 = all_find_tags_by_features(cats_out_vec3, prob_out_vec3, norm_all_vec,
                                                                     ids_cats_vec, nclass,
                                                                     embed_only=embed_only,
                                                                     use_all=False, detect_CP=True,
                                                                     detect_CT=True, detect_UN=False)

            if disp:
                cats_out_vec12 = cats_out_vec.copy()
                prob_out_vec12 = prob_out_vec.copy()
                sc_vec12 = sc_vec.copy()
                print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ",
                      cats_out_vec12, prob_out_vec12, sc_vec12)
                print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ",
                      cats_out_vec3, prob_out_vec3, sc_vec3)

            cats_out_vec, prob_out_vec, sc_vec = get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = [sc_vec]

        else:
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = [sc_vec]

    return cats_out_mat, prob_out_mat, sc_mat


#@staticmethod
def all_predict_LGBM_by_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, img_pieces_vec, img_dims_vec = batch_img_features(img_vec, disp=disp)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))

    predictions_mat, score_mat, sc_mat = all_three_models_features_detection(feature_mat, img_pieces_vec)
    return predictions_mat, score_mat


#@staticmethod
def all_predict_LGBM_by_tags_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        batch_img_feature_tags_scores(img_vec, disp=disp)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))

    predictions_mat, score_mat, sc_mat = all_three_models_tags_features_detection(feature_mat,
                                                                                  labels_topn_mat, scores_topn_mat, img_pieces_vec)
    return predictions_mat, score_mat



#@staticmethod
def all_predict_LGBM_from_fullimg(img, bbx_vec, disp=False):
    global embed_only #= detect_UN_CP_CT.embed_only
    img_vec = [[]]*len(bbx_vec)
    for index, bbx in enumerate(bbx_vec):
        cmin, rmin, cmax, rmax = bbx
        crop_img = img[rmin:rmax, cmin:cmax].copy()
        img_vec[index] = crop_img

    if embed_only:
        predictions_mat, score_mat = all_predict_LGBM_by_features(img_vec, disp=disp)
    else:
        predictions_mat, score_mat = all_predict_LGBM_by_tags_features(img_vec, disp=disp)
    return predictions_mat, score_mat



#@staticmethod
def parallel_predict_LGBM_by_tags_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        batch_img_feature_tags_scores(img_vec, disp=disp)
    prob_vec = [0.0]*len(img_pieces_vec)
    category_vec = [0]*len(img_pieces_vec)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = flatten(labels_topns)
        scores_topn_all = flatten(scores_topns)
        pred_lab_vec, prob_embed_vec, sc_vec = three_models_features_tags_detection(labels_topn_all,
                                                                                    scores_topn_all, image_feature_mat)

        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]
        startat = endat
    return category_vec, prob_vec

print("\n  load functions Finished")



# Step 3: How to use ?
# use function: all_predict_LGBM_from_fullimg
# input args:
# src_img = source image on the fly
# src_bbx_vec = boundary boxes (crops) array
# disp = boolean flag to display notes default False
# output arguments:
# predictions_mat = categries  list prediced for each crop, values 0,1,2
# use monitor_lab_cat to get the category
# score_mat = confidence score predicted for each crop

#demo
run_demo = False
if run_demo:
    import math
    import numpy as np
    #from timebudget import timebudget
    import ipyparallel as ipp
    import ray

    #import matplotlib.pyplot as plt
    import cv2
    from file_utils import load_pickle_file

    #data_path = "D:/GW/senecio/data/CaliforniaYoloCounty/"
    #img_path = data_path + "Replicate #1/Culex pipiens f/"
    i134_name =  "134_MWYwMWVhYThmY2E1NzljYw_1619440343.png"
    #i119_name =  "119_YjA0YTQyOWRiNjRkZDE2NQ_1619440343.png"
    img_path2 =  "Replicate #1/Culex tarsalis f2/"
    #i11_name =  "11.bmp"
    src_imgCP1 = cv2.imread(i134_name)
    #src_imgCP2 = cv2.imread(i119_name)
    #src_imgCT = cv2.imread(i11_name)
    print("src_imgCP1.shape ", src_imgCP1.shape)
    src_isr_cp = "D:/GW/senecio/data/Israel2Crop/Replicate#3/Culex pipiens/18.bmp"
    cpick = "final_all_withcorrections_imgfilename2bbx_1199_2021-09-14.pickle"
    imgfilename2bbx = load_pickle_file(cpick)
    print("len(imgfilename2bbx) ", len(imgfilename2bbx))
    data_path = "D:/GW/senecio/data/CaliforniaYoloCounty/"
    img_path = data_path + "Replicate #1/Culex pipiens f/"
    i134_name = img_path + "134_MWYwMWVhYThmY2E1NzljYw_1619440343.png"
    #i119_name = img_path + "119_YjA0YTQyOWRiNjRkZDE2NQ_1619440343.png"
    #img_path2 = data_path + "Replicate #1/Culex tarsalis f2/"
    #i11_name = img_path2 + "11.bmp"


    bbx_vec_CP1 = imgfilename2bbx[i134_name][2]
    small_bbx_vec_CP1 = imgfilename2bbx[i134_name][5]
    print("bbx_vec_CP1 ", len(bbx_vec_CP1), bbx_vec_CP1, len(small_bbx_vec_CP1), small_bbx_vec_CP1)
    #bbx_vec_CP2 = imgfilename2bbx[i119_name][2]
    #small_bbx_vec_CP2 = imgfilename2bbx[i119_name][5]
    #print("bbx_vec_CP2 ", len(bbx_vec_CP2), bbx_vec_CP2, len(small_bbx_vec_CP2), small_bbx_vec_CP2)
    #bbx_vec_CT = imgfilename2bbx[i11_name][2]
    #small_bbx_vec_CT = imgfilename2bbx[i11_name][5]
    #print("bbx_vec_CT ", len(bbx_vec_CT), bbx_vec_CT, len(small_bbx_vec_CT), small_bbx_vec_CT)
    print("\n end!")

    #%%time
    import datetime as dt
    embed_only=True
    print('start time, embed_only =', str(dt.datetime.now()), embed_only)

    class Timer0():
        def __init__(self):
            self.start_dt = None

        def start(self):
            self.start_dt = dt.datetime.now()

        def stop(self, sff=''):
            end_dt = dt.datetime.now()
            print(f'Time taken:  {(end_dt - self.start_dt)} : {sff}')

    disp0=True
    tot_crops = len(bbx_vec_CP1)
    print("tot_crops, embed_only, disp0 ", tot_crops, embed_only, disp0)
    from timeit import default_timer as timer
    timer0 = Timer0()
    timer0.start()
    start_time = timer()
    print("timeit start_time = ", start_time)
    predictions_mat, score_mat = all_predict_LGBM_from_fullimg(src_imgCP1, bbx_vec_CP1, disp=disp0)
    end_time = timer()
    timer0.stop()
    print("timeit end_time = ", end_time)
    time_diff = (end_time-start_time)
    print("timeit dt [minutes] = ", time_diff/60)
    print("timeit/tot_crops [seconds] = ", time_diff/tot_crops)
    print('end test time =', str(dt.datetime.now()))

    print("predictions_mat ", predictions_mat)
    print("score_mat ", score_mat)
