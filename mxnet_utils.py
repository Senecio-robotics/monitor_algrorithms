#from collections import namedtuple
try:
    from collections.abc import namedtuple
except ImportError:
    from collections import namedtuple

import mxnet
import numpy

#from file_utils import external_path
from response_utils import MlException
from image_utils import DEFAULT_IMAGE_SIZE
BATCH_TUPLE = namedtuple('Batch', ['data'])
#print("BATCH_TUPLE = ", BATCH_TUPLE, type(BATCH_TUPLE))

# ML: image recognition models
#prediction_model = None
#batch_prediction_model = None
#features_model = None
#combined_model = None
# TODO: merge BATCH / sim & FEATURE methods

def load_resnet152_model(prefix='resnet-152', checkpoint_epoch=0, feature_mod=2,
                         shape=(1, 3, DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE), device=mxnet.cpu()):#, ctx=mxnet.cpu()):
    sym, arg_params, aux_params = mxnet.model.load_checkpoint(prefix, checkpoint_epoch)
    devices = {'cpu': mxnet.cpu(), 'gpu': mxnet.gpu(0), 'gpucpu': [mxnet.gpu(0), mxnet.cpu()]}
    devs = devices.get(device, mxnet.cpu())
    if feature_mod == 2:  # get features only

        print("load_resnet152_model: MXNet features only model:[" + prefix + "]")
        all_layers = sym.get_internals()
        fe_sym = all_layers['flatten0_output']
        mod = mxnet.mod.Module(symbol=fe_sym, context=devs, label_names=None)
        mod.bind(for_training=False, data_shapes=[('data', shape)])
        mod.set_params(arg_params, aux_params)
        print("load_resnet152_model: MXNet features only model:[" + prefix + "] loaded successfully")

    elif feature_mod == 1:  # get lables/scores only

        print("load_resnet152_model: MXNet lables-scores only model:[" + prefix + "]")
        mod = mxnet.mod.Module(symbol=sym, context=devs, label_names=None)
        mod.bind(for_training=False, data_shapes=[('data', shape)], label_shapes=mod._label_shapes)
        mod.set_params(arg_params, aux_params, allow_missing=True)
        print("load_resnet152_model: MXNet lables-scores only model:[" + prefix + "] loaded successfully")

    else:  # Combined symbol to get both scores and features

        print("load_resnet152_model: MXNet Combined features-lables-scores model:[" + prefix + "]")
        flatten0 = sym.get_children()[0].get_children()[0]
        combined_sym = mxnet.sym.concat(*[flatten0, sym], name="prediction_with_embedding")
        mod = mxnet.mod.Module(symbol=combined_sym, context=devs, label_names=None)
        mod.bind(for_training=False, data_shapes=[('data', shape)], label_shapes=mod._label_shapes)
        mod.set_params(arg_params, aux_params, allow_missing=True)
        print("load_resnet152_model: MXNet Combined features-lables-scores model:[" + prefix + "] loaded successfully")

    return mod

####combined functions: mxnet combined( feature+prediction ) model
def load_combined_model(prefix, checkpoint_epoch, shape, device):
    print("load_combined_model: MXNet combined_model:[" + prefix + "]")
    sym, arg_params, aux_params = mxnet.model.load_checkpoint(prefix, checkpoint_epoch)
    devices = {'cpu': mxnet.cpu(), 'gpu': mxnet.gpu(0), 'gpucpu': [mxnet.gpu(0), mxnet.cpu()]}
    devs = devices.get(device, mxnet.cpu())
    flatten0 = sym.get_children()[0].get_children()[0]
    combined_sym = mxnet.sym.concat(*[flatten0, sym], name="prediction_with_embedding")
    mod = mxnet.mod.Module(symbol=combined_sym, context=devs, label_names=None)
    mod.bind(for_training=False, data_shapes=[('data', shape)], label_shapes=mod._label_shapes)
    mod.set_params(arg_params, aux_params, allow_missing=True)
    print("load_combined_model: MXNet combined_model:[" + prefix + "] loaded successfully")
    return mod

####resnet feature functions: mxnet feature model
def load_feature_model(prefix, checkpoint_epoch, shape, device):
    print("load_feature_model: MXNet model:[" + prefix + "]")
    sym, arg_params, aux_params = mxnet.model.load_checkpoint(prefix, checkpoint_epoch)
    all_layers = sym.get_internals()
    devices = {'cpu': mxnet.cpu(), 'gpu': mxnet.gpu(0), 'gpucpu': [mxnet.gpu(0), mxnet.cpu()]}
    devs = devices.get(device, mxnet.cpu())
    fe_sym = all_layers['flatten0_output']
    mod = mxnet.mod.Module(symbol=fe_sym, label_names=None, context=devs)
    mod.bind(for_training=False, data_shapes=[('data', shape)])  # (batch_size,3,224,224)
    mod.set_params(arg_params, aux_params)
    print("load_feature_model: MXNet model:[" + prefix + "] loaded successfully")
    return mod

####resnet prediction functions: mxnet prediction model
def load_prediction_model(prefix, checkpoint_epoch, shape, device):
    print("load_prediction_model: MXNet sim model:[" + prefix + "]")
    sym, arg_params, aux_params = mxnet.model.load_checkpoint(prefix, checkpoint_epoch)
    all_layers = sym.get_internals()
    devices = {'cpu': mxnet.cpu(), 'gpu': mxnet.gpu(0), 'gpucpu': [mxnet.gpu(0), mxnet.cpu()]}
    devs = devices.get(device, mxnet.cpu())

    mod = mxnet.mod.Module(symbol=sym, label_names=None, context=devs)
    mod.bind(for_training=False, data_shapes=[('data', shape)], label_shapes=mod._label_shapes)
    mod.set_params(arg_params, aux_params, allow_missing=True)

    print("load_prediction_model: MXNet sim model:[" + prefix + "] loaded successfully")
    return mod


def get_mxnet_combined_data_model(combined_model, img_batch: numpy.ndarray):  # model=combined_model
    try:
        combined_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = combined_model.get_outputs()[0]
        feature_prob_mat = mxnet_outputs.asnumpy()
        return feature_prob_mat
    except Exception as inst:
        raise MlException("Fail predict_mxnet_combined had exception on : ".format(str(inst)))

def get_mxnet_combined_data(combined_model, img_batch: numpy.ndarray): #model=combined_model
    try:
        combined_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = combined_model.get_outputs()[0]
        feature_prob_mat = mxnet_outputs.asnumpy()
        return feature_prob_mat
    except Exception as inst:
        raise MlException("Fail predict_mxnet_combined had exception on : ".format(str(inst)))


def get_mxnet_features_model(features_model, img_batch: numpy.ndarray):
    try:
        features_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = features_model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res[0]
    except Exception as inst:
        raise MlException("Fail get_mxnet_features had exception on : ".format(str(inst)))

def get_mxnet_features(features_model, img_batch: numpy.ndarray): #, model=features_model
    try:
        features_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = features_model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res[0]
    except Exception as inst:
        raise MlException("Fail predict_mxnet had exception on : ".format(str(inst)))


# https://stackoverflow.com/questions/58352859/memory-leak-when-running-mxnet-cpu-inference
# I suspect your .asnumpy() call is associated with high memory usage because it is a blocking call:
# it requires immediate availability of results and consequently forces the mxnet engine to compute
# necessary dependencies immediately. It is generally recommended to avoid using Numpy in MXNet code
# and use MXNet NDArray instead, which is more appropriate for deep learning (asynchronous,
# GPU-compatible, support for automatic differentiation) than Numpy. For example you could accumulate
# any info you need in an MXNet NDArray and then do whatever you need with it at the end of the
# execution (save to file, convert to Numpy, etc).
def get_mxnet_features_batch_model(features_model, img_batch: numpy.ndarray):
    try:
        features_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = features_model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res
    except Exception as inst:
        raise MlException("Fail get_mxnet_features_batch_model had exception on : ".format(str(inst)))

def get_mxnet_features_batch(features_model, img_batch: numpy.ndarray): #, model=features_model
    try:
        features_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = features_model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res
    except Exception as inst:
        print(f'exception: get_mxnet_features_batch 6')
        raise MlException("Fail get_mxnet_features_batch had exception on : ".format(str(inst)))



def get_mxnet_prediction_model(prediction_model, img_batch: numpy.ndarray):
    try:
        prediction_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = prediction_model.get_outputs()[0]
        prob = mxnet_outputs.asnumpy()
        return prob
    except Exception as inst:
        raise MlException("Fail get_mxnet_prediction had exception on : ".format(str(inst)))

def get_mxnet_prediction(prediction_model, img_batch: numpy.ndarray): #model=prediction_model
    try:
        prediction_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = prediction_model.get_outputs()[0]
        prob = mxnet_outputs.asnumpy()
        return prob
    except Exception as inst:
        raise MlException("Fail get_mxnet_prediction had exception on : ".format(str(inst)))


def get_batch_mxnet_prediction(batch_prediction_model, img_batch: numpy.ndarray):
    try:
        batch_prediction_model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = batch_prediction_model.get_outputs()[0]
        prob = mxnet_outputs.asnumpy()
        return prob
    except Exception as inst:
        raise MlException("Fail get_mxnet_prediction had exception on : ".format(str(inst)))

# ======================================================================================================================

def predict_mxnet(model, img_batch: numpy.ndarray):
    try:
        #BATCH = namedtuple('Batch', ['data'])
        model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res
    except Exception as e:
        print('Fail MXNet predict: [{}]'.format(str(e)))

class MXNetBatchService(object):
    def __init__(self, modelPrefix: str, modelEpoch: int, shape, device: str, mode: int):
        try:
            print("MXNetBatchService model.load started: modelPrefix, modelEpoch, shape, device, mode = ",
                  modelPrefix, modelEpoch, shape, device, mode)
            self.mode = mode
            #self.model = load_feature_model(modelPrefix, modelEpoch, shape, device)
            self.model = load_resnet152_model(prefix=modelPrefix, checkpoint_epoch=modelEpoch, feature_mod=mode,
                                 shape=shape, device=device)
            print("MXNetBatchService model.load completed: ", modelPrefix, modelEpoch, shape, device, mode)
        except Exception as e:
            print('Fail to load MXNet model files: [{}]'.format(modelPrefix))

    def predict(self, img_batch: numpy.ndarray) -> numpy.ndarray:
        return predict_mxnet(self.model, img_batch)


# ======================================================================================================================

######### for Data

def get_mxnet_features_batch0(model, img_batch: numpy.ndarray): #, model=features_model
    try:
        model.forward(BATCH_TUPLE([mxnet.nd.array(img_batch)]))
        mxnet_outputs = model.get_outputs()[0]
        mxnet_res = mxnet_outputs.asnumpy()
        return mxnet_res
    except Exception as inst:
        print(f'exception: get_mxnet_features_batch 6')
        raise MlException("Fail get_mxnet_features_batch0 had exception on : ".format(str(inst)))


