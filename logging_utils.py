# Fix warning message: MonkeyPatchWarning: Monkey-patching ssl after ssl has already been imported may lead to errors ..
#from gevent import monkey
#monkey.patch_all()

import datetime
import logging
import socket
from logentries import LogentriesHandler

from env_configuration import MLConfiguration
from env_utils import get_env_str


LOGENTRIES_TEMPLATE = '[{}] %(asctime)s [%(process)d|%(threadName)s] [%(levelname)-5.5s] [%(module)s:%(lineno)s:%(funcName)s] %(message)s'

# Use it only for local development:
CONSOLE_TEMPLATE = '%(asctime)s [%(process)d|%(threadName)s] [%(module)s:%(lineno)s:%(funcName)s] %(message)s'
FILE_TEMPLATE = '%(message)s'
LOG_FILE = get_env_str('ML_LOG_FILE', None)


def init_logger() -> logging.Logger:
    logger = logging.getLogger()
    logging.getLogger("werkzeug").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    logging.getLogger('boto3').setLevel(logging.WARNING)
    logging.getLogger('botocore').setLevel(logging.WARNING)
    logging.getLogger('nose').setLevel(logging.WARNING)

    logger.setLevel(MLConfiguration.LOG_MODE)

    init_console_logger(logger)
    init_logentries_logger(logger)

    if LOG_FILE is not None:
        init_file_logger(logger, LOG_FILE)
        
    logger.propagate = False
    
    return logger


def init_console_logger(logger):
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(logging.Formatter(CONSOLE_TEMPLATE))
    logger.addHandler(console_handler)


def init_logentries_logger(logger):
    if MLConfiguration.LOGENTRIES_TOKEN is not None:
        # formatter = logging.Formatter(LOGENTRIES_TEMPLATE.format(MLConfiguration.WEAVO_MODE))
        formatter = logging.Formatter(LOGENTRIES_TEMPLATE.format(socket.gethostname()))
        logentries_handler = LogentriesHandler(MLConfiguration.LOGENTRIES_TOKEN)
        logentries_handler.setFormatter(formatter)
        logger.addHandler(logentries_handler)


def init_file_logger(logger, log_file: str):
    dt = datetime.datetime.now().strftime(".%y-%m-%d-%H-%M")
    dt = datetime.datetime.now().strftime("_%m%d-%H%M.txt")
    pos_dot = log_file.find('.')
    if pos_dot != -1:
        log_file = log_file[0:pos_dot]
    log_file_name = log_file + dt
    file_handler = logging.FileHandler(log_file_name)
    file_handler.setFormatter(logging.Formatter(FILE_TEMPLATE))
    logger.addHandler(file_handler)


def debug(obj):
    if obj is None:
        return "None"
    else:
        value = ""
        if isinstance(obj, list):
            for entity_data in obj:
                value += "\n" + str(entity_data)
            return value
        elif isinstance(obj, dict):
            for key, val in obj.items():
                value += "\n" + str(key)+" "+str(val)
            return value
        else:
            return str(obj)


LOG = init_logger()
