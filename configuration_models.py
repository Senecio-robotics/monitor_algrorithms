from enum import Enum
from env_utils import get_env_str
#ML_DIRECTORY = get_env_str('ML_DIRECTORY', '../../resources/external/')
ML_DIRECTORY = get_env_str('ML_DIRECTORY', 'D:/GW/senecio/data/resources/external/')
MXNET_DIRECTORY = get_env_str('MXNET_DIRECTORY', 'D:/GW/senecio/data/resources/external/')
#C:\Users\gabiw\resources\external\resnet-152

class AwsAuthMode(Enum):
    EC2 = "EC2"
    IAM = "IAM"


class AwsS3Keys(object):
    def __init__(
            self,
            AWS_CREDENTIAL_TYPE: str,
            AWS_KEY: str,
            AWS_SECRET: str,
            AWS_S3_REGION: str,
            AWS_S3_BUCKET_NAME: str,
            AWS_S3_CONFIGURATION_BUCKET_NAME: str
    ):
        self.aws_credential_type = AwsAuthMode[AWS_CREDENTIAL_TYPE]
        self.aws_access_key_id = AWS_KEY
        self.aws_secret_access_key = AWS_SECRET
        self.region_name = AWS_S3_REGION
        self.bucket = AWS_S3_BUCKET_NAME
        self.configuration_bucket = AWS_S3_CONFIGURATION_BUCKET_NAME

    def __str__(self):
        return str(self.__dict__)


class AwsS3ModelKeys(object):
    def __init__(
            self,
            MXNET_PREFIX: str,
            MXNET_PARAMS: str,
            MXNET_JSON: str,

            #LGBM_SPECIES: str,
           # LGBM_GENUS: str
    ):
        self.MXNET_PREFIX = MXNET_PREFIX
        self.MXNET_PARAMS = MXNET_PARAMS
        self.MXNET_JSON = MXNET_JSON
        #self.LGBM_SPECIES = LGBM_SPECIES
        #self.LGBM_GENUS = LGBM_GENUS
        self.ENV_DIRECTORY = ML_DIRECTORY + "mosq_noise/"
        self.MXNET_DIRECTORY = ML_DIRECTORY + "resnet-152/"

    def __str__(self):
        return str(self.__dict__)


class NOISE_FILTER_Files(object):
    def __init__(
            self,
            NMM_MODEL: str,
            NMM_TBL_MODEL: str,
            NMM_TBL: str,
            NMM_FREQ_TBL: str,
            #NMM_MLARAM_MODEL: str,
            NMM_LABELS: str
    ):
        self.NMM_MODEL = NMM_MODEL
        self.NMM_TBL_MODEL = NMM_TBL_MODEL
        self.NMM_TBL = NMM_TBL
        self.NMM_FREQ_TBL = NMM_FREQ_TBL
        #self.SIM_MLARAM_MODEL = NMM_MLARAM_MODEL
        self.NMM_LABELS = NMM_LABELS
        self.ENV_DIRECTORY = ML_DIRECTORY + "mosq_noise/"
        self.MXNET_DIRECTORY = ML_DIRECTORY + "resnet-152/"

    def __str__(self):
        return str(self.__dict__)


class Configuration(object):
    def __init__(
            self,
            SERO_MODE: str,
            LOGENTRIES_TOKEN: str,
            LOG_MODE: str,
            IMAGE_BATCH_SIZE: int,
            ML_SPECIES_IMAGE_BATCH_SIZE: int,
            AWS_S3_ML_MODELS_BUCKET: str
    ):
        self.SERO_MODE = SERO_MODE
        self.LOGENTRIES_TOKEN = LOGENTRIES_TOKEN
        self.LOG_MODE = LOG_MODE
        self.IMAGE_BATCH_SIZE = IMAGE_BATCH_SIZE
        self.ML_SPECIES_IMAGE_BATCH_SIZE = ML_SPECIES_IMAGE_BATCH_SIZE
        self.AWS_S3_ML_MODELS_BUCKET = AWS_S3_ML_MODELS_BUCKET

    def __str__(self):
        return str(self.__dict__)

