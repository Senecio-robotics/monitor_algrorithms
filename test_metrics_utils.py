########### start statistics functions ##############################################
import datetime as dt
import csv




def get_total_spec(top_labs_freq_dict):
    tot_freq = 0  # sum(top_TP_labs)
    for k, freq in top_labs_freq_dict.items():
        tot_freq += freq
    return tot_freq


def get_intersect_pred_exp(expected_labs, best_labs):
    intersect_num = 0
    for clab in best_labs:
        if clab in expected_labs:
            intersect_num += 1
    return intersect_num


def get_rec_prec(expected_labs, best_labs, local_eps=2.2e-16):
    intersect_num = get_intersect_pred_exp(expected_labs, best_labs)  # TP
    prec_per_samp = intersect_num / max(len(best_labs), local_eps)  # TP/(TP+FP)
    rec_per_samp = intersect_num / max(len(expected_labs), local_eps)  # TP/(TP+FN)
    return prec_per_samp, rec_per_samp


def update_add_dict(in_dict, clab):
    if clab not in in_dict:
        in_dict[clab] = 1
    else:
        in_dict[clab] += 1
    return in_dict


def add_TP_dict(TP_labs_dict, expected_TP, best_labs):
    for ii in range(len(best_labs)):
        clab = best_labs[ii]
        if clab in expected_TP:
            TP_labs_dict = update_add_dict(TP_labs_dict, clab)
    return TP_labs_dict


def add_FP_dict(FP_labs_dict, expected_TP, best_labs):
    for ii in range(len(best_labs)):
        clab = best_labs[ii]
        if clab not in expected_TP:
            FP_labs_dict = update_add_dict(FP_labs_dict, clab)
    return FP_labs_dict


def add_TN_dict(TN_labs_dict, expected_TP, best_labs, species_lab_cat):
    # global species_lab_cat
    expected_TN = [clab for clab in species_lab_cat if clab not in expected_TP]
    for ii in range(len(expected_TN)):
        clab = expected_TN[ii]
        if clab not in best_labs:
            TN_labs_dict = update_add_dict(TN_labs_dict, clab)
    return TN_labs_dict


def add_FN_dict(FN_labs_dict, expected_TP, best_labs):
    for ii in range(len(expected_TP)):
        clab = expected_TP[ii]
        if clab not in best_labs:
            FN_labs_dict = update_add_dict(FN_labs_dict, clab)
    return FN_labs_dict


def calc_rec_per_acc(tpfpfntn_vec):
    num_TP = tpfpfntn_vec[0]
    num_FP = tpfpfntn_vec[1]
    num_FN = tpfpfntn_vec[2]
    num_TN = tpfpfntn_vec[3]
    total_TP_FN = num_TP + num_FN
    recall = num_TP / max(total_TP_FN, 1.0e-16)
    recall = round(recall, 2)
    total_TP_FP = num_TP + num_FP
    precision = num_TP / max(total_TP_FP, 1.0e-16)
    precision = round(precision, 2)
    # print('recall =', recall0, 'precision  =', precision0)
    accuracy = (num_TP + num_TN) / max(num_TP + num_TN + num_FP + num_FN, 1.0e-16)
    accuracy = round(accuracy, 2)
    return recall, precision, accuracy


def show_results(num_TP_per_img, num_TN_per_img, num_FP_per_img, num_FN_per_img, alpha=0.5, show_out=False):
    if show_out:
        print('\n show_results: summary')
        print('num_TN_per_img =', num_TN_per_img, 'num_FP_per_img=', num_FP_per_img)
        print('num_TP_per_img =', num_TP_per_img, 'num_FN_per_img=', num_FN_per_img)

    total_TP_FN = num_TP_per_img + num_FN_per_img  # len(imageProbsTable_calrifgen_TP)
    total_TN_FP = num_TN_per_img + num_FP_per_img

    missing_percentage = num_FN_per_img / max(total_TP_FN, 1.0e-16)
    if show_out:
        print('\n per_img: FN err ( "missing percentage")  =', round(missing_percentage, 2))

    recall = num_TP_per_img / max(total_TP_FN, 1.0e-16)
    recall = round(recall, 2)
    if show_out:
        print('\n per_img: recall =', recall)
        # print('\n test =', len(FNList)/denom +num_TP_per_img/denom )
        print('\n per_img: FP err ( "False Alarm Rate") =', round(num_FP_per_img / max(total_TN_FP, 1.0e-16), 2))
    total_TP_FP = num_TP_per_img + num_FP_per_img
    precision = num_TP_per_img / max(total_TP_FP, 1.0e-16)
    precision = round(precision, 2)
    if show_out:
        print('\n per_img: precision  =', precision)

    num_fscore = precision * recall
    denom = alpha * precision + (1 - alpha) * recall
    Fscore = num_fscore / max(denom, 1.0e-16)
    Fscore = round(Fscore, 2)
    if show_out:
        print('\n per_img: F-Score  =', Fscore)
        print('\n "true negative rate" =', round(num_TN_per_img / max(total_TN_FP, 1.0e-16), 2))
    accuracy = (num_TP_per_img + num_TN_per_img) / max(
        num_TP_per_img + num_TN_per_img + num_FP_per_img + num_FN_per_img, 1.0e-16)
    accuracy = round(accuracy, 2)
    if show_out:
        print('\n per_img: Accuracy  =', accuracy)
    return precision, recall, Fscore, accuracy


def get_parms_results(num_TP_per_img, num_TN_per_img, num_FP_per_img, num_FN_per_img, alpha=0.5, show_out=False):
    if show_out:
        print('\n get_parms_results: summary')
        print('num_TN_per_img =', num_TN_per_img, 'num_FP_per_img=', num_FP_per_img)
        print('num_TP_per_img =', num_TP_per_img, 'num_FN_per_img=', num_FN_per_img)

    total_TP_FN = num_TP_per_img + num_FN_per_img  # len(imageProbsTable_calrifgen_TP)
    total_TN_FP = num_TN_per_img + num_FP_per_img

    missing_percentage = num_FN_per_img / max(total_TP_FN, 1.0e-16)
    if show_out:
        print('\n per_img: FN err ( "missing percentage")  =', round(missing_percentage, 2))

    recall = num_TP_per_img / max(total_TP_FN, 1.0e-16)
    # recall = round(recall, 2)
    if show_out:
        print('\n per_img: recall =', recall)
        # print('\n test =', len(FNList)/denom +num_TP_per_img/denom )
        print('\n per_img: FP err ( "False Alarm Rate") =', round(num_FP_per_img / max(total_TN_FP, 1.0e-16), 2))
    total_TP_FP = num_TP_per_img + num_FP_per_img
    precision = num_TP_per_img / max(total_TP_FP, 1.0e-16)
    # precision = round(precision, 2)
    if show_out:
        print('\n per_img: precision  =', precision)

    num_fscore = precision * recall
    denom = alpha * precision + (1 - alpha) * recall
    Fscore = num_fscore / max(denom, 1.0e-16)
    # Fscore = round(Fscore, 2)
    if show_out:
        print('\n per_img: F-Score  =', Fscore)
        print('\n "true negative rate" =', round(num_TN_per_img / max(total_TN_FP, 1.0e-16), 2))
    accuracy = (num_TP_per_img + num_TN_per_img) / max(
        num_TP_per_img + num_TN_per_img + num_FP_per_img + num_FN_per_img, 1.0e-16)
    # accuracy = round(accuracy, 2)
    if show_out:
        print('\n per_img: Accuracy  =', accuracy)
    return precision, recall, Fscore, accuracy


def get_statistics_params(TP_labs, TN_labs, FP_labs, FN_labs, ids_cats_vec, show_out=False):
    nclass = len(ids_cats_vec)  # global ids_cats_vec
    precision = [0] * nclass
    recall = [0] * nclass
    Fscore = [0] * nclass
    accuracy = [0] * nclass
    jj = 0
    for clab in ids_cats_vec:
        ii = clab
        if show_out:
            print('\n get_statistics_params : summary, label = ', type(ii), 'category = ', clab)
        precision[jj], recall[jj], Fscore[jj], accuracy[jj] = get_parms_results(TP_labs[ii], TN_labs[ii], FP_labs[ii],
                                                                                FN_labs[ii], alpha=0.5,
                                                                                show_out=show_out)
        if show_out:
            print('jj, precision, recall, Fscore, accuracy ', jj, precision[jj], recall[jj], Fscore[jj], accuracy[jj])
        jj += 1
    if show_out:
        print(' precision, recall, Fscore, accuracy ', precision, recall, Fscore, accuracy)
    return precision, recall, Fscore, accuracy


def estimate_micro_mean(TP_labs, TN_labs, FP_labs, FN_labs, ids_cats_vec, show_out=False):
    precision, recall, Fscore, accuracy = get_statistics_params(TP_labs, TN_labs, FP_labs, FN_labs, ids_cats_vec,
                                                                show_out)
    sum_prec = 0.0
    sum_rec = 0.0
    sum_f1 = 0.0
    sum_acc = 0.0
    cprec = 0
    crec = 0
    cf1 = 0
    cacc = 0
    for ii in range(len(precision)):
        if precision[ii] > 0.0:
            sum_prec += precision[ii]
            cprec += 1
        if recall[ii] > 0.0:
            sum_rec += recall[ii]
            crec += 1
        if Fscore[ii] > 0.0:
            sum_f1 += Fscore[ii]
            cf1 += 1
        if accuracy[ii] > 0.0:
            sum_acc += accuracy[ii]
            cacc += 1

    micro_avg_prec = sum_prec / max(cprec, 0.001)
    if show_out:
        print('micro_avg_prec: ', round(micro_avg_prec, 2))
    micro_avg_rec = sum_rec / max(crec, 0.001)
    if show_out:
        print('micro_avg_rec: ', round(micro_avg_rec, 2))
    micro_avg_f1 = sum_f1 / max(cf1, 0.001)
    if show_out:
        print('micro_avg_f1: ', round(micro_avg_f1, 2))
    micro_f1_avg = 2 * micro_avg_prec * micro_avg_rec / max(micro_avg_prec + micro_avg_rec, 1.0)
    if show_out:
        print('micro_f1_avg: ', round(micro_f1_avg, 2))
    micro_acc_avg = sum_acc / max(cacc, 0.001)
    if show_out:
        print('micro_acc_avg: ', round(micro_acc_avg, 2))

    return micro_avg_prec, micro_avg_rec, micro_avg_f1, micro_f1_avg, micro_acc_avg


def sort_dict(dict_in, sort_by_key=True):
    dict_sorted = dict()
    if sort_by_key:
        for key in sorted(dict_in.keys()):
            dict_sorted[key] = dict_in[key]
    else:
        pair_sorted_lst = sorted(dict_in.items(), key=lambda kv: kv[1])
        for pair in pair_sorted_lst:
            dict_sorted[pair[0]] = pair[1]
    return dict_sorted


def save_all_prec_rec_all_only(TP_labs, TN_labs, FP_labs, FN_labs, num_vids_dict, species_vec, ids_cats_vec,
                               prefix='all_approved_table_', root='/home/gabi_weizman/data/', show_out=False):
    nclass = len(ids_cats_vec)  # global species_vec, ids_cats_vec
    try:
        precision = [0] * nclass
        recall = [0] * nclass
        Fscore = [0] * nclass
        accuracy = [0] * nclass
        jj = 0
        acc_table = dict()
        for clab in ids_cats_vec:
            ii = clab
            ckey = species_vec[ii]
            num_vids = num_vids_dict[ii]
            if show_out:
                print('\n top multiclass video : summary, label = ', ii, 'category = ', clab, ckey)
            precision[jj], recall[jj], Fscore[jj], accuracy[jj] = get_parms_results(TP_labs[ii], TN_labs[ii],
                                                                                    FP_labs[ii],
                                                                                    FN_labs[ii], alpha=0.5,
                                                                                    show_out=show_out)
            acc_table[ckey] = [num_vids, TN_labs[ii], FN_labs[ii], FP_labs[ii], TP_labs[ii], recall[jj], precision[jj],
                               Fscore[jj], accuracy[jj]]
            if show_out:
                print('acc_table[ckey] ', ii, ckey, acc_table[ckey])
            jj += 1
        # print('deb01')
        # acc_table = dict()
        # for clab in all683_ids_vec:
        #    ii = clab
        #    jj = int(ii)
        #    num_vids = num_vids_dict[ii]
        #    print('deb num_vids_dict[ii] ', ii, num_vids_dict[ii], TN_labs[ii], FN_labs[ii], FP_labs[ii], TP_labs[ii])
        #    print(precision[jj], recall[jj], Fscore[jj], accuracy[jj])
        #    acc_table[ii] = [num_vids, TN_labs[ii], FN_labs[ii], FP_labs[ii], TP_labs[ii], recall[jj], precision[jj],
        #                     Fscore[jj], accuracy[jj]]
        #    print('acc_table[ii] ', ii, acc_table[ii] )
        # print(acc_table)
        if show_out:
            print('save table')

        out_struct = []
        out_struct.append(['category', 'images', 'TN', 'FN', 'FP', 'TP', 'recall', 'precision', 'Fscore', 'accuracy'])
        for key, val in acc_table.items():
            out_struct.append(
                [key] + [val[0]] + [val[1]] + [val[2]] + [val[3]] + [val[4]] + [val[5]] + [val[6]] + [val[7]] + [
                    val[8]])

        currentDT = str(dt.date.today())
        full_file_name = root + prefix + currentDT + '.csv'

        with open(full_file_name, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, dialect='excel')
            writer.writerows(out_struct)
        if show_out:
            print(' save_prec_rec_all: saved results in file = ', full_file_name)
    except Exception as inst:
        print('save_prec_rec_all: exception ', inst)

########### end statistics functions ##############################################
