import cv2
import glob
import os
from tqdm import tqdm
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg
#from data_functions import create_path

#from keyframe_image.change_img_name_by_keyframe_timestamp import chage_name_to_keyframe_timestamp
#from matplotlib import colors as mcolors
#from utilities_constants import BBOX_DRAW_COLORS

CROP_DRAW_COLORS = {'head':(0, 0, 255),'body':(0, 255, 0),'tail':(255, 0, 0)}

BBOX_DRAW_COLORS = {'female': (0, 0, 255),
               'male': (255, 0, 0),
               'many': (128, 0, 128),
               'suspect': (0, 140, 255),
               'discarded': (0, 0, 0),
               'arm': (0, 255, 0),
               'missing': (128, 0, 128),
               'cluster': (0, 255, 0)
               }

BASE_COLORS = {
    'b': (0, 0, 255),        # blue
    'g': (0, 255, 0),      # green
    'r': (255, 0, 0),        # red
    'c': (0, 191, 191),  # cyan
    'm': (191, 0, 191),  # magenta
    'y': (191, 191, 0),  # yellow
    'k': (0, 0, 0),        # black
    'w': (255, 255, 255),        # white
}


def in_memory_to_jpg(src_img,read_flag=cv2.IMREAD_GRAYSCALE):
    """
    change the format of an image in memory to jpg
    adapted from:
    https://stackoverflow.com/questions/40768621/python-opencv-jpeg-compression-in-memory
    """
    result, encimg = cv2.imencode('.jpg',src_img)
    return cv2.imdecode(encimg,read_flag)


def from_path_imgs_generator(src_path, cv2_flag=cv2.IMREAD_COLOR):
    """
    A generator that yields images and their names for a given source path
    Args:
        src_path: the path to where the images are, assumed to contain only
         the images
        cv2_flag: how to read the images

    Returns: yields the image name as a string and the image as a numpy array

    """
    im_paths = glob.glob(os.path.join(src_path, '*'))
    for path in im_paths:
        im_name = os.path.basename(path)
        im = cv2.imread(path, cv2_flag)
        if im is not None:
            yield im_name, im
        else:
            continue


def load_image_via_path(src_path,mode=cv2.IMREAD_COLOR):
    img = cv2.imread(src_path)
    if mode == cv2.IMREAD_COLOR:
        img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    return img

def save_image(dst_path,img):
    if len(img.shape) > 2:
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(dst_path, img)


def load_image_via_path_and_name(src_path, file_name, mode=cv2.IMREAD_COLOR):
    """
    load an image from path, if color image returns RGB instead of BGR of opencv
    Args:
        src_path:
        file_name:
        mode:

    Returns:

    """
    img_path = os.path.join(src_path,file_name,mode)
    return load_image_via_path(img_path,mode)



def create_color_dict(labels):
    """
    adapted from "https://stackoverflow.com/questions/22408237/named-colors-in-matplotlib"
    Args:
        labels:

    Returns:

    """
    # color_dict = BBOX_DRAW_COLORS
    base_colors = list(BASE_COLORS.values())
    color_dict = dict()
    for i, label in enumerate(labels):
        color_dict[str(label).lower()] = base_colors[i]
    return color_dict

######## Draw Functions ####################



def draw_permit_range(img,permit_range):
    height, width = img.shape[:2]
    edge_buff = 10

    xmin = permit_range['xmin']
    if xmin == 0:
        xmin += edge_buff
    ymin = permit_range['ymin']
    if ymin == 0:
        ymin += edge_buff

    xmax = permit_range['xmax']
    if xmax == width:
        xmax -= edge_buff
    ymax = permit_range['ymax']
    if ymax == height:
        ymax -= edge_buff


    box = [xmin,ymin,xmax,ymax]
    img = draw_bbox(img, box, color=(0, 0, 255))
    return img

def get_upright_rect_points(xmin, ymin, xmax, ymax):
    D = (xmin,ymin)
    C = (xmin, ymax)
    A = (xmax, ymin)
    B = (xmax, ymax)
    box = np.array((A, B, C, D))
    return box



def visualize_calib_on_image(img, detections, retDetections,labels=None):
    if labels is not None:
        colors = create_color_dict(labels)
    else:
        labels=colors = [None]*len(detections)

    for i in range(len(detections)):
        label = labels[i]
        color = colors[label] if label is not None else (0,0,255)
        det = detections[i]
        ob = retDetections[i]
        img = draw_calibration_results(img, det, ob,label,color)
    return img


def draw_calibration_results(img, orig_detection, calibrated_detection, tag, color=(0,0,255)):
    img = draw_point(img, orig_detection,
                     color=color,
                     label=tag,
                     fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, thickness=10,
                     debug_mode=False)

    # x, y = orig_detection
    # y += 120
    # img = cv2.putText(img, f"Calibrated :{str(calibrated_detection)}", (int(x), int(y)), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
    #                   fontScale=2, color=color, thickness=10)




    # x,y = orig_detection
    # y-=50
    #
    # img = draw_point(img, (x,y),
    #                  color=(0, 0, 255),
    #                  label=str(calibrated_detection),
    #                  fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, thickness=10,
    #                  debug_mode=False)
    return img




def draw_items_from_list(img,item_list,item_type='bbox', labels=None, debug_mode=False, add_grid_lines=False,scores=None):
    if labels is not None:
        colors = create_color_dict(labels)
    else:
        labels = colors = [None]*len(item_list)

    if scores is None:
        scores = [None] * len(item_list)

    for i in range(len(item_list)):
        label = labels[i]
        if label is not None and label in colors:
            color = colors[label]
        else:
            color = (0, 0, 0)
        img = draw_item(img, item=item_list[i], item_type=item_type, color=color, label=label, debug_mode=debug_mode, score=scores[i])

    if add_grid_lines:
        img = add_axis_grid_lines(img)

    return img


def draw_item(img,item,item_type,color=None, label=None,fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, thickness=10,debug_mode=False,score=None):
    if item_type =='bbox':
        img = draw_bbox(img, item, color, label, fontFace, fontScale, thickness, debug_mode=debug_mode, score=score)
    elif item_type =='3point':
        img = draw_3_points(img,item,label)
    return img

def draw_3_points(img,points_list,colors=None,labels=None,debug_mode=False):
    if labels is None:
        labels = [None] *len(points_list)

    if colors is None:
        # colors = [None] * len(points_list)
        colors = [(0,0,255)] * len(points_list)

    for i in range(len(points_list)):
        img = draw_point(img,points_list[i],colors[i],labels[i],debug_mode=debug_mode,thickness=30)

    return img


def draw_point(img,point,color,label,fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, thickness=10,debug_mode=False):

    point = tuple((int(point[0]),int(point[1])))
    if color is None:
        color = (0,0,0) # black


    if label is not None:
        # set gep where to put the text above box
        x, y = point
        y-= 50
        img = cv2.putText(img, label, (x,y),fontFace,
                          fontScale=fontScale, color=color, thickness=thickness)

    if debug_mode:
        #draw point coordinates bellow point
        x, y = point
        y+= 50
        img = cv2.putText(img, str(point), (x, y), fontFace,
                          fontScale=fontScale, color=color, thickness=thickness)

    img = cv2.circle(img, (point[0],point[1]), radius=0, color=color, thickness=thickness)
    return img


def draw_bbox(img, box, color=None, label=None, fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=2, thickness=10, debug_mode=False,score=None):
    box = np.array(box, dtype=np.float)
    box = box.astype(np.int)
    rect = get_upright_rect_points(*box)
    return draw_rect(img, rect, color, label, fontFace, fontScale, thickness, debug_mode, score=score)

def draw_rect(img, rect, color=None, label=None, fontFace=cv2.FONT_HERSHEY_SIMPLEX,
              fontScale=2, thickness=10, debug_mode=False, score=None):
    rect = np.array(rect, dtype=int)
    if color is None:
        color = (0, 0, 0) # black

    if debug_mode:

        #draw box mid point
        midx = int(sum([p[0] for p in rect]) / len(rect))
        midy = int(sum([p[1] for p in rect]) / len(rect))
        img = cv2.circle(img, (midx, midy), radius=0, color=color, thickness=thickness*3)

        #draw point coordinates bellow box
        minx = int(min([p[0] for p in rect]))
        maxy = int(max([p[1] for p in rect]))
        img = cv2.putText(img, str((midx,midy)), (minx, maxy),fontFace,
                          fontScale=fontScale, color=color, thickness=thickness)

    if label is not None:
        if type(label) != str:
            if type(label) == float or type(label) == np.float64:
                label = np.around(label,2)
            label = str(label)
        minx = int(min([p[0] for p in rect]))
        miny = int(min([p[1] for p in rect]))  # set gep where to put the text above box

        if miny-50 <= 0:
            miny = int(max([p[1] for p in rect]))+50
        if score:
            label += " " + str(np.around(score,2))
        img = cv2.putText(img, label, (minx,miny),fontFace,
                          fontScale=fontScale, color=color, thickness=thickness)


    img = cv2.drawContours(img, [rect], 0, color, thickness=thickness)
    return  img


#def draw_bboxes_on_single_image_from_row(group, img, tag_font_size=20):

 #   # Check if using normelized coordinates
 #   if 0 < group.iloc[0]['X_MIN'] < 1 and 0 < group.iloc[0]['X_MAX'] < 1:
#        height, width = img.shape[:2]
#        group['X_MIN'] *= width
 #       group['X_MAX'] *= width
#
 #       group['Y_MIN'] *= height
 #       group['Y_MAX'] *= height


 #   for _, row in group.iterrows():
#        color = BBOX_DRAW_COLORS[row['TAG'].lower()]
#        box = upright_bbox_to_4_points(row['X_MIN'], row['Y_MIN'], row['X_MAX'], row['Y_MAX'])
#        img = draw_rect(img, box, label=row['TAG'], color=color)

 #   return img



def draw_mask_on_image(orig_img,mask):
    pass

def add_axis_grid_lines(img,x_size=100,y_size=100,x_margin=100,y_margin=50):
    """
    Open image in matplotlib to draw the axis tics and gridlines, saves the image to path
    Args:
        img:image as numpy array
        x_size: the step size for drawing x gridlines
        y_size: the step size for drawing y gridlines
        x_margin: added margin size in the x axis
        y_margin: added margin size in the y axis

    Returns: a numpy image with drawn axis and gridlines

    """
    img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

    dpi = mpl.rcParams['figure.dpi']
    height, width, depth = img.shape

    figsize = ((width + 2 * x_margin) / dpi, (height + 2 * y_margin) / dpi)  # inches
    left = x_margin / dpi / figsize[0]  # axes ratio
    bottom = y_margin / dpi / figsize[1]

    fig = plt.figure(figsize=figsize, dpi=dpi)
    fig.subplots_adjust(left=left, bottom=bottom, right=1. - left, top=1. - bottom)

    x_ticks = np.arange(0,width , x_size)
    y_ticks = np.arange(0, height, y_size)

    plt.xticks(x_ticks,fontsize=30)
    plt.yticks(y_ticks,fontsize=30)
    plt.grid(which='both', color='red', linewidth=5, linestyle='-', alpha=0.2)

    fig.autofmt_xdate()

    # Display the image.
    plt.imshow(img)
    plt.tight_layout() # remove empty margins

    # call the renderer
    canvas = FigureCanvasAgg(fig)
    canvas.draw()

    buf = canvas.buffer_rgba()
    # convert to a NumPy array
    img_rgba = np.asarray(buf)
    #convert back to bgr for opencv
    ret_img = cv2.cvtColor(img_rgba,cv2.COLOR_RGBA2BGR)
    plt.close('all')
    return ret_img



######Image manipulation functions #############


def convert_image_to_format(src_path, dest_path, res_format ='jpg'):
    """
    Scan path for images and convert to a selected format, e.g to jpg, png e.t.c
    Args:
        src_path: source folder with the images to convert
        dest_path: destination folder to save the converted images
        res_format: conversion format

    Returns:

    """
    # dest_path = create_path(dest_path,res_format)
    for im_name, im in tqdm(from_path_imgs_generator(src_path)):
        im_jpg_name = im_name[:-3] + res_format
        curr_dest_path = os.path.join(dest_path, im_jpg_name)
        cv2.imwrite(curr_dest_path, im)

def resize_pics(src_path,dest_path,x,y,format='jpg'):
    """

    Args:
        src_path:
        dest_path:
        x:
        y:
        format:

    Returns:

    """
    for im_path in tqdm(glob.glob(os.path.join(src_path,f"*.{format}"))):
        im_name = os.path.basename(im_path)
        im = cv2.imread(im_path)
        dest_im = cv2.resize(im,(x,y),interpolation=cv2.INTER_AREA)
        curr_dest = os.path.join(dest_path,im_name)
        cv2.imwrite(curr_dest,dest_im)



def is_bbox_contained(bb1,bb2):
    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        res = 0.0
    else:
        # The intersection of two axis-aligned bounding boxes is always an
        # axis-aligned bounding box
        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        # compute the area of bb1
        bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
        res = intersection_area / float(bb1_area)

    assert res >= 0.0
    assert res <= 1.0
    return res

def get_iou(bb1,bb2):
    """
    Calculate the Intersection over Union (IoU) of two bounding boxes.

    Parameters
    ----------

    Returns
    -------
    float
        in [0, 1]
    """


    # determine the coordinates of the intersection rectangle
    x_left = max(bb1[0], bb2[0])
    y_top = max(bb1[1], bb2[1])
    x_right = min(bb1[2], bb2[2])
    y_bottom = min(bb1[3], bb2[3])

    if x_right < x_left or y_bottom < y_top:
        iou = 0.0
    else:
        # The intersection of two axis-aligned bounding boxes is always an
        # axis-aligned bounding box
        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        # compute the area of both AABBs
        bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
        bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = intersection_area / float(
            bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou

def main():

    #### convert format
    src_path = r'D:\Data\ponka_set2\SRC'
    dest_path =r'D:\Data\ponka_set2\JPG'
    hash_key = r"C:\Work\Senecio\Code\python\projects\development\keyframe_image\hash_keyframe.csv"
    hash_key = None
    # convert_image_to_format(src_path,dest_path,'jpg')
    #convert_images_to_format_new_dest_folder(src_path,dest_path,hash_key=hash_key)



if __name__ == '__main__':
    main()