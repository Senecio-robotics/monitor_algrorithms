from file_utils import load_pickle_file
from numpy_DataStaticClass import get_all_mid_dicts_numpy, get_all_freq_mid_dicts_numpy
from numpy_table_utils import is_cost_valid, norm_cost, norm_freq_cost, get_merged_cost
from numpy_table_utils import complete_merged_cost, numpy_knn_match_cost_counter, numpy_freq_weight_counter
from numpy_table_utils import numpy_most_topn_from_all
from misc_utils import flatten
from numpy_embed_utils import numpy_batch_img_feature_tags_scores
from numpy_embed_utils import numpy_batch_img_features
import json
import numpy as np
import pandas as pd



# np_epsf = np.finfo(float).eps
# np_epsf32 = np.finfo(np.float32).eps

def numpy_get_unique_vals(cats_vec, prob_vec, scal_vec):
    if len(np.unique(cats_vec)) == len(cats_vec):
        if not isinstance(cats_vec, np.ndarray):
            cats_vec = np.array(cats_vec, dtype=np.uint8)
            prob_vec = np.array(prob_vec, dtype=np.float32)
            scal_vec = np.array(scal_vec, dtype=np.float32)
        return cats_vec, prob_vec, scal_vec
    else:
        final_cats_vec = np.full(len(cats_vec), 0, dtype=np.uint8)
        final_prob_vec = np.full(len(prob_vec), 0.0, dtype=np.float32)
        final_sc_vec = np.full(len(scal_vec), np.ndarray([]), dtype=np.ndarray)
        print("warn get_unique_vals: len(np.unique(cats_vec)) != len(cats_vec) ", cats_vec)
        # len(scal_vec) = 2 binary,  len(cats_vec) = len(prob_vec) = 1
        for index, clab in enumerate(cats_vec):
            if clab not in final_cats_vec:
                final_prob_vec[index] = prob_vec[index]
                final_sc_vec[index] = np.array(scal_vec[index], dtype=np.float32)
                final_cats_vec[index] = clab
        #final_prob_vec = np.array(final_prob_vec, dtype=np.float32)
        #final_sc_vec = np.array(final_sc_vec, dtype=np.float32)
        #final_cats_vec = np.array(final_cats_vec, dtype=np.uint8)
        return final_cats_vec, final_prob_vec, final_sc_vec


def numpy_predict_multiclass_lgbm_all(LGBM_Model, feature_mat, img_pieces_vec, nclass, ignore_warn=True):
    prob_vec = np.full(len(img_pieces_vec), 0.0, dtype=np.float32)
    category_vec = np.full(len(img_pieces_vec), 0, dtype=np.uint8)
    sc_mat = np.full(len(img_pieces_vec), np.ndarray([]), dtype=np.ndarray)
    feature_pieces = np.full(len(img_pieces_vec), np.ndarray([]), dtype=np.ndarray)
    try:
        x_mat = np.array(feature_mat)
        x_test = pd.DataFrame(x_mat)
        y_predicted_mat = LGBM_Model.predict(x_test)
        startat = 0
        for index, pieces in enumerate(img_pieces_vec):
            category = [0]
            probabilitiy = [0.0]
            # sc_out_vec = [0] * nclass
            endat = startat + pieces
            cinds = [startat, endat]
            feature_pieces[index] = np.array(cinds, dtype=np.uint16)
            cur_x_mat = x_mat[startat:endat]
            dims = np.shape(cur_x_mat)
            csaf = max(int(0.5 * dims[0]), 1)  # +0.5
            y_predicted = y_predicted_mat[startat:endat]
            # pred_lab_vec, prob_embed_vec, sc_vec = three_models_features_detection(image_feature_mat)
            scores_vec = y_predicted[0]
            sc_out_vec = np.full(len(scores_vec), 0.0, dtype=np.float32)
            if nclass != len(scores_vec) and not ignore_warn:
                if len(scores_vec) > nclass:
                    print('Warning: predict_multiclass_lgbm_all: use transform!', index, nclass, len(scores_vec))

            sc_sz = len(y_predicted)
            count_above = np.full(len(scores_vec), 0, dtype=np.uint8) #[0] * len(sc_out_vec)
            for score_vec in y_predicted:
                for index, cval in enumerate(score_vec):
                    sc_out_vec[index] += cval
                max_val = max(score_vec)
                for index in range(len(score_vec)):
                    if score_vec[index] == max_val:
                        count_above[index] += 1
            sc_out_vec = sc_out_vec/sc_sz
            max_count = max(count_above)
            # max_index = count_above.index(max_count) if count_above >= csaf:
            for ii in range(len(count_above)):
                if count_above[ii] == max_count:
                    if count_above[ii] >= csaf:
                        if ii not in category:
                            category[0] = ii
                            probabilitiy[0] = sc_out_vec[ii]
                        else:
                            if probabilitiy[ii] < sc_out_vec[ii]:
                                probabilitiy[ii] = sc_out_vec[ii]

            if len(category) == 0:
                max_prob = max(sc_out_vec)
                sc_max_prob_ind = sc_out_vec.argmax()
                category[0] = sc_max_prob_ind
                probabilitiy[0] = max_prob

            prob_vec[index] = probabilitiy[0]
            category_vec[index] = category[0]
            sc_mat[index] = np.array(sc_out_vec, dtype=np.float32)
            startat = endat
    except Exception as inst:
        # category = [0]
        # probabilitiy = [0.0]
        print("Fail predict_multiclass_lgbm_all: had exception on : ", inst)
        pass
    return category_vec, prob_vec, sc_mat, feature_pieces



def numpy_predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass, ignore_warn=True):

    sc_out_vec = np.full(nclass, 0.0, dtype=np.float32)
    category = np.full(1, 0, dtype=np.uint8)
    probabilitiy = np.full(1, 0.0, dtype=np.float32)
    x_mat = np.array(feature_mat)
    dims = np.shape(x_mat)
    csaf = max(int(0.5 * dims[0]), 1)  # +0.5
    try:
        x_test = pd.DataFrame(x_mat)
        y_predicted = LGBM_Model.predict(x_test)
        scores_vec = y_predicted[0]
        sc_out_vec = np.full(len(scores_vec), 0.0, dtype=np.float32)
        if nclass != len(scores_vec) and not ignore_warn:
            if len(scores_vec) > nclass:
                print('Warning: predict_nudity_multiclass3_lgbm: use transform!', nclass, len(scores_vec))

        sc_sz = len(y_predicted)
        count_above = np.full(len(sc_out_vec), 0, dtype=np.uint8)
        for score_vec in y_predicted:
            for index, cval in enumerate(score_vec):
                sc_out_vec[index] += cval
            max_val = max(score_vec)
            for index in range(len(score_vec)):
                if score_vec[index] == max_val:
                    count_above[index] += 1

        sc_out_vec = sc_out_vec/sc_sz
        max_count = max(count_above)
        # max_index = count_above.index(max_count) if count_above >= csaf:
        tot_count = 0
        for ii in range(len(count_above)):
            if count_above[ii] == max_count:
                if count_above[ii] >= csaf:
                    if ii not in category:
                        tot_count += 1
                        if tot_count < 2:
                            category[0] = ii
                            probabilitiy[0] = sc_out_vec[ii]
                        else:
                            print("warn numpy_predict_multiclass_lgbm: tot_count > 1 pick first! ", tot_count,
                                  category, ii, probabilitiy, sc_out_vec[ii])
                    else:
                        if probabilitiy[ii] < sc_out_vec[ii]:
                            probabilitiy[ii] = sc_out_vec[ii]

        if len(category) == 0:
            max_prob = max(sc_out_vec)
            sc_max_prob_ind = sc_out_vec.argmax()
            category[0] = sc_max_prob_ind
            probabilitiy[0] = max_prob

    except Exception as inst:
        print("Fail predict_multiclass_lgbm: had exception on : ", inst)
        pass
    return category, probabilitiy, sc_out_vec



def numpy_valid3to2cats_find_by_features(LGBM_Model, feature_mat, nclass, eps_saf=1.0e-08):
    cats_out_vec = np.full(1, 0, dtype=np.uint8)
    probs_out_vec = np.full(1, 0.0, dtype=np.float32)
    category_vec, prob_vec, sc_vec = numpy_predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    if not isinstance(sc_vec, np.ndarray):
        sc_vec = np.array(sc_vec, dtype=np.float32)
    # category_vec = adjust_tbl_embed(category_vec)
    max_prob = max(prob_vec)
    for index, cprob in enumerate(prob_vec):
        cat_val = category_vec[index]
        if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
            cats_out_vec[index] = cat_val
            probs_out_vec[index] = cprob
    return cats_out_vec, probs_out_vec, sc_vec




def numpy_valid3to2cats_find_tags_by_features(LGBM_Model, feature_mat, total_cost_vec, ids_cats_vec, nclass,
                                              embed_only=False, use_all=False, detect_CP=True, detect_CT=False,
                                              detect_UN=True, eps_saf=1.0e-08):

    cats_out_vec = np.full(1, 0, dtype=np.uint8)
    probs_out_vec = np.full(1, 0.0, dtype=np.float32)
    n_cost = nclass * 3
    category_vec, prob_vec, sc_vec = numpy_predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    # category_vec = adjust_tbl_embed(category_vec)
    sc_vec = np.array(sc_vec, dtype=np.float32)
    if embed_only or len(total_cost_vec) == 0:
        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob
    else:
        do_return = False
        #tot_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii]
                if max_cost0 < ctot:
                    max_cost0 = ctot
                #tot_vec.append(ctot)

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost0 < ctot:
                    max_cost0 = ctot
                #tot_vec.append(ctot)

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(ctot - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        #pos_vec = list()
#        inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cpos - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        #neg_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii]
                #neg_vec.append(cneg)
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cneg - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = np.where(category_vec == cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec, sc_vec

        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob

    return cats_out_vec, probs_out_vec, sc_vec


def numpy_all_find_tags_by_features(category_vec, prob_vec, total_cost_vec, ids_cats_vec, nclass,
                              embed_only=False, use_all=False, detect_CP=True, detect_CT=False, detect_UN=True,
                              eps_saf=1.0e-08):

    cats_out_vec = np.full(1, 0, dtype=np.uint8)
    probs_out_vec = np.full(1, 0.0, dtype=np.float32)
    n_cost = nclass * 3
    #category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(LGBM_Model, feature_mat, nclass)
    # category_vec = adjust_tbl_embed(category_vec)

    if embed_only or len(total_cost_vec) == 0:
        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob
    else:
        #tot_vec = list()
        #inds_vec = list()
        do_return = False
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            max_cost0 = -10000000.0
            kk = 0
            for ii in range(0, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost0 < ctot:
                    max_cost0 = ctot

            #max_cost0 = max(tot_vec)
            for index, ii in enumerate(inds_vec):
                ctot = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(ctot - max_cost0) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        #pos_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost1 = -10000000.0
            for ii in range(1, clen, 6):
                inds_vec[kk] = ii
                kk += 1
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost1 < cpos:
                    max_cost1 = cpos

            #max_cost1 = max(pos_vec)
            for index, ii in enumerate(inds_vec):
                cpos = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cpos - max_cost1) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        #neg_vec = list()
        #inds_vec = list()
        if len(total_cost_vec) == n_cost:
            length = (len(total_cost_vec) - 1) // 3 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec), 3):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii]
                #neg_vec.append(cneg)
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                is_max = abs(total_cost_vec[ii] - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        else:  # n_cost*2, n_cost*3
            len_max = n_cost * 2
            clen = len(total_cost_vec[:len_max])
            length = (clen - 1) // 6 + 1
            inds_vec = np.full(length, 0, dtype=np.uint16)
            kk = 0
            max_cost2 = -10000000.0
            for ii in range(2, len(total_cost_vec[:len_max]), 6):
                inds_vec[kk] = ii
                kk += 1
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                if max_cost2 < cneg:
                    max_cost2 = cneg

            #max_cost2 = max(neg_vec)
            for index, ii in enumerate(inds_vec):
                cneg = total_cost_vec[ii] + total_cost_vec[ii + 3]
                is_max = abs(cneg - max_cost2) < eps_saf
                findex = index
                skip_UN = True
                if not use_all:
                    if not detect_UN:  # case: CP vs. CT
                        if index > 0:
                            findex = index - 1
                        else:
                            skip_UN = False
                    elif detect_CP:
                        if detect_CT:  # case : Un vs CPT
                            if index > 0:
                                findex = 1
                        else:  # case UN vs CP
                            if index > 1:
                                findex = 0
                    elif detect_CT:  # case UN vs CT
                        if index == 1:
                            findex = 0
                cat_val = ids_cats_vec[findex]
                if skip_UN and is_max and cat_val in category_vec:
                    cii = category_vec.index(cat_val)
                    cprob = max(prob_vec)
                    if abs(cprob - prob_vec[cii]) < eps_saf and cat_val not in cats_out_vec:
                        cats_out_vec[0] = cat_val
                        probs_out_vec[0] = prob_vec[cii]
                        do_return = True

        if do_return:
            return cats_out_vec, probs_out_vec

        max_prob = max(prob_vec)
        for index, cprob in enumerate(prob_vec):
            cat_val = category_vec[index]
            if abs(cprob - max_prob) < eps_saf and cat_val not in cats_out_vec:
                cats_out_vec[0] = cat_val
                probs_out_vec[0] = cprob

    return cats_out_vec, probs_out_vec


def numpy_estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1, cats_out_vec2, prob_out_vec2, sc_vec2):

    cats_out_vec = np.full(len(cats_out_vec1), 0, dtype=np.uint8)
    prob_out_vec = np.full(len(cats_out_vec1), 0.0, dtype=np.float32)
    sc_vec = []
    is_succeed = True
    def_cat = 0
    for index, cur_cat in enumerate(cats_out_vec1):
        cur_prob = prob_out_vec1[index]
        cur_sc = sc_vec1[index]
        cur_cat2 = cats_out_vec2[index]
        cur_sc2 = sc_vec2[cur_cat2]
        ave_sc = 0.5 * (cur_sc + cur_sc2)
        sc_vec += [ave_sc]
        if cur_cat == 1:  # C.P. in 1
            if cur_cat in cats_out_vec2:  # C.T. in 2
                is_succeed = False
            elif def_cat in cats_out_vec2: # UN in 2
                if cur_cat not in cats_out_vec:
                    cats_out_vec[index] = cur_cat
                    prob_out_vec[index] = cur_prob
                elif prob_out_vec[index] < cur_prob:
                        prob_out_vec[index] = cur_prob
        else:  # cur_cat == 0
            cur_cat2 = cats_out_vec2[index]
            cur_prob2 = prob_out_vec2[index]
            if cur_cat2 == 1:  # C.T.
                if cur_cat2 not in cats_out_vec:
                    cats_out_vec[index] = cur_cat2 + 1
                    prob_out_vec[index] = cur_prob2
                elif prob_out_vec[index] < cur_prob2:
                    prob_out_vec[index] = cur_prob2
            else:  # UN
                ave_prob = 0.5 * (cur_prob + cur_prob2)
                if cur_cat2 not in cats_out_vec:
                    cats_out_vec[index] = cur_cat2
                    prob_out_vec[index] = ave_prob
                elif prob_out_vec[index] < ave_prob:
                        prob_out_vec[index] = ave_prob
    if not is_succeed:
        cats_out_vec = np.concatenate((cats_out_vec1, cats_out_vec2))
        prob_out_vec = np.concatenate((prob_out_vec1, prob_out_vec2))
    return is_succeed, cats_out_vec, prob_out_vec, sc_vec


#### main static class ##############################
# class detect_UN_CP_CT:
use_numpy = False
embed_only = False
debug_mode = False
use_all = True
detect_CP = True
detect_CT = False
detect_UN = True
use_features = False
use_generic_models = False
data_path = "D:/GW/senecio/data/CaliforniaYoloCounty/"
if use_generic_models:
    tables_dir = data_path + "table/tbl_CP_CT_UN/union_all/"
    tbl_name = 'table_spec_califIsr3CP_CT_UN_3x4253_2021-08-18_top_20.pickle'
    tbl_freq_name = 'table_freq_spec_califIsr3CP_CT_UN_3x4253_2021-08-18_top_20.json'
    models_dir = data_path + "models/"
    lgbm_model_name1 = 'califIsr2cats_CP_UN_embed_multiclass_166096x2048_2_30000_s9.87656e-05_lgbm_95_2021-08-22.txt'
    lgbm_model_name2 = 'califIsr2cats_CT_UN_embed_multiclass_166096x2048_30000_s9.80191e-05_lgbm_95_2021-08-22.txt'
    lgbm_model_name3 = 'califIsr2cats_CP_CT_embed_multiclass_73715x2048_2_30000_s1.79523e-08_lgbm_95_2021-08-18.txt'
else:
    use_bug_models = True
    if use_bug_models:
        tables_dir = data_path + "table/tbl_CP_CT_UN/add_tbl/union_all/"
        tbl_name = 'table_spec_califIsr3CP_CT_UN_3x11706_2021-09-11_top_20.pickle'
        tbl_freq_name = 'table_freq_spec_califIsr3CP_CT_UN_3x11706_2021-09-11_top_20.json'
        models_dir = data_path + "models/final_with_aug/bug_with20wrong/"
        lgbm_model_name1 = 'AllwithAug_CP_UN_embed_multiclass_1103674x2048_2_30000_s0.000315338_lgbm_95_2021-09-08.txt'
        lgbm_model_name2 = 'AllwithAug_CP_CT_embed_multiclass_621486x2048_2_30000_s0.000194002_lgbm_95_2021-09-09.txt'
        lgbm_model_name3 = 'califIsr2cats_CP_CT_embed_multiclass_73715x2048_2_30000_s1.79523e-08_lgbm_95_2021-08-18.txt'
    else:
        print("todo....")

valid_tags = ['unknown', 'Culex pipiens', 'Culex tarsalis']
nclass = len(valid_tags)
print("detect_UN_CP_CT_embed support valid_tags = ", valid_tags)
table_dct = []
table_freq_dct = []
ids_cats_vec = []
sorter_cat_lab = []
sorter_lab_cat = []
table_dct_file = tables_dir + tbl_name
try:
    print('\n  Load : table_dct_file dict tables ')
    table_dct = load_pickle_file(table_dct_file)
    print('fullimg table_dct_file = ', table_dct_file, 'len(table_dct) = ', len(table_dct))
except:
    print('failed: table_dct not loaded ', table_dct_file)

if len(table_dct) != len(valid_tags):
    print("error len(table_dct) != len(valid_tags) ", len(table_dct), len(valid_tags))

print("debug: detect_UN_CP_CT_embed support valid_tags = ", valid_tags, len(table_dct))
tbl_tags_vec = [ckey for ckey in table_dct if ckey in valid_tags]
print("debug: detect_UN_CP_CT_embed support tbl_tags_vec = ", tbl_tags_vec)
if valid_tags != tbl_tags_vec:
    reorder_tbl_dct = dict()
    for ctag in valid_tags:
        if ctag in table_dct:
            cval_vec = table_dct[ctag]
            reorder_tbl_dct[ctag] = cval_vec
        else:
            print("error ctag not in table_dct ", ctag)
    table_dct = reorder_tbl_dct.copy()
    tbl_tags_vec = [ckey for ckey in table_dct if ckey in valid_tags]
    print("table warn: reorder, tbl_tags_vec = ", tbl_tags_vec)
else:
    print("table final: tbl_tags_vec = ", tbl_tags_vec)

for ckey, cval in table_dct.items():
    print('table: ckey, len(cval) = ', ckey, len(cval))

# if table_freq_dct:

table_freq_dct_file = tables_dir + tbl_freq_name
try:
    print('\n  Load : table_freq_dct_file ')
    with open(table_freq_dct_file, 'r') as filehandle:
        table_freq_dct = json.load(filehandle)
    print('fullimg table_freq_dct_file = ', table_freq_dct_file, 'len(table_freq_dct) = ', len(table_freq_dct))
except:
    print('failed: table_freq_dct_file not loaded ', table_freq_dct_file)

if len(table_freq_dct) != len(valid_tags):
    print("error len(table_freq_dct) != len(valid_tags) ", len(table_freq_dct), len(valid_tags))

tbl_freq_tags_vec = [ckey for ckey in table_freq_dct if ckey in valid_tags]
if valid_tags != tbl_freq_tags_vec:
    reorder_freq_tbl_dct = dict()
    for ctag in valid_tags:
        if ctag in table_freq_dct:
            cval_vec = table_freq_dct[ctag]
            reorder_freq_tbl_dct[ctag] = cval_vec
        else:
            print("error ctag not in table_freq_dct ", ctag)
    table_freq_dct = reorder_freq_tbl_dct.copy()
    tbl_freq_tags_vec = [ckey for ckey in table_freq_dct if ckey in valid_tags]
    print("freq table warn: reorder, tbl_freq_tags_vec = ", tbl_freq_tags_vec)
else:
    print("freq table final: tbl_tags_vec = ", tbl_freq_tags_vec)

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name1
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model1 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model1 not loaded ', lgbm_model_name1)
    LGBM_Model1 = None

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name2
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model2 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model2 not loaded ', lgbm_model_name2)
    LGBM_Model2 = None

try:
    import lightgbm as lgb

    fullfile_lgbm = models_dir + lgbm_model_name3
    print("LGBM model.load started: " + fullfile_lgbm)
    LGBM_Model3 = lgb.Booster(model_file=fullfile_lgbm)
    print("LGBM model.load completed: " + fullfile_lgbm)
except:
    print('failed: LGBM_Model3 not loaded ', lgbm_model_name3)
    LGBM_Model3 = None

mid_vec = get_all_mid_dicts_numpy(table_dct)
print('\nmid_vec = ', len(mid_vec), mid_vec)
freq_mid_vec = get_all_freq_mid_dicts_numpy(table_freq_dct)
print('\nfreq_mid_vec = ', len(freq_mid_vec), freq_mid_vec)

if debug_mode:
    if not use_all:
        final_tags_vec = []
        if not detect_UN:
            for index, ctag in enumerate(valid_tags):
                if ctag == 'unknown':
                    print("test_validcCats_collect_embed_data: skip not detect_UN: ctag == 'unknown' => "
                          "index, catg, detect_UN = ", index, ctag, detect_UN)
                    continue
                else:
                    if ctag not in final_tags_vec:
                        final_tags_vec += [ctag]
        elif detect_CP:
            if detect_CT:
                for index, ctag in enumerate(valid_tags):
                    if ctag == 'Culex tarsalis' or ctag == 'Culex pipiens':
                        merge_tag = 'Culex pipiens_tarsalis'
                        print(
                            "test_validcCats_collect_embed_data: ctag == 'Culex tarsalis' or ctag == 'Culex pipiens' => "
                            "catg, detect_CP = ", index, merge_tag, detect_CP, detect_CT)
                        if merge_tag not in final_tags_vec:
                            final_tags_vec += [merge_tag]
                    else:
                        if ctag not in final_tags_vec:
                            final_tags_vec += [ctag]

            else:
                for index, ctag in enumerate(valid_tags):
                    if ctag == 'Culex tarsalis':
                        print(
                            "test_validcCats_collect_embed_data: skip detect_CP: ctag == 'Culex tarsalis' => catg, "
                            "detect_CP = ", index, ctag, detect_CP)
                        continue
                    else:
                        if ctag not in final_tags_vec:
                            final_tags_vec += [ctag]
        elif detect_CT:
            for index, ctag in enumerate(valid_tags):
                if ctag == 'Culex pipiens':
                    print("test_validcCats_collect_embed_data: skip detect_CP: ctag == 'Culex pipiens' => "
                          "catg, detect_CP = ", index, ctag, detect_CP)
                else:
                    if ctag not in final_tags_vec:
                        final_tags_vec += [ctag]
    else:
        final_tags_vec = valid_tags.copy()

    try:
        sorter_lab_cat = dict()
        sorter_cat_lab = dict()
        for ii, st in enumerate(final_tags_vec):
            # print(st)
            sorter_lab_cat[ii] = final_tags_vec[ii]
            sorter_cat_lab[final_tags_vec[ii]] = ii

        print('sorter_lab_cat: ', sorter_lab_cat)
        print('sorter_cat_lab: ', sorter_cat_lab)

        ids_cats_vec = [ii for ii in sorter_lab_cat.keys()]
        print('ids_cats_vec = ', len(ids_cats_vec), ids_cats_vec)
    except:
        print("error failed to calc sorter_cat_lab, sorter_lab_cat, ids_cats_vec")
else:
    ids_cats_vec = [ii for ii in range(len(valid_tags))]
    print('ids_cats_vec = ', len(ids_cats_vec), ids_cats_vec)


# return LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, final_tags_vec, table_freq_dct, \
#       mid_vec, freq_mid_vec, ids_cats_vec, sorter_cat_lab, sorter_lab_cat



# @staticmethod
def numpy_three_models_features_detection(feature_mat):
    global nclass
    global LGBM_Model1
    global LGBM_Model2
    global LGBM_Model3
    # nclass = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,

    cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_valid3to2cats_find_by_features(LGBM_Model1, feature_mat, nclass)

    cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_valid3to2cats_find_by_features(LGBM_Model2, feature_mat, nclass)

    cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_get_unique_vals(cats_out_vec1, prob_out_vec1, sc_vec1)

    cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_get_unique_vals(cats_out_vec2, prob_out_vec2, sc_vec2)

    # table truth
    is_succeed, cats_out_vec, prob_out_vec, sc_vec = numpy_estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1,
                                                                                cats_out_vec2, prob_out_vec2, sc_vec2)
    if not is_succeed:
        cats_out_vec12 = cats_out_vec.copy()
        prob_out_vec12 = prob_out_vec.copy()
        sc_vec12 = sc_vec.copy()
        cats_out_vec3, prob_out_vec3, sc_vec3 = numpy_valid3to2cats_find_by_features(LGBM_Model3, feature_mat, nclass)

        print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
              sc_vec12)
        print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
              sc_vec3)

        cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)

    return cats_out_vec, prob_out_vec, sc_vec




# @staticmethod
def numpy_predict_LGBM_by_features(np_img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    use_numpy = True
    feature_mat, img_pieces_vec, img_dims_vec = numpy_batch_img_features(np_img_vec, disp=disp)
    prob_vec = np.full(len(img_pieces_vec), 0.0, dtype=np.float32)
    category_vec = np.full(len(img_pieces_vec), 0.0, dtype=np.uint8)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        image_feature_mat = feature_mat[startat:endat]
        pred_lab_vec, prob_embed_vec, _ = numpy_three_models_features_detection(image_feature_mat)
        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]
        startat = endat

    return category_vec, prob_vec


# @staticmethod
def numpy_three_models_features_tags_detection(labels_topn_all, scores_topn_all, feature_mat, embed_only=False):
    global LGBM_Model1
    global LGBM_Model2
    global LGBM_Model3
    global table_dct
    global table_freq_dct
    global freq_mid_vec
    global mid_vec
    use_complete = True
    cats_out_vec = []
    prob_out_vec = []
    nclass = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    sc_vec = [0] * nclass
    # print('deb00: nclass ', nclass)
    labels_topn, scores_topn = numpy_most_topn_from_all(labels_topn_all, scores_topn_all)

    if len(labels_topn) == 0:
        print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
        return cats_out_vec, prob_out_vec, sc_vec

    # print('deb000: len(labels_topn) ', len(labels_topn))
    total_cost_vec = numpy_knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
    # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
    norm_cost_vec = norm_cost(total_cost_vec)
    # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
    freq_weight_vec = numpy_freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                          mid_vec)
    # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
    norm_freq_vec = norm_freq_cost(freq_weight_vec)
    ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
    # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
    if ok_2D:
        # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
        if use_complete:
            norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
        else:
            norm_all_vec = get_merged_cost(norm_cost_vec, norm_freq_vec)
    else:
        print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
        norm_all_vec = []
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,

    cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_valid3to2cats_find_tags_by_features(LGBM_Model1, feature_mat,
                                                                                      norm_all_vec,
                                                                                      ids_cats_vec, nclass,
                                                                                      embed_only=embed_only,
                                                                                      use_all=False, detect_CP=True,
                                                                                      detect_CT=False, detect_UN=True)

    cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_get_unique_vals(cats_out_vec1, prob_out_vec1, sc_vec1)

    cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_valid3to2cats_find_tags_by_features(LGBM_Model2, feature_mat,
                                                                                      norm_all_vec,
                                                                                      ids_cats_vec, nclass,
                                                                                      embed_only=embed_only,
                                                                                      use_all=False, detect_CP=False,
                                                                                      detect_CT=True, detect_UN=True)

    cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_get_unique_vals(cats_out_vec2, prob_out_vec2, sc_vec2)

    # table truth
    is_succeed, cats_out_vec, prob_out_vec, sc_vec = numpy_estimate_truth_table(cats_out_vec1, prob_out_vec1, sc_vec1,
                                                                                cats_out_vec2, prob_out_vec2, sc_vec2)
    if not is_succeed:
        cats_out_vec12 = cats_out_vec.copy()
        prob_out_vec12 = prob_out_vec.copy()
        sc_vec12 = sc_vec.copy()
        cats_out_vec3, prob_out_vec3, sc_vec3 = numpy_valid3to2cats_find_tags_by_features(LGBM_Model3, feature_mat,
                                                                                          norm_all_vec, ids_cats_vec,
                                                                                          nclass,
                                                                                          embed_only=embed_only,
                                                                                          use_all=False, detect_CP=True,
                                                                                          detect_CT=True,
                                                                                          detect_UN=False)

        print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
              sc_vec12)
        print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
              sc_vec3)

        cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)

    return cats_out_vec, prob_out_vec, sc_vec



# @staticmethod
def numpy_predict_LGBM_by_tags_features(img_vec, embed_only=False, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    use_numpy = True
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        numpy_batch_img_feature_tags_scores(img_vec, disp=disp)

    prob_vec = np.full(len(img_pieces_vec), 0.0, dtype=np.float32)
    category_vec = np.full(len(img_pieces_vec), 0, dtype=np.uint8)

    # feature_mat = batch_img_features(img_vec)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = labels_topns.flatten()
        scores_topn_all = scores_topns.flatten()
        pred_lab_vec, prob_embed_vec, sc_vec = numpy_three_models_features_tags_detection(labels_topn_all,
                                                                                          scores_topn_all,
                                                                                          image_feature_mat,
                                                                                          embed_only=embed_only)

        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]
        startat = endat

    return category_vec, prob_vec



# @staticmethod
def numpy_predict_LGBM_from_fullimg(img, bbx_vec, embed_only=False, disp=False):
    np_img_vec = np.full(len(bbx_vec), np.ndarray([]), dtype=np.ndarray)
    for index, bbx in enumerate(bbx_vec):
        cmin, rmin, cmax, rmax = bbx
        crop_img = img[rmin:rmax, cmin:cmax].copy()
        np_img_vec[index] = crop_img.copy()
    if embed_only:
        predictions_mat, score_mat = numpy_predict_LGBM_by_features(np_img_vec, disp=disp)
    else:
        predictions_mat, score_mat = numpy_predict_LGBM_by_tags_features(np_img_vec, disp=disp)
    return predictions_mat, score_mat


# all parallel

# @staticmethod
def numpy_all_three_models_features_detection(feature_mat, img_pieces_vec):
    global LGBM_Model1
    global LGBM_Model2
    global LGBM_Model3
    global table_dct
    global table_freq_dct
    global freq_mid_vec
    global mid_vec
    nclass = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,

    cats_out_mat1, prob_out_mat1, sc_mat1, feature_pieces1 = numpy_predict_multiclass_lgbm_all(LGBM_Model1, feature_mat,
                                                                                               img_pieces_vec, nclass)

    cats_out_mat2, prob_out_mat2, sc_mat2, feature_pieces2 = numpy_predict_multiclass_lgbm_all(LGBM_Model2, feature_mat,
                                                                                               img_pieces_vec, nclass)

    for index, cur_cat1 in enumerate(cats_out_mat1):
        cur_prob1 = prob_out_mat1[index]
        sc_vec1 = sc_mat1[index]
        cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_get_unique_vals([cur_cat1], [cur_prob1], sc_vec1)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()


    for index, cur_cat2 in enumerate(cats_out_mat2):
        cur_prob2 = prob_out_mat2[index]
        sc_vec2 = sc_mat2[index]
        cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_get_unique_vals([cur_cat2], [cur_prob2], sc_vec2)
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    # table truth
    cats_out_mat = np.full(len(cats_out_mat2), 0, dtype=np.uint8)
    prob_out_mat = np.full(len(cats_out_mat2), 0.0, dtype=np.float32)
    sc_mat = np.full(len(cats_out_mat2), np.ndarray([]), dtype=np.ndarray)
    for index, cur_cat2 in enumerate(cats_out_mat2):
        cur_prob2 = prob_out_mat2[index]
        sc_vec2 = sc_mat2[index]
        cur_cat1 = prob_out_mat1[index]
        cur_prob1 = prob_out_mat1[index]
        sc_vec1 = sc_mat1[index]
        is_succeed, cats_out_vec, prob_out_vec, sc_vec = numpy_estimate_truth_table([cur_cat1], [cur_prob1],
                                                                                    sc_vec1, [cur_cat2],
                                                                                    [cur_prob2], sc_vec2)
        if not is_succeed:

            cats_out_vec12 = cats_out_vec.copy()
            prob_out_vec12 = prob_out_vec.copy()
            sc_vec12 = sc_vec.copy()
            startat, endat = feature_pieces1[index]
            image_feature_mat = feature_mat[startat:endat]
            cats_out_vec3, prob_out_vec3, sc_vec3 = numpy_predict_multiclass_lgbm(LGBM_Model3, image_feature_mat,
                                                                                  nclass)

            print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ", cats_out_vec12, prob_out_vec12,
                  sc_vec12)
            print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ", cats_out_vec3, prob_out_vec3,
                  sc_vec3)

            cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = np.array(sc_vec, dtype=np.float32)

        else:
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = np.array(sc_vec, dtype=np.float32)

    return cats_out_mat, prob_out_mat, sc_mat



#@staticmethod
def numpy_all_three_models_tags_features_detection(feature_mat, labels_topn_mat, scores_topn_mat,
                                             img_pieces_vec, embed_only=False, disp=False):
    #LGBM_Model1 = detect_UN_CP_CT.LGBM_Model1
   # LGBM_Model2 = detect_UN_CP_CT.LGBM_Model2
   # table_dct = detect_UN_CP_CT.table_dct
   # table_freq_dct = detect_UN_CP_CT.table_freq_dct
   # freq_mid_vec = detect_UN_CP_CT.freq_mid_vec
   # mid_vec = detect_UN_CP_CT.mid_vec
  #  ids_cats_vec = detect_UN_CP_CT.ids_cats_vec
    nclass = min([len(table_dct), len(table_freq_dct), len(freq_mid_vec), len(mid_vec)])
    # category_vec, prob_vec, sc_vec = predict_multiclass_lgbm(feature_mat, nclass)
    # print('deb22: len(norm_all_vec) ', len(norm_all_vec))  use_all=False,


    cats_out_mat1, prob_out_mat1, sc_mat1, feature_pieces1 = numpy_predict_multiclass_lgbm_all(LGBM_Model1, feature_mat,
                                                                                               img_pieces_vec, nclass)

    cats_out_mat2, prob_out_mat2, sc_mat2, feature_pieces2 = numpy_predict_multiclass_lgbm_all(LGBM_Model2, feature_mat,
                                                                                               img_pieces_vec, nclass)

    for index, pieces in enumerate(img_pieces_vec):
        startat, endat = feature_pieces1[index]
        cats_out_vec1 = [cats_out_mat1[index]]
        prob_out_vec1 = [prob_out_mat1[index]]
        sc_vec1 = sc_mat1[index]
        cats_out_vec2 = [cats_out_mat2[index]]
        prob_out_vec2 = [prob_out_mat2[index]]
        sc_vec2 = sc_mat2[index]
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        #image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = labels_topns.flatten()
        scores_topn_all = scores_topns.flatten()
        labels_topn, scores_topn = numpy_most_topn_from_all(labels_topn_all, scores_topn_all)

        if len(labels_topn) == 0:
            print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
            cats_out_mat1[index] = cats_out_vec1[0]
            prob_out_mat1[index] = prob_out_vec1[0]
            sc_mat1[index] = sc_vec1.copy()
            cats_out_mat2[index] = cats_out_vec2[0]
            prob_out_mat2[index] = prob_out_vec2[0]
            sc_mat2[index] = sc_vec2.copy()
            continue

        # print('deb000: len(labels_topn) ', len(labels_topn))
        total_cost_vec = numpy_knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
        # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
        norm_cost_vec = norm_cost(total_cost_vec)
        # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
        freq_weight_vec = numpy_freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                              mid_vec)
        # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
        norm_freq_vec = norm_freq_cost(freq_weight_vec)
        ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
        # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
        if ok_2D:
            # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
            norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
        else:
            print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
            #norm_all_vec = []
            cats_out_mat1[index] = cats_out_vec1[0]
            prob_out_mat1[index] = prob_out_vec1[0]
            sc_mat1[index] = sc_vec1.copy()
            cats_out_mat2[index] = cats_out_vec2[0]
            prob_out_mat2[index] = prob_out_vec2[0]
            sc_mat2[index] = sc_vec2.copy()
            continue
        cats_out_vec1, prob_out_vec1 = numpy_all_find_tags_by_features(cats_out_vec1, prob_out_vec1, norm_all_vec,
                                                                                    ids_cats_vec, nclass,
                                                                                    embed_only=embed_only,
                                                                                    use_all=False, detect_CP=True,
                                                                                    detect_CT=False, detect_UN=True)

        cats_out_vec2, prob_out_vec2 = numpy_all_find_tags_by_features(cats_out_vec2, prob_out_vec2, norm_all_vec,
                                                                                    ids_cats_vec, nclass,
                                                                                    embed_only=embed_only,
                                                                                    use_all=False, detect_CP=False,
                                                                                    detect_CT=True, detect_UN=True)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    for index, ccat1 in enumerate(cats_out_mat1):
        sc_vec1 = sc_mat1[index]
        cats_out_vec1, prob_out_vec1, sc_vec1 = numpy_get_unique_vals([ccat1], [prob_out_mat1[index]], sc_vec1)
        cats_out_mat1[index] = cats_out_vec1[0]
        prob_out_mat1[index] = prob_out_vec1[0]
        sc_mat1[index] = sc_vec1.copy()


    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        cats_out_vec2, prob_out_vec2, sc_vec2 = numpy_get_unique_vals([ccat2], [prob_out_mat2[index]], sc_vec2)
        cats_out_mat2[index] = cats_out_vec2[0]
        prob_out_mat2[index] = prob_out_vec2[0]
        sc_mat2[index] = sc_vec2.copy()

    # table truth
    cats_out_mat = np.full(len(cats_out_mat2), 0, dtype=np.uint8)
    prob_out_mat = np.full(len(cats_out_mat2), 0.0, dtype=np.float32)
    sc_mat = np.full(len(cats_out_mat2), np.ndarray([]), dtype=np.ndarray)
    for index, ccat2 in enumerate(cats_out_mat2):
        sc_vec2 = sc_mat2[index]
        sc_vec1 = sc_mat1[index]
        is_succeed, cats_out_vec, prob_out_vec, sc_vec = numpy_estimate_truth_table([cats_out_mat1[index]],
                                                                              [prob_out_mat1[index]], sc_vec1,
                                                                              [ccat2], [prob_out_mat2[index]], sc_vec2)
        if not is_succeed:
           # LGBM_Model3 = detect_UN_CP_CT.LGBM_Model3
            startat, endat = feature_pieces1[index]
            image_feature_mat = feature_mat[startat:endat]
            cats_out_vec3, prob_out_vec3, sc_vec3 = numpy_predict_multiclass_lgbm(LGBM_Model3, image_feature_mat, nclass)
            labels_topns = labels_topn_mat[startat:endat]
            scores_topns = scores_topn_mat[startat:endat]
            # image_feature_mat = feature_mat[startat:endat]
            labels_topn_all = labels_topns.flatten()
            scores_topn_all = scores_topns.flatten()
            labels_topn, scores_topn = numpy_most_topn_from_all(labels_topn_all, scores_topn_all)
            if len(labels_topn) == 0:
                print("Error features_tags_detection: img -> labels_topn, scores_topn are empty ! ")
                cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
                cats_out_mat[index] = cats_out_vec[0]
                prob_out_mat[index] = prob_out_vec[0]
                sc_mat[index] = np.array(sc_vec.copy(), dtype=np.float32)
                continue

            # print('deb000: len(labels_topn) ', len(labels_topn))
            total_cost_vec = numpy_knn_match_cost_counter(labels_topn, scores_topn, table_dct, mid_vec)
            # print('deb01: len(total_cost_vec) ', len(total_cost_vec))
            norm_cost_vec = norm_cost(total_cost_vec)
            # print('deb02: len(norm_cost_vec) ', len(norm_cost_vec))
            freq_weight_vec = numpy_freq_weight_counter(labels_topn, scores_topn, table_dct, table_freq_dct, freq_mid_vec,
                                                  mid_vec)
            # print('deb02: len(freq_weight_vec) ', len(freq_weight_vec))
            norm_freq_vec = norm_freq_cost(freq_weight_vec)
            ok_2D = is_cost_valid(norm_cost_vec) and is_cost_valid(norm_freq_vec)
            # print('deb22: len(total_cost_vec) ', len(total_cost_vec))
            if ok_2D:
                # print('norm_cost_vec, norm_freq_vec ', norm_cost_vec, norm_freq_vec)
                norm_all_vec = complete_merged_cost(norm_cost_vec, norm_freq_vec)
            else:
                print('error: features_tags_detection  norm_cost_vec or norm_freq_vec !')
                # norm_all_vec = []
                cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
                cats_out_mat[index] = cats_out_vec[0]
                prob_out_mat[index] = prob_out_vec[0]
                sc_mat[index] = np.array(sc_vec.copy(), dtype=np.float32)
                continue

            cats_out_vec3, prob_out_vec3 = numpy_all_find_tags_by_features(cats_out_vec3, prob_out_vec3, norm_all_vec,
                                                                              ids_cats_vec, nclass,
                                                                              embed_only=embed_only,
                                                                              use_all=False, detect_CP=True,
                                                                              detect_CT=True, detect_UN=False)

            if disp:
                cats_out_vec12 = cats_out_vec.copy()
                prob_out_vec12 = prob_out_vec.copy()
                sc_vec12 = sc_vec.copy()
                print("debug disagrre pred: cats_out_vec12, prob_out_vec12, sc_vec12 = ",
                      cats_out_vec12, prob_out_vec12, sc_vec12)
                print("debug disagrre pred: cats_out_vec3, prob_out_vec3, sc_vec3 = ",
                      cats_out_vec3, prob_out_vec3, sc_vec3)

            cats_out_vec, prob_out_vec, sc_vec = numpy_get_unique_vals(cats_out_vec3, prob_out_vec3, sc_vec3)
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = np.array(sc_vec.copy(), dtype=np.float32)

        else:
            cats_out_mat[index] = cats_out_vec[0]
            prob_out_mat[index] = prob_out_vec[0]
            sc_mat[index] = np.array(sc_vec.copy(), dtype=np.float32)

    return cats_out_mat, prob_out_mat, sc_mat




# @staticmethod
def numpy_all_predict_LGBM_by_features(np_img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, img_pieces_vec, img_dims_vec = numpy_batch_img_features(np_img_vec, disp=disp)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))

    predictions_mat, score_mat, sc_mat = numpy_all_three_models_features_detection(feature_mat, img_pieces_vec)
    return predictions_mat, score_mat


#@staticmethod
def numpy_all_predict_LGBM_by_tags_features(img_vec, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        numpy_batch_img_feature_tags_scores(img_vec, disp=disp)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))

    predictions_mat, score_mat, sc_mat = numpy_all_three_models_tags_features_detection(feature_mat,
                                                            labels_topn_mat, scores_topn_mat, img_pieces_vec)
    return predictions_mat, score_mat



# @staticmethod
def numpy_all_predict_LGBM_from_fullimg(img, bbx_vec, embed_only=False, disp=False):
    use_numpy = True
    np_img_vec = np.full(len(bbx_vec), np.ndarray([]), dtype=np.ndarray)
    for index, bbx in enumerate(bbx_vec):
        cmin, rmin, cmax, rmax = bbx
        crop_img = img[rmin:rmax, cmin:cmax].copy()
        np_img_vec[index] = crop_img.copy()
    if embed_only:
        predictions_mat, score_mat = numpy_all_predict_LGBM_by_features(np_img_vec, disp=disp)
    else:
        predictions_mat, score_mat = numpy_all_predict_LGBM_by_tags_features(np_img_vec, disp=disp)
    return predictions_mat, score_mat



# @staticmethod
def parallel_numpy_predict_LGBM_by_tags_features(img_vec, embed_only=False, disp=False):
    # LGBM_Model1, LGBM_Model2, LGBM_Model3, table_dct, table_freq_dct, freq_mid_vec, mid_vec
    feature_mat, labels_topn_mat, scores_topn_mat, img_pieces_vec, img_dims_vec = \
        numpy_batch_img_feature_tags_scores(img_vec, disp=disp)
    prob_vec = np.full(len(img_pieces_vec), 0.0, dtype=np.float32)
    category_vec = np.full(len(img_pieces_vec), 0, dtype=np.uint8)
    if disp:
        print('len, img_pieces_vec = ', len(img_pieces_vec), sum(img_pieces_vec))
    startat = 0
    for index, pieces in enumerate(img_pieces_vec):
        endat = startat + pieces
        labels_topns = labels_topn_mat[startat:endat]
        scores_topns = scores_topn_mat[startat:endat]
        image_feature_mat = feature_mat[startat:endat]
        labels_topn_all = labels_topns.flatten()
        scores_topn_all = scores_topns.flatten()
        pred_lab_vec, prob_embed_vec, sc_vec = numpy_three_models_features_tags_detection(labels_topn_all,
                                                                                    scores_topn_all, image_feature_mat,
                                                                                    embed_only=embed_only)
        prob_vec[index] = prob_embed_vec[0]
        category_vec[index] = pred_lab_vec[0]

        startat = endat

    return category_vec, prob_vec


