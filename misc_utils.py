"""
misc_utils.py
new version  :03/11/20
Created on 03/11/20
misc_utils
@author: Gabriel Weizman
"""

# Dependencies

import numpy as np
#from functools import lru_cache

from image_utils import DEFAULT_IMAGE_SIZE, resize_min, rescale_img, get_resize_img_dims

# rules
from logging_utils import LOG

import cv2

#from augment_segmented_image import random_transform_segmented_image, transform_segmented_image


# =============================15-07-19 ======================================================================================= #

def fast_sorted_values(hash_dict):
    dict_sorted = dict()
    for clab , prob_vec in hash_dict.items():
        dict_sorted[clab] = sorted(prob_vec)
    return dict_sorted
#np_epsf = np.finfo(float32).eps
def merge_hash_dict(ref_dict, add_dict, eps_saf=np.finfo(np.float).eps):
    if not ref_dict:
        ref_dict = add_dict.copy()
    else:  # merge
        for add_key, add_val_vec in add_dict.items():
            if add_key in ref_dict.keys():
                ref_vec = ref_dict[add_key]
                for confidence in add_val_vec:
                    dif_vec = [abs(x - confidence) for x in ref_vec]
                    min_dif = min(dif_vec)
                    if min_dif > eps_saf:
                        ref_vec = ref_vec + [confidence]
                ref_dict[add_key] = ref_vec
            else:
                ref_dict[add_key] = add_val_vec

    ref_dict = fast_sorted_values(ref_dict)
    return ref_dict

def merge_freq_hash_dict(ref_dict, ref_sum_freq, add_dict, add_sum_freq):
    if not ref_dict:
        ref_dict = add_dict.copy()
    else:  # merge
        for add_key, add_freq in add_dict.items():
            new_add_freq = add_sum_freq * add_freq
            if add_key in ref_dict.keys():
                ref_freq = ref_dict[add_key]
                new_ref_freq = ref_sum_freq * ref_freq
                cur_sum_freq = new_add_freq + new_ref_freq
                ref_dict[add_key] = cur_sum_freq
            else:
                ref_dict[add_key] = new_add_freq
    sum_freq = max(sum([cfreq for cfreq in ref_dict.values()]), 1.0)
    for clab in ref_dict.keys():
        cfreq_norm = ref_dict[clab] / sum_freq
        ref_dict[clab] = cfreq_norm
    return ref_dict, sum_freq

def set_update_dct(input_dict, ckey, cvals, disp=False):
    if ckey not in input_dict:
        input_dict[ckey] = [cvals]
    else:
        values_vec = input_dict[ckey]
        if cvals not in values_vec:
            values_vec += [cvals]
            input_dict[ckey] = values_vec
    if disp:
        total_amount = 0
        for kk, vv in input_dict.items():
            total_amount += len(vv)
        print("set_update_dct: total_amount = ", total_amount)


def merge_dicts(in_dct1, in_dct2):
    out_dct = in_dct2.copy()
    for ccat, values_vec in in_dct1.items():
        if ccat not in out_dct:
            out_dct[ccat] = list(np.unique(values_vec))
        else:
            cur_vals = list(np.unique(out_dct[ccat]))
            for cval in values_vec:
                if cval not in cur_vals:
                    cur_vals += [cval]
            out_dct[ccat] = cur_vals
    return out_dct

def flatten(in_mat, show=False):
    if type(in_mat) is list:
        if len(in_mat) > 1:
            # print('flatten(l[0]) ', flatten0(l[0]))
            # print('flatten(l[1:]) ', flatten0(l[1:]))
            return flatten(in_mat[0]) + flatten(in_mat[1:])
        else:
            if len(in_mat) == 0:
                return in_mat
            else:
                # print('flatten(l[0]) ', flatten0(l[0]))
                return flatten(in_mat[0])
    elif type(in_mat) is tuple:
        if len(in_mat) > 1:
            if show:
                print('2 tup', in_mat)
            return list(flatten(in_mat[0])) + list(flatten(in_mat[1:]))
        else:
            if len(in_mat) == 0:
                return in_mat
            else:
                if show:
                    print('1 tup', in_mat)
                return list(flatten(in_mat[0]))  # + [")"]

    elif isinstance(in_mat, np.ndarray):
        return list(in_mat.flatten())
    else:
        return [in_mat]

def conc_feature_numpy_mats(mat_in, mat_out, ax=0):
    if mat_in.size == 0:
        return mat_out
    dims = np.shape(mat_in)
    if len(dims) > 1:
        if len(mat_out) > 0:
            mat_out = np.concatenate((mat_out, mat_in), axis=ax)
        else:
            mat_out = mat_in
    elif len(dims) == 1 and len(mat_in) > 0:
        if len(mat_out) > 0:
            mat_out = np.concatenate((mat_out, [mat_in]), axis=ax)
        else:
            mat_out = [mat_in]
    else:
        LOG.debug(f'warning: conc_numpy_mats: mat_out not changed... len(dims) == 0,  len(mat_in)=0: {dims}, '
                  f'{len(mat_in)}')
    return mat_out

def update_feature_mat(mat_in, mat_out):
    if isinstance(mat_in, np.ndarray):
        if not isinstance(mat_out, np.ndarray):
            mat_out = np.array(mat_out)
        return conc_feature_numpy_mats(mat_in, mat_out, ax=0)
    elif not isinstance(mat_in, list):
        mat_in = list(mat_in)

    if len(mat_in) == 0:
        return mat_out
    dims = np.shape(mat_in)
    if len(dims) > 1:
        if len(mat_in) > 0:
            if len(mat_out) > 0:
                mat_out += mat_in
            else:
                mat_out = mat_in
    elif len(dims) == 1 and len(mat_in) > 0:
        if len(mat_out) > 0:
            mat_out += [mat_in]
        else:
            mat_out = [mat_in]
    else:
        LOG.debug('warning: update_feature_mat, len(dims) == 0,  len(mat_in)=0: dims=[%s]; len(mat_in)=%s '
                  % (dims, len(mat_in)))
    return mat_out

def update_np_feature_mat(mat_in, mat_out):
    if isinstance(mat_in, np.ndarray):
        if not isinstance(mat_out, np.ndarray):
            mat_out = np.array(mat_out)
        return conc_feature_numpy_mats(mat_in, mat_out, ax=0)
    else:
        mat_in = np.array(mat_in)
        if not isinstance(mat_out, np.ndarray):
            mat_out = np.array(mat_out)
        return conc_feature_numpy_mats(mat_in, mat_out, ax=0)



def conc_numpy_mats(mat_in, mat_out, ax=0):
    if mat_in.size == 0:
        return mat_out
    dims = np.shape(mat_in)
    if len(dims) > 1:
        if len(mat_out) > 0:
            mat_out = np.concatenate((mat_out, mat_in), axis=ax)
        else:
            mat_out = mat_in
    elif len(dims) == 1 and len(mat_in) > 0:
        if len(mat_out) > 0:
            mat_out = np.concatenate((mat_out, [mat_in]), axis=ax)
        else:
            mat_out = [mat_in]
    else:
        LOG.debug('warning: conc_numpy_mats: mat_out not changed... len(dims) == 0, dims=[%s]; len(mat_in)=%s '
                  % (dims, len(mat_in)))
    return mat_out


def update_mat_list(mat_in, mat_out, smsg=False):
    if isinstance(mat_in, np.ndarray):
        if not isinstance(mat_out, np.ndarray):
            mat_out = np.array(mat_out)
        return conc_numpy_mats(mat_in, mat_out, ax=0)
    elif not isinstance(mat_in, list):
        mat_in = list(mat_in)

    if len(mat_in) == 0:
        return mat_out
    dims = np.shape(mat_in)
    if smsg:
        if mat_in:
            LOG.debug('update_mat_list, dims:', dims, len(dims), len(mat_in), type(mat_in), type(mat_in[0]))
        else:
            LOG.debug('update_mat_list, empty mat_in dims:', dims, len(dims), len(mat_in), type(mat_in))
    if len(dims) > 1:
        if len(mat_in) > 0:
            if smsg:
                LOG.debug('update_mat_list, len(dims) > 1 ,(mat_in) > 0 : ', dims)
            if len(mat_out) > 0:
                mat_out += mat_in
            else:
                mat_out = mat_in
    elif len(dims) == 1 and len(mat_in) > 0:
        if smsg:
            LOG.debug('update_mat_list, len(dims) == 1 : ', dims)
        if len(mat_out) > 0:
            mat_out += [mat_in]
        else:
            mat_out = [mat_in]
    else:
        LOG.debug('warning: update_mat_list: mat_out not changed... len(dims) == 0, dims=[%s]; len(mat_in)=%s '
                  % (dims, len(mat_in)))
    return mat_out



def get_img_swaped(img, aspect_ratio=True, size0=224):
    # download and show the image
    # fname = mx.test_utils.download(url)
    if img is None:
        return None
    # convert into format (batch, RGB, width, height)
    try:
        dims = np.shape(img)[0:2]
        if dims[0] != size0 or dims[1] != size0:
            if aspect_ratio:
                img = rescale_img(img)
            else:
                img = rescale_img(img, (size0, size0))
            print("get_image:img.size", dims[0], dims[1], 'aspect_ratio =', aspect_ratio)
        img = np.swapaxes(img, 0, 2)
        img = np.swapaxes(img, 1, 2)
        img = img[np.newaxis, :]

    except Exception as inst:
        print("convert into format (batch, RGB, width, height) had exception on get_image: ", inst)
        img = None
        pass

    return img

def get_bulk_swapped(batch_image_blocks):
    #print("get_bulk_swapped: batch_image_blocks ", np.shape(batch_image_blocks), type(batch_image_blocks))
    img_batch = [[]]*len(batch_image_blocks)
    for index, img_block in enumerate(batch_image_blocks):
        #print("get_bulk_swapped: index, img_block ", index, np.shape(img_block), type(img_block))
        img_swapped = get_img_swaped(img_block)
        img_batch[index] = img_swapped.copy()
    img_conc = np.concatenate(img_batch)
    return img_conc



def get_square_slices(img, slicesize=DEFAULT_IMAGE_SIZE, ovFac=2):
    width = img.shape[1]
    height = img.shape[0]
    if height <= slicesize and width <= slicesize:
        # print('warning get_square_slices: image size is smaller than slice size = ', img.shape, slicesize)
        return [img]
    y_end = height - slicesize
    x_end = width - slicesize
    y_starts = list(range(0, y_end + 1, slicesize // ovFac))
    if len(y_starts) > 0 and y_starts[len(y_starts) - 1] < y_end - 1:
        y_starts.append(y_end)
    x_starts = list(range(0, x_end + 1, slicesize // ovFac))
    if len(x_starts) > 0 and x_starts[len(x_starts) - 1] < x_end - 1:
        x_starts.append(x_end)
    return [img[y:y + slicesize, x:x + slicesize] for y in y_starts for x in x_starts]


def get_rectangle_slices(img, y_slicesize=DEFAULT_IMAGE_SIZE, x_slicesize=DEFAULT_IMAGE_SIZE, ovFac=2):
    width = img.shape[1]
    height = img.shape[0]
    if height <= y_slicesize and width <= x_slicesize:
        # print('warning get_rectangle_slices: image size is smaller than slice size = ', img.shape, y_slicesize, x_slicesize)
        return [img]
    y_end = height - y_slicesize
    x_end = width - x_slicesize
    y_starts = list(range(0, y_end + 1, y_slicesize // ovFac))
    if len(y_starts) > 0 and y_starts[len(y_starts) - 1] < y_end - 1:
        y_starts.append(y_end)
    x_starts = list(range(0, x_end + 1, x_slicesize // ovFac))
    if len(x_starts) > 0 and x_starts[len(x_starts) - 1] < x_end - 1:
        x_starts.append(x_end)
    return [img[y:y + y_slicesize, x:x + x_slicesize] for y in y_starts for x in x_starts]


### batch functions

def get_img_center(scaledImg, target_size=224):
    img_cenetr = None
    pieces = 0
    try:  # cv2
        dims = np.shape(scaledImg)[0:2]
        if dims[0] == target_size and dims[1] == target_size:
            return scaledImg, pieces
        sc_height = scaledImg.shape[0]
        sc_width = scaledImg.shape[1]
        if abs(sc_height - target_size) < abs(sc_width - target_size):
            if sc_height > target_size:
                scaledImg = (scaledImg[0:target_size, 0:sc_width]).copy()
                sc_height = scaledImg.shape[0]
                sc_width = scaledImg.shape[1]
        else:
            if sc_width > target_size:
                scaledImg = (scaledImg[0:sc_height, 0:target_size]).copy()
                sc_height = scaledImg.shape[0]
                sc_width = scaledImg.shape[1]
        if sc_height != target_size and sc_width != target_size:
            print('1 Warning: stopped get_img_slice - skip stopped scaledImg size =', target_size, scaledImg.shape)
            return scaledImg, pieces
        if sc_height == target_size:
            xc0 = int(0.5 * (sc_width - target_size + 1))
            xce = xc0 + target_size
            img_cenetr = (scaledImg[0:sc_height, xc0:xce]).copy()
            ratio = sc_width / target_size
            n_parts = int(ratio)
            pieces = n_parts
            addi = ratio - n_parts
            if addi > 0:
                pieces += 1
        else:  # sc_width == target_size
            yc0 = int(0.5 * (sc_height - target_size + 1))
            yce = yc0 + target_size
            img_cenetr = (scaledImg[yc0:yce, 0:sc_width]).copy()
            ratio = sc_height / target_size
            n_parts = int(ratio)
            pieces = n_parts
            addi = ratio - n_parts
            if addi > 0:
                pieces += 1
    except Exception as inst:
        print("cv2 cropped had exception TODO try PIL: ", inst)
        # isok, cropped_images = get_block_images_by_PIL(img, target_size, debug_mode)
        # if not isok:
        #    print('crop_save_by_PIL is failed -> skip')
        pass

    return img_cenetr, pieces


def get_img_section(scaledImg, block_chosen=0, target_size=224, debug_mode=True):
    try:  # cv2
        dims = np.shape(scaledImg)[0:2]
        if dims[0] == target_size and dims[1] == target_size:
            return scaledImg
        sc_height = scaledImg.shape[0]
        sc_width = scaledImg.shape[1]
        if abs(sc_height - target_size) < abs(sc_width - target_size):
            if sc_height > target_size:
                scaledImg = (scaledImg[0:target_size, 0:sc_width]).copy()
                sc_height = scaledImg.shape[0]
                sc_width = scaledImg.shape[1]
        else:
            if sc_width > target_size:
                scaledImg = (scaledImg[0:sc_height, 0:target_size]).copy()
                sc_height = scaledImg.shape[0]
                sc_width = scaledImg.shape[1]
        if sc_height != target_size and sc_width != target_size:
            print('1 Warning: stopped get_img_slice - skip stopped scaledImg size =', target_size, scaledImg.shape)
            return scaledImg
        if sc_height == target_size:
            ratio = sc_width / target_size
            n_parts = int(ratio)
            if block_chosen < n_parts:
                jj = block_chosen
                i0 = int(jj * target_size)
                ie = int((jj + 1) * target_size)
                imadd = (scaledImg[0:sc_height, i0:ie]).copy()
                return imadd

            addi = ratio - n_parts
            if addi > 0:
                if block_chosen == n_parts:
                    ie = sc_width
                    i0 = ie - target_size
                    imadd = (scaledImg[0:sc_height, i0:ie]).copy()
                    return imadd

        else:  # sc_width == target_size
            ratio = sc_height / target_size
            n_parts = int(ratio)
            if block_chosen < n_parts:
                jj = block_chosen
                i0 = int(jj * target_size)
                ie = int((jj + 1) * target_size)
                imadd = (scaledImg[i0:ie, 0:sc_width]).copy()
                return imadd

            addi = ratio - n_parts
            if addi > 0:
                if block_chosen == n_parts:
                    ie = sc_height
                    i0 = ie - target_size
                    imadd = (scaledImg[i0:ie, 0:sc_width]).copy()
                    return imadd
    except Exception as inst:
        print("cv2 cropped had exception TODO try PIL: ", inst)
        # isok, cropped_images = get_block_images_by_PIL(img, target_size, debug_mode)
        # if not isok:
        #    print('crop_save_by_PIL is failed -> skip')
        pass

    return scaledImg


def get_image_blocks_collection(scaledImg, target_size=DEFAULT_IMAGE_SIZE):
    img_center, pieces = get_img_center(scaledImg, target_size)
    image_blocks = [[]]*len(pieces+1)
    image_blocks[0] = img_center.copy()

    for block_chosen in range(pieces):
        img_block = get_img_section(scaledImg, block_chosen, target_size)
        image_blocks[block_chosen+1] = img_block.copy()
    # print('image_blocks shape:', np.array(image_blocks).shape)
    return image_blocks, pieces


def divide_image_to_slices(image, target_size=DEFAULT_IMAGE_SIZE):
    sz_img = np.shape(image)
    if sz_img[0] <= target_size and sz_img[1] <= target_size:
        if sz_img[0] < target_size or sz_img[1] < target_size:
            out_size = (target_size, target_size)  # (width, height)
            image = cv2.resize(image, out_size, interpolation=cv2.INTER_AREA)
            # print('debo1 sz_img= ', sz_img)
            sz_img = np.shape(image)
            # print('debo sz_img= ', sz_img)
        pieces = 1
        dims = sz_img[0:2]
        return [image], pieces, dims
    slices_image = get_rectangle_slices(image, DEFAULT_IMAGE_SIZE, DEFAULT_IMAGE_SIZE)
    if slices_image is None or len(slices_image) == 0:
        slices_image = []
        pieces = 0
        dims = 0
        print("Error divide_image_to_slices: get_rectangle_slices is empty - skip ", slices_image)
        return slices_image, pieces, dims

    dims = slices_image[0].shape[0:2]
    pieces = len(slices_image)
    return slices_image, pieces, dims


def divide_image_to_blocks(image, target_size=DEFAULT_IMAGE_SIZE):
    image_blocks = []
    pieces = 0
    scaled_image, dims = get_resize_img_dims(image)
    if scaled_image is None or len(scaled_image) == 0:
        print("Error divide_image_to_blocks: get_resize_img is empty - skip ", scaled_image)
        return image_blocks, pieces, dims

    image_blocks, pieces = get_image_blocks_collection(scaled_image, target_size)
    return image_blocks, pieces, dims


def get_image_blocks(image_vec, do_rescale=True, use_slices=True):
    batch_image_blocks = []
    img_pieces_vec = []
    img_dims_vec = []
    if use_slices:
        for image in image_vec:
            if do_rescale:
                image = resize_min(image)
                # print('deb image.shape = ', image.shape)
            image_blocks, pieces, dims = divide_image_to_slices(image, target_size=DEFAULT_IMAGE_SIZE)
            # print('deb divide_image_to_slices image: blocks[0].shape = ', image_blocks[0].shape, pieces, len(image_blocks))
            # print('ddd: np.shape(image_blocks) = ', np.shape(image_blocks), type(image_blocks))
            img_dims_vec += [dims]
            img_pieces_vec += [pieces]
            try:
                batch_image_blocks += image_blocks
            except:
                try:
                    #print('warning: np.shape(image) = ', np.shape(image), len(np.shape(image)))
                    batch_image_blocks += [image_blocks]
                except Exception as inst:
                    print("error: skip...get_image_blocks had exception on  =  ", inst)
                    continue
            # print('bbb: np.shape(batch_image_blocks) = ', np.shape(batch_image_blocks), type(batch_image_blocks))
    else:
        for image in image_vec:
            image_blocks, pieces, dims = divide_image_to_blocks(image, target_size=DEFAULT_IMAGE_SIZE)
            img_dims_vec += [dims]
            # print('deb divide_image_to_blocks image_blocks[0] = ', image_blocks[0].shape)
            img_pieces_vec += [len(image_blocks)]
            batch_image_blocks += image_blocks

    return batch_image_blocks, img_pieces_vec, img_dims_vec



def transform_x(xval, shift=0.4936203776879302, slope=20.0):
    dx = xval - int(xval)
    rxval = round(xval, 3)
    if xval > 1.0:
        yval0 = 0.9875
    else:
        yval0 = rxval - 0.005
    y_val = max(min(yval0 - 0.001 * dx, 0.99), 0.0)
    return slope * (y_val - shift)


def final_sigmoid(xval, shift=0.4936203776879302, alpha=0.40253157):
    y_val = transform_x(xval, shift)
    arg = alpha * y_val
    sig_out = 1.0 / (1.0 + np.exp(-arg))
    return sig_out


def estimate_weight_sigmoid(weight):
    yval = 0.5 * (1.0 + final_sigmoid(weight))
    return yval

## read image , slicing etc


# similarity


from sklearn.metrics.pairwise import cosine_similarity
#print('cosine_similarity = ', cosine_similarity(feature_matL, feature_matR)[0,0], cosine_similarity(feature_matR, feature_matL)[0,0])

def get_sim_cosine_similarity(v1, v2):
    try:
        if not isinstance(v1, np.ndarray):
            v1 = np.asarray(v1)
        if not isinstance(v2, np.ndarray):
            v2 = np.asarray(v2)
        if v1.ndim < 2:
            v1 = np.expand_dims(v1, axis=0)
        if v2.ndim < 2:
            v2 = np.expand_dims(v2, axis=0)
        out_sim = cosine_similarity(v1, v2)[0, 0]
    except:
        print('error get_sim_cosine_similarity: cosine_similarity failed ')
        print('error get_sim_cosine_similarity: v1 =  ', v1)
        print('error get_sim_cosine_similarity: v2 =  ', v2)
        out_sim = -1
    return out_sim

def get_sim_cosine(v1, v2, lc_eps=2.2e-16):
    try:
        if not isinstance(v1, np.ndarray):
            v1 = np.asarray(v1)
        if not isinstance(v2, np.ndarray):
            v2 = np.asarray(v2)
        if len(v1.shape) > 1:
            v10 = v1.flatten()
        else:
            v10 = v1
        if len(v2.shape) > 1:
            v20 = v2.flatten()
        else:
            v20 = v2
        sv1 = np.sqrt(sum([c*c for c in v10]))
        sv2 = np.sqrt(sum([c*c for c in v20]))
        denom = max(sv1*sv2, lc_eps)
        smult = sum([c1*c2 for c1, c2 in zip(v10,v20)])
        out_sim = smult/denom
    except:
        print('error get_sim_cosine failed ')
        out_sim = -1
    return out_sim

def sim_cosine(v1, v2, weight=0.5):
    cos_sim0 = get_sim_cosine(v1, v2)
    cos_sim1 = get_sim_cosine_similarity(v1, v2)
    out_sim = weight*cos_sim0 + (1.0-weight)*cos_sim1
    return out_sim


def get_set_diff(listA, listB):
    set_A = set(listA)
    set_B = set(listB)
    vec_diff = set_A.symmetric_difference(set_B)
    # print("diff :", (set_A ^ set_B) == vec_diff)
    # print("vec_diff :", vec_diff)
    return vec_diff


def get_set_common(listA, listB):
    set_A = set(listA)
    set_B = set(listB)
    vec_diff = set_A.intersection(set_B)
    # print("Intersection :", (set_A & set_B) == vec_diff)
    return vec_diff


def get_list_union(listA, listB):
    set_A = set(listA)
    set_B = set(listB)
    vec_union = list(set_A.union(set_B))
    return vec_union




