class ErrorDto(dict):
    def __init__(self, error, status, detail):
        dict.__init__(self, error=error, status=status, detail=detail)


class MlException(Exception):
    pass


class MlValidationException(Exception):
    pass


class MlTooLongException(Exception):
    pass
